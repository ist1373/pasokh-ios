//
//  AppDelegate.swift
//  SnapAsk
//
//  Created by iman on 5/12/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import AKMaskField
import Cache
import NVActivityIndicatorView
import Material
import ISTimeline
import ToastSwiftFramework
import FontAwesomeKit
import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var storage:Storage?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        UIApplication.shared.statusBarStyle = .lightContent
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "IRANSansMobile", size: 11)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "IRANSansMobile", size: 11)!], for: .selected)
        
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleMultiple
        //NVActivityIndicatorView.DEFAULT_COLOR = UIColor(red: 55/255, green: 34/255, blue: 96/255, alpha: 1.0)
        NVActivityIndicatorView.DEFAULT_COLOR = .orange
        
        DropDown.startListeningToKeyboard()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let signStoryboard: UIStoryboard = UIStoryboard(name: "SignInUp", bundle: nil)
        let studentStoryboard: UIStoryboard = UIStoryboard(name: "StudentMain", bundle: nil)
        let tutorStoryboard: UIStoryboard = UIStoryboard(name: "TutorMain", bundle: nil)
        
        
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token"){
            print("kkkkkkkkkkkkk")
            print(token)
                if let userType = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "userType")
                {

                    if userType == "Student"
                    {
                        let initialViewController: ST_ContainerViewController = studentStoryboard.instantiateViewController(withIdentifier: "ST_ContainerViewController") as! ST_ContainerViewController
                        self.window?.rootViewController = initialViewController
                    }
                    else if userType == "Tutor"
                    {
                        let initialViewController: TT_ContainerViewController = tutorStoryboard.instantiateViewController(withIdentifier: "TT_ContainerViewController") as! TT_ContainerViewController
                        self.window?.rootViewController = initialViewController
                    }

                }
                else{
                    let initialViewController: LoginViewController = signStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.window?.rootViewController = initialViewController
                }
        }
        else
        {
            let initialViewController: LoginViewController = signStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window?.rootViewController = initialViewController
        }
    
        
        
//        let initialViewController: ST_GradeSelViewController = signStoryboard.instantiateViewController(withIdentifier: "ST_GradeSelViewController") as! ST_GradeSelViewController
//        self.window?.rootViewController = initialViewController

        
//        let chatStoryboard: UIStoryboard = UIStoryboard(name: "ChatMessage", bundle: nil)
//        let initialViewController: ChatNavController = chatStoryboard.instantiateViewController(withIdentifier: "ChatNavController") as! ChatNavController
//        self.window?.rootViewController = initialViewController
        
        
//        let introStoryboard: UIStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
//        let initialViewController: IntroductionViewController = introStoryboard.instantiateViewController(withIdentifier: "IntroductionViewController") as! IntroductionViewController
//        self.window?.rootViewController = initialViewController
        
            self.window?.makeKeyAndVisible()
            
            return true
        
        
        
        
        return true
    }
    


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let purchaseStoryboard: UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let vc = purchaseStoryboard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
        print(url.query)
        print("url.query")
        
        for attr in url.query!.components(separatedBy: "&")
        {
            if attr.components(separatedBy: "=")[0] == "Authority"
            {
                vc.authority = attr.components(separatedBy: "=")[1]
            }
            if attr.components(separatedBy: "=")[0] == "Status"
            {
                if attr.components(separatedBy: "=")[1] == "NOK"
                {
                    print(attr.components(separatedBy: "=")[1])
                    vc.callbackSuccess = false
                    vc.callbackMessage = "عملیات نا موفق بود"
                }
                else
                {
                    vc.callbackSuccess = true
                    vc.callbackMessage = "عملیات با موفق انجام شد"
                }
            }
        }
        
        
        vc.isCallback = true
        

        window?.rootViewController?.dismiss(animated: false, completion: nil)
        window?.rootViewController?.present(vc, animated: false, completion: nil)
        return true
    }
    

    
    static let diskConfig = DiskConfig(
        // The name of disk storage, this will be used as folder name within directory
        name: "Booksum",
        // Expiry date that will be applied by default for every added object
        // if it's not overridden in the `setObject(forKey:expiry:)` method
        expiry: .date(Date().addingTimeInterval(1*3600)),
        // Maximum size of the disk cache storage (in bytes)
        maxSize: 10000000,
        // Where to store the disk cache. If nil, it is placed in `cachesDirectory` directory.
        directory: try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask,
                                                appropriateFor: nil, create: true).appendingPathComponent("MyPreferences"),
        // Data protection is used to store files in an encrypted format on disk and to decrypt them on demand
        protectionType: .complete
    )
    
    static let memoryConfig = MemoryConfig(
        // Expiry date that will be applied by default for every added object
        // if it's not overridden in the `setObject(forKey:expiry:)` method
        expiry: .date(Date().addingTimeInterval(1*60)),
        /// The maximum number of objects in memory the cache should hold
        countLimit: 50,
        /// The maximum total cost that the cache can hold before it starts evicting objects
        totalCostLimit: 0
    )
   
    
    static func getStorage() ->Storage
    {
        if storage == nil
        {
            do {
                storage = try Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
                
            } catch {
                print(error)
            }
            try? storage?.removeExpiredObjects()
            return storage!
        }
        else
        {
            try? storage?.removeExpiredObjects()
            return storage!
        }
    }
    func showLoginPage()
    {
        let signStoryboard: UIStoryboard = UIStoryboard(name: "SignInUp", bundle: nil)
        let initialViewController: LoginViewController = signStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.window?.rootViewController = initialViewController
    }
    

}





extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIImage {
    class func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

        
    func scaledImage(withSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    func scaleImageToFitSize(size: CGSize) -> UIImage {
        let aspect = self.size.width / self.size.height
        if size.width / aspect <= size.height {
            return scaledImage(withSize: CGSize(width: size.width, height: size.width / aspect))
        } else {
            return scaledImage(withSize: CGSize(width: size.height * aspect, height: size.height))
        }
    }
        
   
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func hideModalWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.hideModal))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func hideModal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showToast(message:String)
    {
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = .red
        style.cornerRadius = 15
        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
        self.view.makeToast(message,duration: 3.0, position: .bottom, style: style)
    }
    func showToast(message:String,bgColor:UIColor)
    {
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = bgColor
        style.cornerRadius = 15
        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
        self.view.makeToast(message,duration: 3.0, position: .bottom, style: style)
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}






extension UIImageView {
    
    func setRounded() {
        let radius = (self.frame.width) / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

class RaisedButtonRTL: RaisedButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - 35), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}

class RaisedButtonLTR: RaisedButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 5 , bottom: 5, right: (bounds.width - 35))
            titleEdgeInsets = UIEdgeInsets(top: 0, left: (imageView?.frame.width)!, bottom: 0, right: 0)
        }
    }
}

//class RaisedButtonToggle: RaisedButton {
//    override func layoutSubviews() {
//        super.layoutSubviews()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.layer.borderWidth = 1
//        self.layer.borderColor = UIColor.green.cgColor
//        self.layer.cornerRadius = 2
//        self.titleLabel?.font = UIFont(name:"IRANSansMobile" , size:15)
//        self.titleColor = .green
//    }
//}

extension RaisedButton {
    
    func setAsToggleButton(color:UIColor) {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = 4
        self.titleLabel?.font = UIFont(name:"IRANSans" , size:15)
        self.titleColor = color
        
    }
}
extension UITextField{
    
    func setAsDropDown()
    {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 15));
        var image = FAKIonIcons.arrowDownBIcon(withSize: 25).image(with: CGSize(width:25,height:25)).tint(with: self.textColor!);
        imageView.image = image;
        imageView.contentMode = .scaleAspectFit
        
        self.rightViewMode = .always
        self.rightView = imageView
        
        self.allowsEditingTextAttributes = false
    }
}

extension Date {
    var yearsFromNow:   Int { return Calendar.current.dateComponents([.year],       from: self, to: Date()).year        ?? 0 }
    var monthsFromNow:  Int { return Calendar.current.dateComponents([.month],      from: self, to: Date()).month       ?? 0 }
    var weeksFromNow:   Int { return Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear  ?? 0 }
    var daysFromNow:    Int { return Calendar.current.dateComponents([.day],        from: self, to: Date()).day         ?? 0 }
    var hoursFromNow:   Int { return Calendar.current.dateComponents([.hour],       from: self, to: Date()).hour        ?? 0 }
    var minutesFromNow: Int { return Calendar.current.dateComponents([.minute],     from: self, to: Date()).minute      ?? 0 }
    var secondsFromNow: Int { return Calendar.current.dateComponents([.second],     from: self, to: Date()).second      ?? 0 }
    var relativeTime: String {
        if yearsFromNow   > 0 { return "\(yearsFromNow) سال"    + (yearsFromNow    > 1 ? "" : "") + " پیش" }
        if monthsFromNow  > 0 { return "\(monthsFromNow) ماه"  + (monthsFromNow   > 1 ? "" : "") + " پیش" }
        if weeksFromNow   > 0 { return "\(weeksFromNow) هفته"    + (weeksFromNow    > 1 ? "" : "") + " پیش" }
        if daysFromNow    > 0 { return daysFromNow == 1 ? "دیروز" : "\(daysFromNow) روز پیش" }
        if hoursFromNow   > 0 { return "\(hoursFromNow) ساعت"     + (hoursFromNow   > 1 ? "" : "") + " پیش" }
        if minutesFromNow > 0 { return "\(minutesFromNow) دقیقه" + (minutesFromNow > 1 ? "" : "") + " پیش" }
        if secondsFromNow > 0 { return secondsFromNow < 15 ? "اکنون" : "\(secondsFromNow) ثانیه پیش" }
        return ""
    }
}


extension UINavigationItem {
    
    
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = GlobalProperties.getIranSansFont(size: 14)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = GlobalProperties.getIranSansFont(size: 10)
        two.textAlignment = .center
        two.sizeToFit()
        
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        
        
        self.titleView = stackView
    }
}


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}



public extension String {
    
    public var replacedEnglishDigitsWithPersian: String {
        var str = self
        let map = ["0" : "۰",
                   "1" : "۱",
                   "2": "۲",
                   "3": "۳",
                   "4": "۴",
                   "5": "۵",
                   "6": "۶",
                   "7": "۷",
                   "8": "۸",
                   "9": "۹"]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
    }
}


extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        guard let text = self.text else { return super.intrinsicContentSize }
        
        var contentSize = super.intrinsicContentSize
        var textWidth: CGFloat = frame.size.width
        var insetsHeight: CGFloat = 0.0
        
        if let insets = padding {
            textWidth -= insets.left + insets.right
            insetsHeight += insets.top + insets.bottom
        }
        
        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedStringKey.font: self.font], context: nil)
        
        contentSize.height = ceil(newSize.size.height) + insetsHeight
        
        return contentSize
    }
}
