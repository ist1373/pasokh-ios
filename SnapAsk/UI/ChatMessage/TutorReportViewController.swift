//
//  TutorReportViewController.swift
//  SnapAsk
//
//  Created by iman on 7/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import DropDown
import NVActivityIndicatorView
import DropDown

protocol TutorReportDelegate:class {
    func onReportComplete();
}

class TutorReportViewController: UIViewController {
    
    weak var delegate :TutorReportDelegate?

    var tutor:User?
    var questionId:Int64 = 0;
    var complaintReport = ComplaintReport()
    
    @IBOutlet weak var report_tf: UITextField!
    @IBOutlet weak var desc_tf: UITextField!
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    let dropDownReport = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        report_tf.setAsDropDown()
        report_tf.tintColor = .clear
        report_tf.addTarget(self, action: #selector(onEditBegining), for: .editingDidBegin)
        // Do any additional setup after loading the view.
        
        
        
        dropDownReport.anchorView = report_tf
        
        dropDownReport.dataSource = [ReportTopic.LateAnswered.description , ReportTopic.NotAnswered.description ,ReportTopic.WrongAnswered.description , ReportTopic.Other.description]
        
        dropDownReport.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .right
            cell.optionLabel.font = GlobalProperties.getIranSansFont(size: 13)
        }

        
        dropDownReport.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.report_tf.text = item
            switch item {
            case ReportTopic.LateAnswered.description:
                self.complaintReport.reportTopic = ReportTopic.LateAnswered
                break
            case  ReportTopic.NotAnswered.description:
                self.complaintReport.reportTopic = ReportTopic.NotAnswered
                break
            case ReportTopic.WrongAnswered.description:
                self.complaintReport.reportTopic = ReportTopic.WrongAnswered
                break
            case ReportTopic.Other.description:
                self.complaintReport.reportTopic = ReportTopic.Other
                break
            default:
                break
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onSend(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        
        complaintReport.description = desc_tf.text
        let q = Question()
        q.id = questionId
        complaintReport.question = q
        if let u = tutor
        {
            complaintReport.tutor = u;
        }
        
        if(complaintReport.reportTopic == nil)
        {
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.showToast(message: "لطفا موضوع گزارش را انتخاب کنید", bgColor: UIColor.red)
            return
        }
        ComplaintService.getInstance().addComplaintReport(complaintReport: complaintReport).subscribe(onNext:{res in
            if res.success! == 1
            {
                
                self.delegate?.onReportComplete();
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.showToast(message: res.message!, bgColor: UIColor.red)
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
        
        
    }
    
    
    @objc func onEditBegining()
    {
        self.view.endEditing(true)
        dropDownReport.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
