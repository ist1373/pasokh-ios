//
//  EndSessionViewController.swift
//  SnapAsk
//
//  Created by iman on 7/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

protocol EndSessionDelegate:class {
    func onSessionEnd();
}

class EndSessionViewController: UIViewController {

    
    weak var delegate :EndSessionDelegate?

    @IBOutlet weak var starBar: CosmosView!
    @IBOutlet weak var starLbl: UILabel!
    @IBOutlet weak var descTf: UITextField!

    @IBOutlet weak var favorateBtn: RaisedButton!
    
    
    var rating = 3;
    var question:Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        starBar.didTouchCosmos = { rating in
            self.rating = Int(rating)
            if rating == 1
            {
                self.starLbl.text = "بد"
            }
            else if rating == 2
            {
                self.starLbl.text = "ضعیف"
            }
            else if rating == 3
            {
                self.starLbl.text = "متوسط"
            }
            else if rating == 4
            {
                self.starLbl.text = "خوب"
            }
            else if rating == 5
            {
                self.starLbl.text = "عالی"
            }
        }
        
        favorateBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)

        FavoriteTutorService.getInstance().existFavoriteTutor(stuId: (question!.questioner?.id)!, tutorId: (question!.responder?.id)!)?.subscribe(onNext:{res in
            if res.success! == 1
            {
                if res.code == 100
                {
                    self.favorateBtn.animate(.background(color: GlobalProperties.PrimaryColor))
                    self.favorateBtn.titleColor = .white
                }
                else if res.code == 104
                {
                    self.favorateBtn.animate(.background(color: .white))
                    self.favorateBtn.titleColor = GlobalProperties.PrimaryColor
                }
            }
        })
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSelectFavorate(_ sender: Any) {
        
        if favorateBtn.backgroundColor == .white
        {
            favorateBtn.animate(.background(color: GlobalProperties.PrimaryColor))
            favorateBtn.titleColor = .white
            
            FavoriteTutorService.getInstance().addFavoriteTutor(stuId: (question!.questioner?.id)!, tutorId: (question!.responder?.id)!, isFavorate: true).subscribe(onNext:{res in
                if res.success! == 1
                {
                    self.showToast(message: "پاسخ دهنده به لیست پاسخ دهندگان محبوب اضافه شد", bgColor: GlobalProperties.PrimaryColor)
                }
            })
        }
        else {
            
            favorateBtn.animate(.background(color: .white))
            favorateBtn.titleColor = GlobalProperties.PrimaryColor
            FavoriteTutorService.getInstance().addFavoriteTutor(stuId: (question!.questioner?.id)!, tutorId: (question!.responder?.id)!, isFavorate: false).subscribe(onNext:{res in
                if res.success! == 1
                {
                    self.showToast(message: "پاسخ دهنده از لیست پاسخ دهندگان محبوب حذف شد", bgColor: GlobalProperties.PrimaryColor)
                }
            })
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func onEndSession(_ sender: Any) {
        QuestionService.getInstance().endQuestion(questionId: (question?.id!)!, rate: rating, desc: descTf.text!).subscribe(onNext:{res in
            if res.success! == 1
            {
                self.delegate?.onSessionEnd()
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.showToast(message: res.message!, bgColor: .red)
            }
        })
    
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
