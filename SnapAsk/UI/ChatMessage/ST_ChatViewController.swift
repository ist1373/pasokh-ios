//
//  ST_ChatViewController.swift
//  SnapAsk
//
//  Created by iman on 6/14/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SVPullToRefresh_Bell
import Material
import FontAwesomeKit
import Kingfisher
import CameraViewController
import ObjectMapper
import MaterialShowcase

//import UIScrollView_InfiniteScroll

class ST_ChatViewController: JSQMessagesViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate,UploadCompleteDelegate , StompClientLibDelegate ,TutorReportDelegate , EndSessionDelegate  , MaterialShowcaseDelegate{

    

    
    
    

    

    
    
    var isWating = false
    var messages = [JSQMessage]()
    var chatroom : ChatRoom?
    var question: Question?
    var page = 0;
    var user:User?
    var chatMessages:[ChatMessage] = []
    let socketClient = StompClientLib()
    var endSessionButton : UIBarButtonItem?
    var supportButton:UIBarButtonItem?
    var tutorialStep = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if let cr = try? AppDelegate.getStorage().object(ofType: ChatRoom.self, forKey: "chatroom")
        {
            chatroom = cr
        }
        
        if let q = try? AppDelegate.getStorage().object(ofType: Int.self, forKey: "qId")
        {
            loadQuestion(id: q)
        }
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            self.senderId = String(user.id!)
            self.senderDisplayName = " "
            self.user = user
            
            
        }

        FileService.getInstance().delegate = self
        self.initSocketLib()
        
        self.initBottomToolbar()
        self.initCollectionView()
        self.getExsitingMessages()
        

    }
    
    func loadQuestion(id:Int)
    {
        QuestionService.getInstance().getQuestionById(id: id)?.subscribe(onNext: {q in
            self.question = q
            
            self.initNavigationUI()
            
            if q.questionState == QuestionState.Finished
            {
                self.inputToolbar.isHidden = true
                self.endSessionButton?.isEnabled = false
            }
        })
    }
    

    func initSocketLib()
    {
//        let url = NSURL(string: "wss://192.168.26.1:8080/ws/websocket")!
        let url = GlobalProperties.WebSocketUrl
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self)
        socketClient.subscribe(destination: "/topic/private.chat.\((chatroom?.uid)!)")
    }
       

    
    func initNavigationUI()
    {
        let backButton = UIBarButtonItem(image: FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: .black), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        
        if let userType = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "userType")
        {
            if userType == "Student"
            {
                endSessionButton = UIBarButtonItem(image: UIImage(named: "end-session")?.scaledImage(withSize: CGSize(width: 25, height: 25)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(endSessionTapped))
                endSessionButton?.tintColor = .black
                
                supportButton = UIBarButtonItem(image: UIImage(named: "support")?.scaledImage(withSize: CGSize(width: 20, height: 20)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(tutorReportTapped))
                supportButton?.tintColor = .black
                navigationItem.leftBarButtonItems = [endSessionButton!,supportButton!]
                
                if let t = try? AppDelegate.getStorage().object(ofType: Bool.self, forKey: "st-chtutorial-end")
                {
                    if t {
                        
                    }
                    else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                            self.showTutorialCaseMenue()
                        }
                        
                    }
                }
                else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                        self.showTutorialCaseMenue()
                    }
                }
            }
        }
        


        
        navigationItem.rightBarButtonItem = backButton
        
        navigationItem.setTitle(title: (question?.questionTopic?.description)!, subtitle: (question?.grade?.description)!)
        
        
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func backButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func endSessionTapped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EndSessionViewController") as! EndSessionViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.question = question;
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
    func onSessionEnd() {
        self.showToast(message: "این جلسه با موفقیت پایان یافت", bgColor: GlobalProperties.PrimaryColor)
        inputToolbar.isHidden = true
        endSessionButton?.isEnabled = false
    }
    
    @objc func tutorReportTapped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TutorReportViewController") as! TutorReportViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.questionId = (question?.id)!
        vc.tutor = question?.responder
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
    }
    
    func initBottomToolbar()
    {
        inputToolbar.contentView.backgroundColor = UIColor.white
        inputToolbar.contentView.leftBarButtonItem = Button(image: UIImage(named:"add"))
        inputToolbar.contentView.rightBarButtonItem = Button(image: UIImage(named:"send"))

        inputToolbar.contentView.textView.placeHolder = "متن پیام"
        inputToolbar.contentView.textView.textAlignment = .right
        inputToolbar.contentView.textView.borderColor = UIColor.clear
        inputToolbar.contentView.textView.font = GlobalProperties.getIranSansFont(size: 13)
    }
    func initCollectionView()
    {
        collectionView.collectionViewLayout.messageBubbleFont = GlobalProperties.getIranSansFont(size: 14)
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        //collectionView.delegate = self
        
        collectionView.addPullToRefresh(actionHandler: {
            self.collectionView.pullToRefreshView.setTitle("در حال بارگزاری", for: SVPullToRefreshState.all)
            self.collectionView.pullToRefreshView.titleLabel.font = GlobalProperties.getIranSansFont(size: 14)
            self.collectionView.pullToRefreshView.stopAnimating()
            
            self.getExsitingMessages()
//            for  i  in 0 ... 10
//            {
//                let m = JSQMessage(senderId: i % 2 == 0 ?self.senderId : "sdfs", displayName: self.senderDisplayName, text: "برای رسیدن به یک نقطه مشترک باید \(i)")
//
//                self.messages.insert(m!, at: 0)
//                if i % 2 == 0
//                {
//
//                    let photoItem = JSQPhotoMediaItem(image: UIImage(named: "signbg"))
//
//                    let phm = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: photoItem)
//                    self.messages.insert(phm!, at: 0)
//
//
//                }
//            }
            
//            self.collectionView.reloadData()
//            self.collectionView.scrollToItem(at: IndexPath(item: 15, section: 0), at: UICollectionViewScrollPosition.top, animated: false)
            
        }, position: SVPullToRefreshPosition.top)
    }
    
    func getExsitingMessages()
    {
        
        ChatService.getInstance().getExistingChatMessages(roomUId: (chatroom?.uid)!, page: page).subscribe(onNext:{res in
            if res.count > 0
            {
                for mess in res
                {
                    print("sender:",self.senderId)
                    print("author:",(mess.authorUser?.id)!)
                    self.chatMessages.insert(mess, at: 0)
                    if mess.messageType == MessageType.SEND_TEXT
                    {
                        print("text:\(mess.id)")
                        let message = JSQMessage(senderId: "\((mess.authorUser?.id)!)", displayName: " ", text: mess.contents)
                        
                        self.messages.insert(message!, at: 0)
                    }
                    else if mess.messageType == MessageType.SEND_IMAGE
                    {
                        if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: "\((mess.authorUser?.id)!)" == self.senderId) {
                            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
                            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((mess.mediaFile?.id)!)))!
                            imageView.kf.setImage(with: url, completionHandler: {
                                (image, error, cacheType, imageUrl) in
                                mediaItem.image = image
                                
                            })
                            let phm = JSQMessage(senderId: "\((mess.authorUser?.id)!)", displayName: " ", media: mediaItem)
                            self.messages.insert(phm!, at: 0)
                            //self.addPhotoMessage(withId: "id", key: "snapshot.key", mediaItem: mediaItem)
                        }
                    }
                }
                if self.page == 0
                {
                    self.finishSendingMessage()
                    self.finishReceivingMessage()
                }
                else
                {
                    self.collectionView.reloadData()
                    self.collectionView.scrollToItem(at: IndexPath(item: res.count, section: 0), at: UICollectionViewScrollPosition.top, animated: false)
                }
                self.page += 1
            }
            else
            {
                
            }
            
            
        })
    }

    
    func onReportComplete() {
        self.showToast(message: "گزارش شما با موفقیت ارسال شد", bgColor: GlobalProperties.PrimaryColor)
    }
    
    
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }()
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        return NSAttributedString(string: "")
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return nil
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        print(chatMessages.count)
        if messages[indexPath.row].isMediaMessage
        {
            print("is message")
            let sb = UIStoryboard(name: "ImageView", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "ImageViewController")
            (vc as! ImageViewController).imgId = chatMessages[indexPath.row].mediaFile?.id
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        //cell.textView.font = GlobalProperties.getIranSansFont(size: 14)
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
            cell.textView?.textAlignment = .right
        } else {
            cell.textView?.textColor = GlobalProperties.PrimaryText
        }
        return cell
    }
    
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        let m = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        messages.append(m!)
        
        let cm = ChatMessage()
        cm.chatRoom = chatroom
        cm.authorUser = self.user
        cm.messageType = MessageType.SEND_TEXT
        cm.contents = text
        cm.timeSent = doubleToInteger(data: Date().timeIntervalSince1970 * 1000)
        
        chatMessages.append(cm)
        
        ChatService.getInstance().addChatMessage(roomUId: (chatroom?.uid)!, chatMessage: cm).subscribe(onNext:{res in
            print("successs:\(res.success)")
        })
        finishSendingMessage()

    }
    func doubleToInteger(data:Double)-> Int64 {
        let doubleToString = "\(data)"
        let stringToInteger = (doubleToString as NSString).integerValue
        
        return Int64(stringToInteger)
    }
    
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            collectionView.reloadData()
        }
    }
    
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = false
            imagePickerController.sourceType = .photoLibrary
            
            
            let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
//            let imagePickerViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingEnabled) { [weak self] image, asset in
//                if let img = image{
//                    FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
//                    let photoItem = JSQPhotoMediaItem(image: image)
//                    self?.addMedia(photoItem!)
//                }
//                self?.dismiss(animated: true, completion: nil)
//            }
            let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true){ [weak self] image, asset in
                
                    if let img = image{
                        FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
                        let photoItem = JSQPhotoMediaItem(image: image)
                        self?.addMedia(photoItem!)
                    }
                    self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(imagePickerViewController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(photoAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
        self.messages.append(message!)
        
        self.finishSendingMessage(animated: true)
    }
    
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        let uploadImage = UploadedFile()
        uploadImage.id = imageId
        let cm = ChatMessage()
        cm.chatRoom = chatroom
        cm.authorUser = self.user
        cm.messageType = MessageType.SEND_IMAGE
        cm.mediaFile = uploadImage
        cm.timeSent = doubleToInteger(data: Date().timeIntervalSince1970 * 1000)
        ChatService.getInstance().addChatMessage(roomUId: (chatroom?.uid)!, chatMessage: cm).subscribe(onNext:{res in
            print("successs:\(res.success)")
        })
        chatMessages.append(cm)
        

//        let header = [StompCommands.commandHeaderContentType:"application/json;charset=UTF-8"]
//        socketClient.sendMessage(message: cm.toJSONString()!, toDestination: "/app/chat.addUser", withHeaders: header, withReceipt: nil)
        
        
        finishSendingMessage()
    }
    
    
    
    
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        
        print("didReceiveMessageWithJSONBody")
        print(jsonBody)
        let cm = Mapper<ChatMessage>().map(JSONObject: jsonBody)
        
        if "\((cm?.authorUser?.id)!)" != senderId
        {
            chatMessages.append(cm!)
            if cm?.messageType == MessageType.SEND_TEXT
            {
                let m = JSQMessage(senderId: "\((cm?.authorUser?.id)!)", displayName: " ", text: cm?.contents)
                messages.append(m!)
            }
            else
            {
                if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: "\((cm?.authorUser?.id)!)" == self.senderId) {
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
                    let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((cm?.mediaFile?.id)!)))!
                    imageView.kf.setImage(with: url, completionHandler: {
                        (image, error, cacheType, imageUrl) in
                        mediaItem.image = image
                        self.finishReceivingMessage()
                    })
                    let phm = JSQMessage(senderId: "\((cm?.authorUser?.id)!)", displayName: " ", media: mediaItem)
                    self.messages.append(phm!)
                    //self.addPhotoMessage(withId: "id", key: "snapshot.key", mediaItem: mediaItem)
                }
            }
            finishReceivingMessage()
        }

        
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        
    }
    
    func stompClientDidConnect(client: StompClientLib!) {
        print("stompClientDidConnect")
        //navigationItem.setTitle(title: "ایمان صابری", subtitle: "آنلاین")
        socketClient.subscribe(destination: "/topic/private.chat.\((chatroom?.uid)!)")
        
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        
    }
    
    func serverDidSendPing() {
        
    }
    
    
    

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .white
    }
    
    
    
    
    func showTutorialCaseMenue() {
        
        
        let showcase = MaterialShowcase()
        showcase.setTargetView(barButtonItem: endSessionButton!) // always required to set targetView
        showcase.primaryText = " اتمام جلسه"
        showcase.secondaryText = "پس از دریافت پاسخ کامل از پاسخ دهنده، دکمه اتمام جلسه را فشار دهید و به پاسخ دهنده خود امتیاز دهید"
        
        
        
        // Background
        showcase.backgroundPromptColor = GlobalProperties.PrimaryDarkColor
        showcase.backgroundPromptColorAlpha = 0.95
        showcase.backgroundViewType = .circle
        
        // Target
        showcase.targetTintColor = GlobalProperties.PrimaryDarkColor
        showcase.targetHolderRadius = 44
        showcase.targetHolderColor = UIColor.clear
        // Text
        showcase.primaryTextColor = UIColor.white
        showcase.secondaryTextColor = UIColor.white
        showcase.primaryTextSize = 20
        showcase.secondaryTextSize = 15
        showcase.primaryTextFont = GlobalProperties.getIranSansFont(size: 15)
        showcase.secondaryTextFont = GlobalProperties.getIranSansFont(size: 13)
        //Alignment
        showcase.primaryTextAlignment = .right
        showcase.secondaryTextAlignment = .right
        // Animation
        showcase.aniComeInDuration = 0.5 // unit: second
        showcase.aniGoOutDuration = 0.5 // unit: second
        showcase.aniRippleScale = 1.5
        showcase.aniRippleColor = UIColor.white
        showcase.aniRippleAlpha = 0.2
        showcase.delegate = self
        
        
        showcase.show(completion: {
            try? AppDelegate.getStorage().setObject(true, forKey: "st-chtutorial-end" , expiry: .never)
        })
    }
    
    func showTutorialCaseAddQuestion() {
        
        
        let showcase = MaterialShowcase()
        showcase.setTargetView(barButtonItem: supportButton!) // always required to set targetView
        showcase.primaryText = " ثبت شکایت"
        showcase.secondaryText = "در صورت اعتراض به روند ارسال و دریافت پاسخ، شکایت خود را از این طریق ارسال نمایید"
        showcase.delegate = self
        
        
        // Background
        showcase.backgroundPromptColor = GlobalProperties.PrimaryDarkColor
        showcase.backgroundPromptColorAlpha = 0.95
        showcase.backgroundViewType = .circle
        
        // Target
        showcase.targetTintColor = GlobalProperties.PrimaryDarkColor
        showcase.targetHolderRadius = 44
        showcase.targetHolderColor = UIColor.clear
        // Text
        showcase.primaryTextColor = UIColor.white
        showcase.secondaryTextColor = UIColor.white
        showcase.primaryTextSize = 20
        showcase.secondaryTextSize = 15
        showcase.primaryTextFont = GlobalProperties.getIranSansFont(size: 15)
        showcase.secondaryTextFont = GlobalProperties.getIranSansFont(size: 13)
        //Alignment
        showcase.primaryTextAlignment = .center
        showcase.secondaryTextAlignment = .center
        // Animation
        showcase.aniComeInDuration = 0.5 // unit: second
        showcase.aniGoOutDuration = 0.5 // unit: second
        showcase.aniRippleScale = 1.5
        showcase.aniRippleColor = UIColor.white
        showcase.aniRippleAlpha = 0.2
        showcase.delegate = self
        
        showcase.show(completion: {
            // You can save showcase state here
            // Later you can check and do not show it again
            
        })
    }
    
    
    func showCaseDidDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
        
        if tutorialStep == 0
        {
            print("showcase runnnnnnnnn")
            showTutorialCaseAddQuestion()
        }
        tutorialStep += 1
        
    }
    
}
