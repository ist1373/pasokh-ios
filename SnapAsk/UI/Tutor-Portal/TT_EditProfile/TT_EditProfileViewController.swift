//
//  TT_EditProfileViewController.swift
//  SnapAsk
//
//  Created by iman on 7/24/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import NVActivityIndicatorView
import FontAwesomeKit
import CameraViewController


class TT_EditProfileViewController: UIViewController , UploadCompleteDelegate , UIImagePickerControllerDelegate ,UINavigationControllerDelegate , LocationTableViewDelegate {
    var tutor:Tutor!
    var uploadedFile:UploadedFile?
    
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var toolbar: Toolbar!
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var familyTf: UITextField!
    
    
    @IBOutlet weak var resumeTv: UITextView!
    
    
    @IBOutlet weak var bioBtn: RaisedButton!
    
    @IBOutlet weak var phsBtn: RaisedButton!
    
    @IBOutlet weak var chmBtn: RaisedButton!
    
    @IBOutlet weak var mthBtn: RaisedButton!
    
    @IBOutlet weak var engBtn: RaisedButton!
    
    @IBOutlet weak var rlgBtn: RaisedButton!
    
    @IBOutlet weak var arbBtn: RaisedButton!
    
    @IBOutlet weak var ltcBtn: RaisedButton!
    
    
    @IBOutlet weak var emailTf: UITextField!
    
    @IBOutlet weak var passTf: UITextField!
    
    @IBOutlet weak var passrepTf: UITextField!
    
    
    @IBOutlet weak var avatarImg: UIImageView!
    
    @IBOutlet weak var saveBtn: RaisedButton!
    
    @IBOutlet weak var editAvatarBtn: RaisedButton!
    
    var togglesBtn : [RaisedButton:Bool] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        city.setAsDropDown()
        city.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TT_EditProfileViewController.onShowProvinces)))
        

        
        bioBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        phsBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        chmBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        mthBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        engBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        rlgBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        arbBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        ltcBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        // Do any additional setup after loading the view.
        togglesBtn[bioBtn] = false
        togglesBtn[phsBtn] = false
        togglesBtn[chmBtn] = false
        togglesBtn[mthBtn] = false
        togglesBtn[engBtn] = false
        togglesBtn[rlgBtn] = false
        togglesBtn[arbBtn] = false
        togglesBtn[ltcBtn] = false
        
        updateTopics()
        getUser()
        initToolbar()
        
        FileService.getInstance().delegate = self
        editAvatarBtn.cornerRadiusPreset = .cornerRadius2
        saveBtn.cornerRadiusPreset = .cornerRadius2
        
    }
    
    func initToolbar()  {
        //UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
        
        toolbar.depthPreset = .depth2
        toolbar.backgroundColor = UIColor.white
        toolbar.titleLabel.font = GlobalProperties.getIranSansFont(size: 14)
        toolbar.titleLabel.text = "ویرایش اطلاعات";
        toolbar.titleLabel.textColor = UIColor.darkText
        toolbar.layer.zPosition = 10;
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: UIColor.darkText)
        
        let backBtn = IconButton(image: backImg?.tint(with: UIColor.darkText), tintColor: UIColor.darkText)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        toolbar.rightViews = [backBtn]
        
    }
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onShowProvinces()
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "Location", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil) //(vc, sender: self)
        vc.locationTableView.locationDelegate = self
    }
    
    func onSelectCity(city: City) {
        self.city.text = city.title
        tutor?.city = city
    }
    
    func getUser()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        TutorService.getInstance().getAuthenticatedTutor().subscribe(onNext:{t in
            self.tutor = t
            print(t)
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.loadData()
        })
    }
    
    
    
    func loadData()
    {
        print("tutor.topics")
        print(tutor.topics)
//        if let topics = tutor.topics
//        {
            print("t")
            for t in tutor.topics
            {
                print(t)
                switch(t)
                {
                case "Physics":
                    togglesBtn[phsBtn] = true
                    
                    break;
                case "Chemistry":
                    togglesBtn[chmBtn] = true
                    break;
                case "Biology":
                    togglesBtn[bioBtn] = true
                    break;
                case "Literature":
                    togglesBtn[ltcBtn] = true
                    break;
                case "Math":
                    togglesBtn[mthBtn] = true
                    break;
                case "Arabic":
                    togglesBtn[arbBtn] = true
                    break;
                case "Religious":
                    togglesBtn[rlgBtn] = true
                    break;
                case "English":
                    togglesBtn[engBtn] = true
                    break
                default:
                    break
                }
            }
            updateTopics()
//        }
//        else{
//            tutor.topics = [];
//        }
        
        if let name = tutor.firstName
        {
            nameTf.text = name
        }
        if let family = tutor.lastName
        {
            familyTf.text = family
        }
        if let resume = tutor.biography
        {
            resumeTv.text = resume
        }
        if let email = tutor.email
        {
            emailTf.text = email
        }
        if let uf = tutor.profileImage
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((uf.id)!)))!
            avatarImg.kf.setImage(with: url)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onBioSelect(_ sender: Any) {
        
        if togglesBtn[bioBtn]!
        {
            togglesBtn[bioBtn] = false
            if let index = tutor.topics.index(of: "Biology") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[bioBtn] = true
            tutor.topics.append("Biology")
        }
        updateTopics()
    }
    
    @IBAction func onPhsSelect(_ sender: Any) {
        if togglesBtn[phsBtn]!
        {
            togglesBtn[phsBtn] = false
            if let index = tutor.topics.index(of: "Physics") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[phsBtn] = true
            tutor.topics.append("Physics")

        }
        updateTopics()
    }
    
    @IBAction func onChmSelect(_ sender: Any) {
        if togglesBtn[chmBtn]!
        {
            togglesBtn[chmBtn] = false
            if let index = tutor.topics.index(of: "Chemistry") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[chmBtn] = true
            tutor.topics.append("Chemistry")

        }
        updateTopics()
    }
    
    @IBAction func onMthSelect(_ sender: Any) {
        if togglesBtn[mthBtn]!
        {
            togglesBtn[mthBtn] = false
            if let index = tutor.topics.index(of: "Math") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[mthBtn] = true
            tutor.topics.append("Math")

        }
        updateTopics()
    }
    
    @IBAction func onEngSelect(_ sender: Any) {
        if togglesBtn[engBtn]!
        {
            togglesBtn[engBtn] = false
            if let index = tutor.topics.index(of: "English") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[engBtn] = true
            tutor.topics.append("English")
        }
        updateTopics()
    }
    
    @IBAction func onRlgSelect(_ sender: Any) {
        if togglesBtn[rlgBtn]!
        {
            togglesBtn[rlgBtn] = false
            if let index = tutor.topics.index(of: "Religious") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[rlgBtn] = true
            tutor.topics.append("Religious")

        }
        updateTopics()
    }
    
    @IBAction func onArbSelect(_ sender: Any) {
        if togglesBtn[arbBtn]!
        {
            togglesBtn[arbBtn] = false
            if let index = tutor.topics.index(of: "Arabic") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[arbBtn] = true
            tutor.topics.append("Arabic")

        }
        updateTopics()
    }
    
    @IBAction func onLtcSelect(_ sender: Any) {
        if togglesBtn[ltcBtn]!
        {
            togglesBtn[ltcBtn] = false
            if let index = tutor.topics.index(of: "Literature") {
                tutor.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[ltcBtn] = true
            tutor.topics.append("Literature")

        }
        updateTopics()
    }
    
    
    
    func updateTopics()
    {
        for (k,v) in togglesBtn
        {
            if v
            {
                k.animate(.background(color: GlobalProperties.PrimaryColor))
                k.titleColor = .white
            }
            else
            {
                k.animate(.background(color: .white))
                k.titleColor = GlobalProperties.PrimaryColor
            }
            
        }
    }

    
    @IBAction func onEditAvatar(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        FileService.getInstance().delegate = self
        
        
        let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
        let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true) { [weak self] image, asset in

            if let img = image{
                self?.spinner.isHidden = false
                self?.spinner.startAnimating()

                self?.avatarImg.image = img
                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
            }
            self?.dismiss(animated: true, completion: nil)
        }
        

        
        self.present(imagePickerViewController, animated: true, completion: nil)
    }
    
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        if success
        {
            uploadedFile = UploadedFile()
            uploadedFile?.fileType = "Public"
            uploadedFile?.id = imageId
        }
        spinner.isHidden = true
        spinner.stopAnimating()
    }
    
    @IBAction func onSaveChanges(_ sender: Any) {
        if let up = uploadedFile
        {
            tutor.profileImage = up
        }
        if let firstName = nameTf.text
        {
            tutor.firstName = firstName
        }
        if let lastname = familyTf.text
        {
            tutor.lastName = lastname
        }
        if let res = resumeTv.text
        {
            tutor.biography = res
        }
        if let email = emailTf.text
        {
            tutor.email = email
        }
        if passTf.text == passrepTf.text && passTf.text != "password"
        {
            tutor.password = passTf.text
        }
        else if passTf.text == "password"
        {
            
        }
        else
        {
            showToast(message: "رمز عبور با تکرار آن برابر نیست", bgColor: UIColor.red)
            return
        }
        spinner.isHidden = false
        spinner.startAnimating()
        TutorService.getInstance().editTutor(tutor: tutor).subscribe(onNext:{res in
            if res.success! == 1
            {
                self.showToast(message: "تغییرات با موفقیت ذخیره شد", bgColor: GlobalProperties.PrimaryDarkColor)
            }
            else
            {
                self.showToast(message: res.message! , bgColor: UIColor.red)
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating();
        })
        

        
    }
    

}
