//
//  TT_TransactionsPage.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//


import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework
import FontAwesomeKit
import NVActivityIndicatorView

class TT_TransactionsPage: SwiftyOnboardPage, UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {
   
    var page = 0;
    var endedQuestions:[EndedQuestion] = []
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    @IBOutlet weak var tableview: UITableView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TT_TransactionsPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func initUI()
    {
        tableview.register(UINib(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: "TransactionCell")
        tableview.delegate = self
        tableview.dataSource = self

        loadEndedQuestion(page: page)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return endedQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "TransactionCell",for: indexPath) as! TransactionCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.card.cornerRadiusPreset = .cornerRadius2
        
        let q = endedQuestions[indexPath.row];

    
        cell.titleLbl.text = (q.question?.grade?.description)! + "-" + (q.question?.questionTopic?.description)!
        cell.descLbl.text = "\(Int(q.bid! * Float(q.credit!) * Float(GlobalProperties.CreditValue)))".replacedEnglishDigitsWithPersian + "تومان" + " - " + (q.endedQuestionState?.description)!
        
        cell.descImg.image = UIImage(named: "desc")?.tint(with: GlobalProperties.PrimaryColor)
        cell.titleImg.image = UIImage(named: "money")?.tint(with: GlobalProperties.PrimaryText)
        
        var date = Date(timeIntervalSince1970: q.creationDate! / 1000)
        
        cell.leftBottomLblb.text = date.relativeTime.replacedEnglishDigitsWithPersian
        return cell
    }
    
    func loadEndedQuestion(page:Int)
    {
        spinner.isHidden = false
        spinner.startAnimating()
        EndedQuestionService.getInstance().getEndedQuestionByResponder(page: page)?.subscribe(onNext:{res in
            if page == 0
            {
                self.endedQuestions = res
            }
            else
            {
                for q in res
                {
                    self.endedQuestions.append(q)
                }
            }
            self.tableview.reloadData()
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }
    
}
