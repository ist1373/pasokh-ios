//
//  FinancialOverlay.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation




import UIKit
import SwiftyOnboard
import Material
import Segmentio
import FontAwesomeKit





public class FinancialOverlay: SwiftyOnboardOverlay {
    
    open weak var delegate: OverlayPageDelegate?
    var segmentioView: Segmentio!
    var toolbar: Toolbar!
    var swiftyOnboard: CustomSwiftyOnboard?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "FinancialOverlay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    
    func initTopTab()
    {
        
        
        let segmentioViewRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 105)
        segmentioView = Segmentio(frame: segmentioViewRect)
        
        let toolbarViewRect = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 50)
        toolbar = Toolbar(frame: toolbarViewRect)
        self.initToolbar()
    
        // segmentioView.semanticContentAttribute = .forceRightToLeft
        self.addSubview(segmentioView)
        self.addSubview(toolbar)
        
        var transactionItem = SegmentioItem(
            title: "درآمد ها",
            image: nil
            
        )
        var finDetailsItem = SegmentioItem(
            title: "مشخصات حساب",
            image: nil
        )
        var paymentItem = SegmentioItem(
            title: "تسویه حساب ها",
            image: nil
        )

        
        let op1 = SegmentioIndicatorOptions(
            type: .bottom,
            ratio: 1,
            height: 2,
            color: .orange
        )
        let op2 = SegmentioHorizontalSeparatorOptions(
            type: SegmentioHorizontalSeparatorType.topAndBottom, // Top, Bottom, TopAndBottom
            height: 0.7,
            color: .lightGray
        )
        let op3 = SegmentioVerticalSeparatorOptions(
            ratio: 0.1, // from 0.1 to 1
            color: .clear
        )
        let op4 = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear,
                titleFont: GlobalProperties.getIranSansFont(size: 12),
                titleTextColor: UIColor.darkText
            ),
            selectedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: GlobalProperties.getIranSansFont(size: 12),
                titleTextColor: UIColor.darkText
            ),
            highlightedState: SegmentioState(
                backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                titleFont: GlobalProperties.getIranSansFont(size: 12),
                titleTextColor: UIColor.darkText
            )
        )
        
        let op6 = SegmentioOptions(backgroundColor: .white, maxVisibleItems: 3, scrollEnabled: true, indicatorOptions: op1, horizontalSeparatorOptions: op2, verticalSeparatorOptions: op3, imageContentMode: .center, labelTextAlignment: .center, labelTextNumberOfLines: 1, segmentStates: op4, animationDuration: 0)
        
        //segmentioView.addBadge(at: 0, count: 5, color: GlobalProperties.blueColor)
        
        segmentioView.setup(
            content: [ paymentItem,finDetailsItem , transactionItem],
            style: SegmentioStyle.imageOverLabel,
            options: op6
        )
        
        
        segmentioView.valueDidChange = { segmentio, segmentIndex in
            print("Selected item: ", segmentIndex)
            self.swiftyOnboard?.goToPage(index: segmentIndex, animated: true)
            
            
        }

    }
    func initToolbar()  {
       // UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
        
        toolbar.depthPreset = .none
        //toolbar.backgroundColor = GlobalProperties.PrimaryColor
        toolbar.titleLabel.font = GlobalProperties.getIranSansFont(size: 14)
        toolbar.titleLabel.text = "امور مالی";
        toolbar.titleLabel.textColor = UIColor.darkText
        toolbar.layer.zPosition = 10;
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: UIColor.darkText)
        
        let backBtn = IconButton(image: backImg?.tint(with: UIColor.darkText), tintColor: UIColor.darkText)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        toolbar.rightViews = [backBtn]
        
    }
    
    
    
    @objc func back()
    {
        self.parentViewController?.dismiss(animated: true, completion: nil)
    }
}

