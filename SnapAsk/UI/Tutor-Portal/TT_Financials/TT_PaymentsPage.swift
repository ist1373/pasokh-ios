//
//  TT_PaymentsPage.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//



import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework


class TT_PaymentsPage: SwiftyOnboardPage, UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var headerCard: Card!
    
    @IBOutlet weak var paymentBtn: RaisedButton!
    @IBOutlet weak var moneyLbl: UILabel!
    
    var page = 0;
    var settlements:[Settlement] = []
    
    @IBOutlet weak var tableview: UITableView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TT_PaymentsPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func initUI()
    {
                tableview.register(UINib(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: "TransactionCell")
                tableview.delegate = self
                tableview.dataSource = self
                paymentBtn.cornerRadiusPreset = .cornerRadius4
        headerCard.depthPreset = .depth2
                getSettlements()
                getAvailableMoney()
    }
    
    func getAvailableMoney()
    {
        if let observer = SettlementService.getInstance().getAvailableMoney()
        {
            observer.subscribe(onNext:{res in
                print(res)
                if res.success! == 1
                {
                    self.moneyLbl.text = "\(res.message!) تومان"
                }
                else
                {
                    
                }
            })
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settlements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "TransactionCell",for: indexPath) as! TransactionCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth1
        cell.card.cornerRadiusPreset = .cornerRadius2
        
        let s = settlements[indexPath.row];
        
        cell.titleLbl.text = "\(s.amount!) تومان".replacedEnglishDigitsWithPersian
        cell.descLbl.text = s.settlementState?.description
        
        cell.descImg.image = UIImage(named: "money")?.tint(with: GlobalProperties.PrimaryColor)
        cell.titleImg.image = UIImage(named: "desc")?.tint(with: GlobalProperties.PrimaryText)
        
        var date = Date(timeIntervalSince1970: s.creationDate! / 1000)
        
        cell.leftBottomLblb.text = date.relativeTime.replacedEnglishDigitsWithPersian
        
        
        return cell
    }
    
    func getSettlements()
    {
        SettlementService.getInstance().getSettlementsByTutor(page: self.page)?.subscribe(onNext:{res in
            print(res)
            if self.page == 0
            {
                self.settlements = res
            }
            else
            {
                for q in res
                {
                    self.settlements.append(q)
                }
            }
            self.tableview.reloadData()
        })
    }
    
    
    @IBAction func onPayment(_ sender: Any) {
        SettlementService.getInstance().addSettlement().subscribe(onNext:{res in
            if res.success! == 1
            {
                self.parentViewController?.showToast(message: "درخواست شما با موفقیت ارسال شد", bgColor: UIColor.green)
                self.page = 0
                self.settlements = []
                self.getSettlements()
            }
            else{
                self.parentViewController?.showToast(message: res.message! , bgColor: UIColor.red)

            }
        })
        
    }
    
}
