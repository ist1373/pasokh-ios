//
//  TT_MainFinancialViewController.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Segmentio
import SwiftyOnboard

class TT_MainFinancialViewController:  UIViewController,SwiftyOnboardDataSource,SwiftyOnboardDelegate,OverlayPageDelegate {

    var transactionsPage: TT_TransactionsPage!
    var finDetailsPage: TT_FinDetailsPage!
    var paymentsPage: TT_PaymentsPage!
    
    var segmentioVeiw : Segmentio?
    var swiftyOnboard :CustomSwiftyOnboard? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        swiftyOnboard = CustomSwiftyOnboard(frame: view.frame)
        view.addSubview(swiftyOnboard!)
        swiftyOnboard?.dataSource = self
        swiftyOnboard?.delegate = self
        swiftyOnboard?.fadePages = false
        
        // Do any additional setup after loading the view.
    }
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: CustomSwiftyOnboard) -> Int {
        return 3
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: CustomSwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        var view:SwiftyOnboardPage?
 
        switch index {
        case 0:
            view = TT_PaymentsPage.instanceFromNib() as? TT_PaymentsPage
            paymentsPage = view as! TT_PaymentsPage
            paymentsPage.initUI()
        case 1:
            view = TT_FinDetailsPage.instanceFromNib() as? TT_FinDetailsPage
            finDetailsPage = view as! TT_FinDetailsPage
            finDetailsPage.initUI()
        case 2:
            view = TT_TransactionsPage.instanceFromNib() as? TT_TransactionsPage
            transactionsPage = view as! TT_TransactionsPage
            transactionsPage.initUI()
            
        default:
            view = TT_PaymentsPage.instanceFromNib() as? TT_PaymentsPage
            paymentsPage = view as! TT_PaymentsPage
            paymentsPage.initUI()
        }
        return view
    }

    func swiftyOnboardViewForOverlay(_ swiftyOnboard: CustomSwiftyOnboard) -> SwiftyOnboardOverlay?{
        let view = FinancialOverlay.instanceFromNib() as? FinancialOverlay
        view?.delegate = self
        view?.initTopTab()
        segmentioVeiw = view?.segmentioView
        segmentioVeiw?.selectedSegmentioIndex = 2
        swiftyOnboard.goToPage(index: 2, animated: false)
        view?.swiftyOnboard = swiftyOnboard;
        return view
    }
    
    func swiftyOnboard(_ swiftyOnboard: CustomSwiftyOnboard, currentPage index: Int) {
        print(index)
        segmentioVeiw?.selectedSegmentioIndex = index
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
