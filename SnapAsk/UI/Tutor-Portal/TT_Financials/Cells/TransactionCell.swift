//
//  TransactionCell.swift
//  SnapAsk
//
//  Created by iman on 7/23/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

class TransactionCell: UITableViewCell {

    @IBOutlet weak var card: PulseView!
    
    @IBOutlet weak var titleImg: UIImageView!
    
    @IBOutlet weak var descImg: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var leftBottomLblb: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
