//
//  TT_FinDetailsPage.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework
import AKMaskField
import NVActivityIndicatorView
import CameraViewController



class TT_FinDetailsPage: SwiftyOnboardPage , UploadCompleteDelegate , UIImagePickerControllerDelegate ,UINavigationControllerDelegate{

    var uploadedFile:UploadedFile?
    var tutor:Tutor?;
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
//
//
    @IBOutlet weak var nationalCodeTf: UITextField!
//
    @IBOutlet weak var postalCodeTf: UITextField!
//
    @IBOutlet weak var shabaCodeTf: AKMaskField!
//
    @IBOutlet weak var addressTf: UITextView!
//
    @IBOutlet weak var editBtn: RaisedButton!
//
    @IBOutlet weak var nationalImg: UIImageView!
    
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TT_FinDetailsPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func initUI()
    {
        FileService.getInstance().delegate = self
        self.parentViewController?.hideKeyboardWhenTappedAround()
        editBtn.cornerRadiusPreset = .cornerRadius2
        getUser()
    }
    
    func getUser()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        TutorService.getInstance().getAuthenticatedTutor().subscribe(onNext:{user in
            self.tutor = user
            self.fillData()
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }
    func fillData()
    {
        if let nc = tutor?.nationalCode
        {
            self.nationalCodeTf.text = nc
        }
        if let pc = tutor?.postalCode
        {
            self.postalCodeTf.text = pc
        }
        if let sb = tutor?.shabaCode
        {
            self.shabaCodeTf.text = sb
        }
        if let add = tutor?.address
        {
            self.addressTf.text = add
        }
        if let uf = tutor?.nationalCardImage
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((uf.id)!)))!
            nationalImg.kf.setImage(with: url)
    
        }
    }
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        if success
        {
            uploadedFile = UploadedFile()
            uploadedFile?.fileType = "Public"
            uploadedFile?.id = imageId
        }
        spinner.isHidden = true
        spinner.stopAnimating()
    }
    
  
    @IBAction func onEditImage(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        FileService.getInstance().delegate = self


        let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
        let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true) { [weak self] image, asset in

            if let img = image{
                self?.spinner.isHidden = false
                self?.spinner.startAnimating()

                self?.nationalImg.image = img
                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
            }
            self?.parentViewController?.dismiss(animated: true, completion: nil)
        }

        self.parentViewController?.present(imagePickerViewController, animated: true, completion: nil)

    }

    @IBAction func onSaveChanges(_ sender: Any) {
        let letters = CharacterSet.alphanumerics
        var template: String = ""
        for c in (self.shabaCodeTf.text?.characters)! {
            if c == "I" || c == "R" || c == " " || c == "*"
            {
                
            }
            else
            {
                template += String(c)
            }
        }
        print(template)
        print(nationalCodeTf.text)
        
        tutor?.shabaCode = template
        if let nc = nationalCodeTf.text
        {
           tutor?.nationalCode = nc
        }
        if let pc = postalCodeTf.text
        {
            tutor?.postalCode = pc
        }
        if let ad = addressTf.text
        {
            tutor?.address = ad
        }
        
        if let uf = uploadedFile
        {
            tutor?.nationalCardImage = uf
        }
        spinner.isHidden = false
        spinner.startAnimating()
        TutorService.getInstance().editTutor(tutor: tutor!).subscribe(onNext:{res in
            print(res)
            if res.success! == 1
            {
                self.parentViewController?.showToast(message: "اطلاعات شما با موفقیت ثبت شد", bgColor: UIColor.green)

            }
            else
            {
                self.parentViewController?.showToast(message: res.message!, bgColor: UIColor.red)
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
        
        
    }
    
}
