//
//  TT_TabBarViewController.swift
//  SnapAsk
//
//  Created by iman on 6/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

class TT_TabBarViewController: UITabBarController,UITabBarControllerDelegate  {

    var toolbar:Toolbar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 3;
        self.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        tabFrame.size.height = 56
        tabFrame.origin.y = self.view.frame.size.height - 56
        self.tabBar.frame = tabFrame
        self.tabBar.backgroundColor = .white
        self.tabBar.tintColor = GlobalProperties.PrimaryColor
        self.tabBar.depthPreset = .depth5
    }
    
//    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//
//
//        if let toolbar = toolbar
//        {
//            if self.selectedIndex == 3
//            {
//                toolbar.depthPreset = .none
//            }
//            else
//            {
//                toolbar.depthPreset = .depth2
//            }
//        }
//    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let toolbar = toolbar
        {
            if self.selectedIndex == 3
            {
                toolbar.depthPreset = .none
            }
            else
            {
                toolbar.depthPreset = .depth2
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
