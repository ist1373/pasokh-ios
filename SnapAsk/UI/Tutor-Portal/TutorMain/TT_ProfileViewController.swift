//
//  TT_ProfileViewController.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Material
import FontAwesomeKit
import IoniconsSwift


class TT_ProfileViewController: UIViewController {


    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    @IBOutlet weak var fullname: UILabel!
    
    @IBOutlet weak var bio: UILabel!
    
    @IBOutlet weak var edit_btn: IconButton!
    
    @IBOutlet weak var cardHeader: PulseView!
    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var email: AttrItem!
    
    @IBOutlet weak var skills: AttrItem!
    
    
    @IBOutlet weak var city: AttrItem!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getMyUser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.setRounded()
        
        //        fullname.attrIcon.image = FAKIonIcons.iosPersonOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        //        fullname.attrTitle.text = "نام و نام خانوادگی"
        
        edit_btn.image = FAKIonIcons.editIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).tint(with: UIColor.white)
        
        email.attrIcon.image = FAKIonIcons.iosEmailOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        email.attrTitle.text = "ایمیل"
        
        
        
        city.attrIcon.image = FAKIonIcons.iosLocationOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        city.attrTitle.text = "شهر"
        
        skills.attrIcon.image = FAKIonIcons.hammerIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        skills.attrTitle.text = "مهارت ها"
        
        
        // Do any additional setup after loading the view.
    }
    
    func getMyUser()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        TutorService.getInstance().getAuthenticatedTutor().subscribe(onNext:{res in
            self.fullname.text = ""
            if let temp = res.firstName
            {
                self.fullname.text = temp
            }
            if let temp = res.lastName
            {
                self.fullname.text = self.fullname.text! + " " + temp
            }
            
            
            if let email = res.email
            {
                self.email.attrLabel.text = email;
            }
            
            if let cityy = res.city
            {
                self.city.attrLabel.text = cityy.title
            }
            if let pi = res.profileImage
            {
                let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((pi.id)!)))!
                self.profileImage.kf.setImage(with: url)
            }
            
            if let b = res.biography
            {
                self.bio.text = b
            }
            
//            if let tops = res.topics
//            {
                var temp = ""
                for top in res.topics
                {
                    
                    temp += " " + (QuestionTopic(rawValue: top)?.description)!
                }
                self.skills.attrLabel.text = temp
//            }
            
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onEdit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "TT_EditProfile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TT_EditProfileViewController") as! TT_EditProfileViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ksldjflskdjflskjdfsd")
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    
}
