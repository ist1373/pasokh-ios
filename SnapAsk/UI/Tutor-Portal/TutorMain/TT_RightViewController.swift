//
//  TT_RighViewController.swift
//  SnapAsk
//
//  Created by iman on 6/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import FontAwesomeKit

class TT_RightViewController: UIViewController {

    var slideMenuController:SlideMenuController?
    
    
    @IBOutlet weak var back_btn: UIButton!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var logInOut: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var headerPart: UIView!
    
    @IBOutlet weak var exitItem: MenuItem!
    @IBOutlet weak var aboutusItem: MenuItem!
    @IBOutlet weak var rulesItem: MenuItem!
    @IBOutlet weak var contactusItem: MenuItem!
    @IBOutlet weak var shareItem: MenuItem!
    
//    @IBOutlet weak var purchaseItem: MenuItem!
    
    @IBOutlet weak var accountItem: MenuItem!
    
    @IBOutlet weak var financialItem: MenuItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        back_btn.layer.zPosition = 15
        headerPart.backgroundColor = GlobalProperties.PrimaryColor
        initMenuItems()
        

    }
    
    @IBAction func onBackTouch(_ sender: Any) {
        self.slideMenuController()?.closeRight()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    
    
    
    func initMenuItems() {
        
        shareItem.icon.image = UIImage(named:"share")?.tint(with: GlobalProperties.PrimaryText)
        shareItem.title.text = "معرفی به دوستان"
        shareItem.title.textColor = GlobalProperties.PrimaryText
        
        let cimage = FAKMaterialIcons.emailIcon(withSize: 30).image(with: CGSize(width:30,height:30))
        contactusItem.icon.image = cimage?.tint(with: GlobalProperties.PrimaryText)
        contactusItem.title.text = "ارتباط با ما"
        contactusItem.title.textColor = GlobalProperties.PrimaryText
        
        rulesItem.icon.image = UIImage(named:"rules")?.tint(with: GlobalProperties.PrimaryText)
        rulesItem.title.text = "قوانین و مقررات"
        let rulesItemGesture = UITapGestureRecognizer(target: self, action: #selector(TT_RightViewController.policiesTouch))
        rulesItem.addGestureRecognizer(rulesItemGesture)
        rulesItem.title.textColor = GlobalProperties.PrimaryText

        accountItem.icon.image = FAKMaterialIcons.accountOIcon(withSize: 40).image(with: CGSize(width:40,height:40)).tint(with: GlobalProperties.PrimaryText)
        accountItem.title.text = "مشخصات حساب"
        accountItem.title.textColor = GlobalProperties.PrimaryText
        accountItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TT_RightViewController.onEditProfile)))
        
        financialItem.icon.image = FAKMaterialIcons.moneyIcon(withSize: 50).image(with: CGSize(width:50,height:50)).tint(with: GlobalProperties.PrimaryText)
        financialItem.title.text = "امور مالی"
        financialItem.title.textColor = GlobalProperties.PrimaryText
        financialItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TT_RightViewController.onShowFinancials)))
        
        
//        purchaseItem.icon.image = FAKIonIcons.iosCartIcon(withSize: 40).image(with: CGSize(width:40,height:40)).tint(with: GlobalProperties.PrimaryText)
//        purchaseItem.title.text = "خرید سوال"
//        let purchaseItemGesture = UITapGestureRecognizer(target: self, action: #selector(TT_RightViewController.purchaseTouch))
//        purchaseItem.addGestureRecognizer(purchaseItemGesture)
//        purchaseItem.title.textColor = GlobalProperties.PrimaryText

        
        aboutusItem.icon.image = UIImage(named:"info")?.tint(with: GlobalProperties.PrimaryText)
        aboutusItem.title.text = "درباره پاسخ"
        let aboutusGusture = UITapGestureRecognizer(target:self,action: #selector(TT_RightViewController.aboutUsTouch))
        aboutusItem.addGestureRecognizer(aboutusGusture)
        aboutusItem.title.textColor = GlobalProperties.PrimaryText

        exitItem.icon.image = FAKIonIcons.androidExitIcon(withSize: 50).image(with: CGSize(width:50,height:50)).tint(with: GlobalProperties.PrimaryText)
        exitItem.title.text = "خروج"
        exitItem.isHidden = false;
        let exitRecognizer = UITapGestureRecognizer(target: self, action: #selector(onExitTouch))
        exitItem.addGestureRecognizer(exitRecognizer)
        exitItem.title.textColor = GlobalProperties.PrimaryText

        //        let exitRecognizer = UITapGestureRecognizer(target: self, action: #selector(RightDrawer.onExitTouch(sender:)))
        //        exitItem.addGestureRecognizer(exitRecognizer)
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            
            if let fn = user.firstName
            {
                username.text = fn
            }
            if let ln = user.lastName
            {
                username.text = username.text! + " " + ln
            }
            if (username.text?.characters.count)! < 2
            {
                username.text = "شماره موبایل"
            }
            if let uf = user.profileImage
            {
                let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((uf.id)!)))!
                avatarImage.kf.setImage(with: url)
                avatarImage.setRounded()
            }
            
            logInOut.text = user.mobileNumber!.replacedEnglishDigitsWithPersian
            exitItem.isHidden = false;
        }
    }
    
    @objc func onEditProfile()
    {
        let storyBoard = UIStoryboard(name: "TT_EditProfile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TT_EditProfileViewController") as! TT_EditProfileViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func aboutUsTouch()
    {
        let st = UIStoryboard()
        let storyboard: UIStoryboard = UIStoryboard(name: "RightDrawer", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func onShowFinancials()
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "tt_financial", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TT_MainFinancialViewController") as! TT_MainFinancialViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func policiesTouch()
    {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "RightDrawer", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PoliciesViewController") as! PoliciesViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func purchaseTouch()
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func onExitTouch()
    {
        try? AppDelegate.getStorage().removeAll()
        let st = UIStoryboard(name: "SignInUp", bundle: nil)
        let vc  = st.instantiateViewController(withIdentifier: "LoginViewController")
        vc.modalPresentationStyle = .currentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}
