//
//  TT_ContainerViewController.swift
//  SnapAsk
//
//  Created by iman on 6/29/18.
//  Copyright © 2018 iman. All rights reserved.
//
import UIKit
import SlideMenuControllerSwift
import Material
import FontAwesomeKit
import MaterialShowcase
import NVActivityIndicatorView


class TT_ContainerViewController: SlideMenuController , SlideMenuControllerDelegate {
    
    
    @IBOutlet weak var toolbar: Toolbar!
    
    var menuBtn:IconButton? = nil;
    var menuImgClose:UIImage? = nil;
    var menuImgOpen:UIImage? = nil;
    
    
    
    override func awakeFromNib() {
        
        //        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        //        let statusBarColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        //        statusBarView.backgroundColor = statusBarColor
        //        statusBarView.layer.zPosition = 10
        //        view.addSubview(statusBarView)
        
        
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.rightViewWidth  = self.view.frame.width * 0.7;
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.simultaneousGestureRecognizers = true
        SlideMenuOptions.panGesturesEnabled = true
        SlideMenuOptions.tapGesturesEnabled = true
        
        
        
        toolbar.depthPreset = .none
        toolbar.titleLabel.font = UIFont(name: "Mj_Flow Bold", size: 15)
        toolbar.titleLabel.text = "پاسخ"
        toolbar.titleLabel.textColor = GlobalProperties.PrimaryDarkColor
        toolbar.layer.zPosition = 10;
        
        
        //        let addBookImg = UIImage(named:"ic_add_book");
        //        let addBookBtn = IconButton(image: ResizeImage(image: addBookImg!, targetSize: CGSize(width: 25, height: 25)), tintColor: .white)
        //        addBookBtn.pulseColor = .white
        
        menuImgClose = FAKIonIcons.androidMenuIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        menuImgOpen = FAKIonIcons.androidArrowForwardIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        
        menuBtn = IconButton(image: menuImgClose?.tint(with: GlobalProperties.PrimaryDarkColor), tintColor: GlobalProperties.PrimaryDarkColor)
        menuBtn?.pulseColor = .white
        menuBtn?.addTarget(self, action: #selector(handleMenuBtn(button:)), for: .touchUpInside)
        
        
        // toolbar.leftViews = [addBookBtn]
        toolbar.rightViews = [menuBtn!]
        
        self.delegate = self;
        
        
        
        
        
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TT_TabBarViewController") {
            self.mainViewController = controller
            (controller as! TT_TabBarViewController).toolbar = toolbar
     
            
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TT_RightViewController") {
            self.rightViewController = controller
            let rvc = controller as! TT_RightViewController
            rvc.slideMenuController = self.slideMenuController()
        }
        super.awakeFromNib()
        
        
        if let t = try? AppDelegate.getStorage().object(ofType: Bool.self, forKey: "tt-tutorial-end")
        {
            if t {
                
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                    self.showTutorialCaseMenue()
                }
                
            }
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                self.showTutorialCaseMenue()
            }
        }
        
    }
    
    func showTutorialCaseMenue() {
        
        
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: menuBtn!) // always required to set targetView
        showcase.primaryText = "منوی کناری"
        showcase.secondaryText = "برای ورود، ثبت نام و یا مشاهده قابلیت های برنامه، منوی کناری را باز کنید"
        
        
        
        // Background
        showcase.backgroundPromptColor = GlobalProperties.PrimaryDarkColor
        showcase.backgroundPromptColorAlpha = 0.95
        showcase.backgroundViewType = .circle
        
        // Target
        showcase.targetTintColor = GlobalProperties.PrimaryDarkColor
        showcase.targetHolderRadius = 44
        showcase.targetHolderColor = UIColor.clear
        // Text
        showcase.primaryTextColor = UIColor.white
        showcase.secondaryTextColor = UIColor.white
        showcase.primaryTextSize = 20
        showcase.secondaryTextSize = 15
        showcase.primaryTextFont = GlobalProperties.getIranSansFont(size: 15)
        showcase.secondaryTextFont = GlobalProperties.getIranSansFont(size: 13)
        //Alignment
        showcase.primaryTextAlignment = .right
        showcase.secondaryTextAlignment = .right
        // Animation
        showcase.aniComeInDuration = 0.5 // unit: second
        showcase.aniGoOutDuration = 0.5 // unit: second
        showcase.aniRippleScale = 1.5
        showcase.aniRippleColor = UIColor.white
        showcase.aniRippleAlpha = 0.2

        
        showcase.show(completion: {
            try? AppDelegate.getStorage().setObject(true, forKey: "tt-tutorial-end" , expiry: .never)
        })
    }
    
    //    func onChangeItem(index: Int) {
    ////        var statusBarView:UIView?
    //        if(index == 3)
    //        {
    //            toolbar.isHidden = true
    //            UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
    //
    //        }
    //        else
    //        {
    ////            if let v = statusBarView
    ////            {
    ////                v.removeFromSuperview()
    ////            }
    //            UIApplication.shared.statusBarView?.backgroundColor = .white
    //            toolbar.isHidden = false
    //        }
    //    }
    
    @objc
    func handleMenuBtn(button: UIButton) {
        print("cliccccccc")
        if (self.slideMenuController()?.isRightOpen())!
        {
            self.slideMenuController()?.closeRight()
        }
        else
        {
            self.slideMenuController()?.openRight()
        }
        
    }
    
    func rightWillClose() {
        menuBtn?.image = menuImgClose
    }
    func rightWillOpen() {
        menuBtn?.image = menuImgOpen
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        UIApplication.shared.statusBarStyle = .default
    //    }
    //    override var preferredStatusBarStyle : UIStatusBarStyle {
    //        return .lightContent
    //    }

}
