//
//  TT_TimeLineViewController.swift
//  SnapAsk
//
//  Created by iman on 7/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class TT_TimeLineViewController: UIViewController , UIScrollViewDelegate{

    var timelines:[Timeline] = []
    var page = 0;
    var loadingData = false
    
    @IBOutlet weak var timeline: TimelineUI!
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        timeline.bubbleColor = GlobalProperties.PrimaryColor
        timeline.titleColor = .white
        timeline.descriptionColor = .darkText
        timeline.pointDiameter = 10.0
        timeline.showsVerticalScrollIndicator = false
        timeline.showsHorizontalScrollIndicator = false
        timeline.bubbleArrows = false
        timeline.lineWidth = 3.0
        timeline.bubbleRadius = 10.0
        timeline.delegate = self
        
        
        loadTimeline()
        
        
    }
    
    func loadTimeline()  {
        spinner.isHidden = false
        spinner.startAnimating()
        TimelineService.getInstance().getTimeline(page: page).subscribe(onNext:{res in
            for tl in res {
                var date = Date(timeIntervalSince1970: tl.creationDate! / 1000)
                let point = Point(title: date.relativeTime.replacedEnglishDigitsWithPersian)
                point.fill = true
                point.lineColor = GlobalProperties.PrimaryColor
                point.pointColor = point.lineColor
                
                
                if tl.eventType == EventType.NewQuestion
                {
                    if let qt = tl.questionTopic
                    {
                        point.description = "یک سوال در زمینه ی \(qt.description) پرسیده شد"
                    }
                }
                else if tl.eventType == EventType.TakenStar
                {
                    point.description = "\(tl.responder?.firstName != nil ? (tl.responder?.firstName)! : "" ) \(tl.responder?.lastName != nil ? (tl.responder?.lastName)! : "") امتیاز \((tl.star)!) دریافت کرد"
                    
                }
                else if tl.eventType == EventType.NewAnswer
                {
                    point.description = "\(tl.responder?.firstName != nil ? (tl.responder?.firstName)! : "" ) \(tl.responder?.lastName != nil ? (tl.responder?.lastName)! : "") به یک سوال در زمینه \((tl.questionTopic?.description)!) پاسخ داد"
                    
                }
                
                
                
                self.timeline.points.append(point)
            }
            if res.count > 0
            {
                self.loadingData = false
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var scrollViewHeight = scrollView.frame.size.height;
        var scrollContentSizeHeight = scrollView.contentSize.height;
        var scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
        {
            if !loadingData {
                page += 1
                spinner.isHidden = false
                spinner.startAnimating()
                loadingData = true
                loadTimeline()
            }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}
