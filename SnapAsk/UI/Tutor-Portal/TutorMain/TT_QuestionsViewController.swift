//
//  TT_QuestionsViewController.swift
//  SnapAsk
//
//  Created by iman on 6/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Segmentio
import SwiftyOnboard


class TT_QuestionsViewController: UIViewController,SwiftyOnboardDataSource,SwiftyOnboardDelegate,OverlayPageDelegate {


    var directQuestionsPage: DirectQuestionsPage!
    var publicQuestionsPage: PublicQuestionsPage!
    //    var bookOwnerPage:BookOwnerPage!
    
    
    var segmentioVeiw : Segmentio?
    var swiftyOnboard :CustomSwiftyOnboard? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        swiftyOnboard = CustomSwiftyOnboard(frame: view.frame)
        view.addSubview(swiftyOnboard!)
        swiftyOnboard?.dataSource = self
        swiftyOnboard?.delegate = self
        swiftyOnboard?.fadePages = false
        //swiftyOnboard?.semanticContentAttribute = .forceRightToLeft
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: CustomSwiftyOnboard) -> Int {
        return 2
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: CustomSwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        var view:SwiftyOnboardPage?
        print(index , "pageeeeee")
        switch index {
        case 0:
            view = DirectQuestionsPage.instanceFromNib() as? DirectQuestionsPage
            directQuestionsPage = view as! DirectQuestionsPage
            directQuestionsPage.initUI()
        case 1:
            view = PublicQuestionsPage.instanceFromNib() as? PublicQuestionsPage
            publicQuestionsPage = view as! PublicQuestionsPage
            publicQuestionsPage.initUI()

        default:
            view = DirectQuestionsPage.instanceFromNib() as? DirectQuestionsPage
            directQuestionsPage = view as! DirectQuestionsPage
            directQuestionsPage.initUI()
        }
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: CustomSwiftyOnboard) -> SwiftyOnboardOverlay?{
        let view = QuestionsOverlayPage.instanceFromNib() as? QuestionsOverlayPage
        view?.delegate = self
        view?.initTopTab()
        segmentioVeiw = view?.segmentioView
        segmentioVeiw?.selectedSegmentioIndex = 1
        swiftyOnboard.goToPage(index: 1, animated: false)
        view?.swiftyOnboard = swiftyOnboard;
        return view
    }
    
  
    func swiftyOnboard(_ swiftyOnboard: CustomSwiftyOnboard, currentPage index: Int) {
        print(index)
        segmentioVeiw?.selectedSegmentioIndex = index
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

enum SegmentioPosition {
    case dynamic
    case fixed(maxVisibleItems: Int)
}
