//
//  QuestionsOverlayPage.swift
//  SnapAsk
//
//  Created by iman on 6/30/18.
//  Copyright © 2018 iman. All rights reserved.
//


import UIKit
import SwiftyOnboard
import Material
import Segmentio





public class QuestionsOverlayPage: SwiftyOnboardOverlay {
    
    open weak var delegate: OverlayPageDelegate?
    var segmentioView: Segmentio!
    var swiftyOnboard: CustomSwiftyOnboard?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "QuestionsOverlayPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    
    func initTopTab()
    {
        
        
        let segmentioViewRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 115)
        segmentioView = Segmentio(frame: segmentioViewRect)
       // segmentioView.semanticContentAttribute = .forceRightToLeft
        self.addSubview(segmentioView)
        
        var publicItem = SegmentioItem(
            title: "سوالات عمومی",
            image: nil
            
        )
        var directItem = SegmentioItem(
            title: "سوالات خصوصی",
            image: nil
        )
        
       

        
        let op1 = SegmentioIndicatorOptions(
            type: .bottom,
            ratio: 1,
            height: 2,
            color: .orange
        )
        let op2 = SegmentioHorizontalSeparatorOptions(
            type: SegmentioHorizontalSeparatorType.topAndBottom, // Top, Bottom, TopAndBottom
            height: 0.7,
            color: .lightGray
        )
        let op3 = SegmentioVerticalSeparatorOptions(
            ratio: 0.1, // from 0.1 to 1
            color: .clear
        )
        let op4 = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear,
                titleFont: GlobalProperties.getIranSansFont(size: 14),
                titleTextColor: GlobalProperties.PrimaryDarkColor
            ),
            selectedState: SegmentioState(
                backgroundColor: .clear,
                titleFont: GlobalProperties.getIranSansFont(size: 14),
                titleTextColor: GlobalProperties.PrimaryDarkColor
            ),
            highlightedState: SegmentioState(
                backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                titleFont: GlobalProperties.getIranSansFont(size: 14),
                titleTextColor: GlobalProperties.PrimaryDarkColor
            )
        )
        
        let op6 = SegmentioOptions(backgroundColor: .white, maxVisibleItems: 2, scrollEnabled: true, indicatorOptions: op1, horizontalSeparatorOptions: op2, verticalSeparatorOptions: op3, imageContentMode: .center, labelTextAlignment: .center, labelTextNumberOfLines: 1, segmentStates: op4, animationDuration: 0)
        
        //segmentioView.addBadge(at: 0, count: 5, color: GlobalProperties.blueColor)
        
        segmentioView.setup(
            content: [directItem,publicItem],
            style: SegmentioStyle.imageOverLabel,
            options: op6
        )
        
        
        segmentioView.valueDidChange = { segmentio, segmentIndex in
            print("Selected item: ", segmentIndex)
            self.swiftyOnboard?.goToPage(index: segmentIndex, animated: true)
            
        
        }
        
    
        
        
        
    }
}

