//
//  QuestionsType1Page.swift
//  SnapAsk
//
//  Created by iman on 6/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework
import NVActivityIndicatorView



class DirectQuestionsPage: SwiftyOnboardPage, UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource , BidChangeDelegate {

    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    var page = 0;
    var questions:[Question] = []
    var loadingData = false
    
    
    @IBOutlet weak var tableview: UITableView!
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "DirectQuestionsPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }

    func initUI()
    {
        tableview.register(UINib(nibName: "DirectQuestionCell", bundle: nil), forCellReuseIdentifier: "DirectQuestionCell")
        tableview.delegate = self
        tableview.dataSource = self
        
        loadDirectQuestion()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "DirectQuestionCell",for: indexPath) as! DirectQuestionCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.acceptBtn.cornerRadiusPreset = .cornerRadius6
        cell.reportBtn.cornerRadiusPreset = .cornerRadius6
        cell.rejectBtn.cornerRadiusPreset = .cornerRadius6
        cell.delegate = self
        cell.index = indexPath.row
        
        let q = questions[indexPath.row];
        cell.questionId = q.id!
        
        if let img = q.image
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((img.id)!)))!
            cell.questionImg.kf.setImage(with: url)
            cell.imgId = img.id!
            
        }
        cell.questionTitle.text = q.description
        cell.initUI()
        
        cell.questionImg.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = questions.count - 1
        
        if !loadingData && indexPath.row == lastElement {
            print("page:\(page) ")
            page += 1
            spinner.isHidden = false
            spinner.startAnimating()
            loadingData = true
            loadDirectQuestion()
            
        }
    }

    
    
    func loadDirectQuestion()
    {
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        QuestionService.getInstance().getDirectQuestions(page: page)?.subscribe(onNext:{res in
            if self.page == 0
            {
                self.questions = res
            }
            else
            {
                for q in res
                {
                    self.questions.append(q)
                }
                if res.count > 0 {
                    self.loadingData = false
                }
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            
            self.tableview.reloadData()
        })
    }
    
    func onBidChange(_ value: Float, cell: QuestionBidCell) {
        
    }
    
    func onQuestionDelete(index: Int) {
        
        self.questions.remove(at: index)
        tableview.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.fade)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableview.reloadData()
        }
    }
    
}




