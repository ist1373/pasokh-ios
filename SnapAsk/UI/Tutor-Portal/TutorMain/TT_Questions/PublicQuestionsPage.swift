//
//  PublicQuestionsPage.swift
//  SnapAsk
//
//  Created by iman on 6/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework
import Kingfisher
import NVActivityIndicatorView

class PublicQuestionsPage: SwiftyOnboardPage, UIScrollViewDelegate , UITableViewDataSource , UITableViewDelegate, BidChangeDelegate {

    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    @IBOutlet weak var messageLbl: UILabel!
    
    
    @IBOutlet weak var tableview: UITableView!
    
    var questions:[Question] = []
    var page = 0;
    var loadingData = false

    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "QuestionBidCell",for: indexPath) as! QuestionBidCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.bidRegister.cornerRadiusPreset = .cornerRadius6
        cell.index = indexPath.row
        
        let q = questions[indexPath.row];
        cell.questionId = q.id!
        if let img = q.image
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((img.id)!)))!
            cell.questionImage.kf.setImage(with: url)
            cell.imgId = img.id!
        }
        cell.questionDesc.text = q.description
        cell.bidValue.text = " \(Int(250 * questions[cell.index!].credit! )) تومان".replacedEnglishDigitsWithPersian
        cell.initUI()
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = questions.count - 1
        
        if !loadingData && indexPath.row == lastElement {
            print("page:\(page) ")
            page += 1
            spinner.isHidden = false
            spinner.startAnimating()
            loadingData = true
            loadPublicQuestion()
            
        }
    }
    
    
    func onBidChange(_ value: Float, cell: QuestionBidCell) {
        
        cell.bidValue.text = " \(Int(value * Float(questions[cell.index!].credit!) * 500)) تومان".replacedEnglishDigitsWithPersian
    }

  
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "PublicQuestionsPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func initUI()
    {
        tableview.register(UINib(nibName: "QuestionBidCell", bundle: nil), forCellReuseIdentifier: "QuestionBidCell")
        tableview.delegate = self
        tableview.dataSource = self
        loadPublicQuestion()
    }
    
    func loadPublicQuestion()
    {
        print("loaddddddddddd")
        spinner.isHidden = false
        spinner.startAnimating()
        QuestionService.getInstance().getPublicQuestions(page: page)?.subscribe(onNext:{res in
            print(res)
            if self.page == 0
            {
                self.questions = res
            }
            else
            {
                for q in res
                {
                    self.questions.append(q)
                }
                if res.count > 0 {
                    self.loadingData = false
                }
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.tableview.reloadData()
            if(self.questions.count == 0)
            {
                self.messageLbl.isHidden = false
                self.messageLbl.text = "پیشنهاد مرتبطی وجود ندارد"
            }
        },onError:{error in
            print("errorrrrrrrr")
            print(type(of: error))
            if  error is ResponseError
            {
                self.messageLbl.isHidden = false
                self.messageLbl.text = (error as! ResponseError).message
            }
            if  error is URLError
            {
                print((error as! URLError).code)
                if (error as! URLError).errorCode == -1004
                {
                self.messageLbl.isHidden = false
                self.messageLbl.text = "خطا در برقراری ارتباط با سرور"
                }

            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }
   
    func onQuestionDelete(index:Int) {

        self.questions.remove(at: index)
        tableview.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.fade)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableview.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
//        tableview.reloadData()
    }
    
    
    
}
