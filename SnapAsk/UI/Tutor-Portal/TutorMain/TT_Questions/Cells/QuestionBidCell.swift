//
//  QuestionBidCell.swift
//  SnapAsk
//
//  Created by iman on 6/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Material

protocol BidChangeDelegate:class {
    func onBidChange(_ value:Float,cell:QuestionBidCell);
    func onQuestionDelete(index:Int);
}

class QuestionBidCell:UITableViewCell
{
    weak var delegate :BidChangeDelegate?

    @IBOutlet weak var card: Card!
    
    @IBOutlet weak var bidSlider: UISlider!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var questionDesc: UILabel!
    @IBOutlet weak var bidValue: UILabel!
    @IBOutlet weak var bidRegister: RaisedButton!
    @IBOutlet weak var reportBtn: UIButton!
    
    var credit : Float = 0.5;
    
    var index:Int?
    var questionId:Int64 = 0
    var imgId:Int = 0
    
    @IBAction func onChangeBid(_ sender: Any) {
        bidSlider.setValue((round(bidSlider.value / 0.125) * 0.125), animated: true)
        delegate?.onBidChange(bidSlider.value, cell: self)
        credit = bidSlider.value
    }
    
    @IBAction func onSendBid(_ sender: Any) {
        print("questionId:\(questionId)","credit:\(credit)")
        BidService.getInstance().addBid(questionId: questionId, credit: credit).subscribe(onNext:{res in
            if res.success! == 1
            {
                 self.parentViewController?.showToast(message: "پیشنهاد شما با موفقیت ارسال شد",bgColor:GlobalProperties.PrimaryColor)
                self.delegate?.onQuestionDelete(index: self.index!)
            }
            else
            {
                self.parentViewController?.showToast(message: res.message!)
            }

        })
    }
    
    
    @IBAction func onShowReport(_ sender: Any) {
        
        let vc = self.parentViewController?.storyboard?.instantiateViewController(withIdentifier: "QuestionReportViewController") as! QuestionReportViewController
        vc.modalPresentationStyle = .overCurrentContext;
        vc.questionId = Int(questionId)
        vc.index = self.index
        vc.delegate = self.delegate;
        self.parentViewController?.present(vc, animated: false, completion: nil)
    }
    
    func initUI()
    {
        questionImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(onShowImage(_:))))
    }
    
    @objc func onShowImage(_ sender:UIImageView)
    {
        let sb = UIStoryboard(name: "ImageView", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ImageViewController")
        (vc as! ImageViewController).imgId = imgId
        self.parentViewController?.present(vc, animated: true, completion: nil)
    }
    
    
}
