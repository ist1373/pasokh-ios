//
//  DirectQuestionCell.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Material


class DirectQuestionCell:UITableViewCell
{
    
    weak var delegate :BidChangeDelegate?
    
    @IBOutlet weak var card: Card!
    @IBOutlet weak var questionImg: UIImageView!
    @IBOutlet weak var questionTitle: UILabel!
    @IBOutlet weak var acceptBtn: RaisedButton!
    @IBOutlet weak var rejectBtn: RaisedButton!
    @IBOutlet weak var reportBtn: RaisedButton!
    
    var index:Int?
    var questionId:Int64 = 0
    var imgId:Int = 0
    func initUI()
    {
        questionImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(onShowImage(_:))))
    }
    
    @objc func onShowImage(_ sender:UIImageView)
    {
        let sb = UIStoryboard(name: "ImageView", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ImageViewController")
        (vc as! ImageViewController).imgId = imgId
        self.parentViewController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAccept(_ sender: Any) {
        BidService.getInstance().addDirectBid(questionId: questionId , accept: true).subscribe(onNext:{res in
            if res.success! == 1 
            {
                self.parentViewController?.showToast(message: "این سوال توسط شما پذیرفته شده",bgColor:GlobalProperties.PrimaryColor)
                self.delegate?.onQuestionDelete(index: self.index!)
            }
            else
            {
                self.parentViewController?.showToast(message: res.message!)
            }
            
        })
    }
    
    
    @IBAction func onReject(_ sender: Any) {
        BidService.getInstance().addDirectBid(questionId: questionId , accept: false).subscribe(onNext:{res in
            if res.success! == 1
            {
                self.parentViewController?.showToast(message: "این سوال توسط شما رد شد",bgColor:GlobalProperties.PrimaryColor)
                self.delegate?.onQuestionDelete(index: self.index!)
            }
            else
            {
                self.parentViewController?.showToast(message: res.message!)
            }
            
        })
    }
    
    @IBAction func onReport(_ sender: Any) {
        let vc = self.parentViewController?.storyboard?.instantiateViewController(withIdentifier: "QuestionReportViewController") as! QuestionReportViewController
        vc.modalPresentationStyle = .overCurrentContext;
        vc.questionId = Int(questionId)
        vc.delegate = self.delegate
        vc.index = self.index
        self.parentViewController?.present(vc, animated: false, completion: nil)
    }
    
    
}








