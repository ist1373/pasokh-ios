//
//  QuestionReportViewController.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import DropDown
import NVActivityIndicatorView

class QuestionReportViewController: UIViewController {

    var questionReport = QuestionReport()
    
    @IBOutlet weak var report_tf: UITextField!
    @IBOutlet weak var desc_tf: UITextField!
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    var delegate :BidChangeDelegate?
    var index:Int?
    
    let dropDownReport = DropDown()
    var questionId:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        report_tf.setAsDropDown()
        report_tf.tintColor = .clear
        report_tf.addTarget(self, action: #selector(onEditBegining), for: .editingDidBegin)
        // Do any additional setup after loading the view.
      
        
        
        dropDownReport.anchorView = report_tf
        
        dropDownReport.dataSource = [ReportType.IrrevelentQuestion.description , ReportType.ManyQuestion.description ,ReportType.NotQuestion.description , ReportType.Other.description]
        
        dropDownReport.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .right
            cell.optionLabel.font = GlobalProperties.getIranSansFont(size: 13)
        }
        
        dropDownReport.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.report_tf.text = item
            switch item {
            case ReportType.IrrevelentQuestion.description:
                self.questionReport.reportType = ReportType.IrrevelentQuestion
                break
            case  ReportType.ManyQuestion.description:
                self.questionReport.reportType = ReportType.ManyQuestion
                break
            case ReportType.NotQuestion.description:
                self.questionReport.reportType = ReportType.NotQuestion
                break
            case ReportType.Other.description:
                self.questionReport.reportType = ReportType.Other
                break
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSend(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        
        questionReport.description = desc_tf.text
        let q = Question()
        q.id = Int64(questionId!)
        questionReport.question = q
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            questionReport.tutor = u;
        }
        if(questionReport.reportType == nil)
        {
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.showToast(message: "لطفا موضوع گزارش را انتخاب کنید", bgColor: UIColor.red)
            return
        }
        QuestionReportService.getInstance().addQuestionReport(questionReport: questionReport).subscribe(onNext:{res in
            if res.success! == 1
            {
                self.delegate?.onQuestionDelete(index: self.index!)
                self.showToast(message: "گزارش شما با موفقیت ارسال شد", bgColor: GlobalProperties.PrimaryColor)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.showToast(message: res.message!, bgColor: UIColor.red)
            }
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
        
        
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func onEditBegining()
    {
        self.view.endEditing(true)
        dropDownReport.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
