//
//  TT_ChatroomsViewController.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TT_ChatroomsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var spinner: NVActivityIndicatorView!
    @IBOutlet weak var tableview: UITableView!
    
    var questions:[Question] = []
    var page = 0
    var loadingData = false
    
    @IBOutlet weak var message: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.register(UINib(nibName: "QuestionCell", bundle: nil), forCellReuseIdentifier: "QuestionCell")
        tableview.delegate = self
        tableview.dataSource = self
       
        
        loadChatrooms()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadChatrooms()
    {
        QuestionService.getInstance().getQuestionsByTutor(page: page)?.subscribe(onNext:{res in
            if self.page == 0
            {
                self.questions = res
                if res.count == 0
                {
                    self.message.isHidden = false
                    self.message.text = "اتاق گفتگویی برای شما وجود ندارد"
                }
            
            }
            else{
                for ch in res
                {
                    self.questions.append(ch)
                }
                
                if res.count > 0
                {
                    self.loadingData = false;
                }
                
            }
            self.tableview.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "QuestionCell",for: indexPath) as! QuestionCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth1
        let q = questions[indexPath.row];
        cell.title.text = q.description;
        cell.selectionStyle = .none
        
        cell.state.text = "\((q.grade?.description)!) - \((q.questionTopic?.description)!)"
        
        if let rate = q.rate
        {
            cell.ratingView.isHidden = false
            cell.ratingView.rating = Double(q.rate!)
        }
        else {
            cell.ratingView.isHidden = true
        }
//        if (q.questionState == QuestionState.Pending)
//        {
//            cell.state.textColor = GlobalProperties.blueColor
//            cell.state.text = "در حال بررسی"
//            cell.ratingView.isHidden = true
//
//        }
//        else if q.questionState == QuestionState.Finished
//        {
//            cell.ratingView.isHidden = false
//            cell.state.text = "پاسخ دهنده:\((q.responder?.firstName)!) \((q.responder?.lastName)!)"
//            cell.state.textColor = GlobalProperties.greenColor
//            cell.ratingView.rating = Double(q.rate!)
//        }
//        else
//        {
//            cell.ratingView.isHidden = true
//            cell.state.text = "پاسخ دهنده:\((q.responder?.firstName)) \((q.responder?.lastName))"
//            cell.state.textColor = GlobalProperties.greenColor
//        }
        
        if let img = q.image
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((img.id)!)))!
            cell.questionImage.kf.setImage(with: url)
        }
        
        cell.date.text = PersianTool.getDateStringFromTimeStamp(date: q.creationDate!)
        print("q.creationDate")
        
        var date = Date(timeIntervalSince1970: q.creationDate! / 1000)
        
        cell.date.text = date.relativeTime.replacedEnglishDigitsWithPersian
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatStoryboard: UIStoryboard = UIStoryboard(name: "ChatMessage", bundle: nil)
        let vc: ChatNavController = chatStoryboard.instantiateViewController(withIdentifier: "ChatNavController") as! ChatNavController
        try? AppDelegate.getStorage().setObject(questions[indexPath.row].chatRoom, forKey: "chatroom", expiry: .date(Date().addingTimeInterval(10)))
        try? AppDelegate.getStorage().setObject(questions[indexPath.row].id!, forKey: "qId", expiry: .date(Date().addingTimeInterval(10)))
        self.present(vc, animated: true, completion: nil)
        
    }

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = questions.count - 1
        
        if !loadingData && indexPath.row == lastElement {
            print("page:\(page) ")
            page += 1
            spinner.isHidden = false
            spinner.startAnimating()
            loadingData = true
            loadChatrooms()
            
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
