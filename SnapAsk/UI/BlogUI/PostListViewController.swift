//
//  PostListViewController.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import NVActivityIndicatorView
import FontAwesomeKit

class PostListViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource{

    
    private var posts:[Post] = []
    private var page = 0;
    var catId = 0;
    var catTitle = ""
    
    @IBOutlet weak var toolbar: Toolbar!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        loadPosts()
        initToolbar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "PostCell",for: indexPath) as! PostCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.card.cornerRadiusPreset = .cornerRadius1
        cell.selectionStyle = .none
        
        
        let post = posts[indexPath.row];
        cell.postTitle.text = post.title;
        cell.postDesc.text = post.content
        if let d = post.creationDate
        {
            var date = Date(timeIntervalSince1970: d / 1000)
            cell.date.text = date.relativeTime.replacedEnglishDigitsWithPersian
            
        }

        let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((post.coverImage?.id)!)))!
        cell.postImg.kf.setImage(with: url)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailsViewController") as! PostDetailsViewController
        vc.postId = Int(posts[indexPath.row].id!)
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadPosts()
    {
        spinner.isHidden == false
        spinner.startAnimating()
        
        PostService.getInstance().getPostsByCategory(catId: self.catId).subscribe(onNext:{res in
            print(res)
            if self.page == 0 {
                self.posts = res
            }
            else {
                for p in res {
                    self.posts.append(p)
                }
            }
            self.tableview.reloadData()
            
            self.spinner.isHidden == true
            self.spinner.stopAnimating()
        })
    }
    
    func initToolbar()  {
        
        toolbar.depthPreset = .depth2
        toolbar.titleLabel.font = UIFont(name: "IRANSansMobile", size: 14)
        toolbar.titleLabel.text = catTitle;
        toolbar.titleLabel.textColor = UIColor.black
        toolbar.layer.zPosition = 10;
        toolbar.backgroundColor = UIColor.white
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).tint(with: .white)
        
        let backBtn = IconButton(image: backImg?.tint(with: .black), tintColor: .black)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        
        toolbar.rightViews = [backBtn]
    }
    
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
