//
//  PostDetailsViewController.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import NVActivityIndicatorView
import FontAwesomeKit

class PostDetailsViewController: UIViewController ,UIScrollViewDelegate{

    var backBtn:IconButton?
    @IBOutlet weak var postImgTopConst: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var toolbar: Toolbar!
    @IBOutlet weak var card: Card!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postContent: UILabel!
    
    @IBOutlet weak var shareBtn: FABButton!
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    var showToolbar = false
    var postId = 0;
    var post:Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        UIApplication.shared.statusBarStyle = .lightContent
        initUI()
        loadPost()
        // Do any additional setup after loading the view.
    }
    

    func initUI()
    {
        scrollview.delegate = self
        initToolbar()
        shareBtn.tintColor = UIColor.white
        shareBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        card.depthPreset = .depth2
    }
    
    
    func initToolbar()  {
        
        toolbar.depthPreset = .depth2
        toolbar.titleLabel.font = UIFont(name: "IRANSansMobile", size: 14)
        toolbar.titleLabel.text = " ";
        toolbar.titleLabel.textColor = UIColor.black
        toolbar.layer.zPosition = 10;
        toolbar.backgroundColor = UIColor.clear
        toolbar.shadowColor = UIColor.clear
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: .white)
        
        backBtn = IconButton(image: backImg?.tint(with: .white), tintColor: .white)
        backBtn?.pulseColor = .white
        backBtn?.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        
        toolbar.rightViews = [backBtn!]
    }
    
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadPost()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        PostService.getInstance().getPostById(id: postId).subscribe(onNext:{res in
            self.post = res
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.postTitle.text = res.title
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((res.coverImage?.id)!)))!
            self.postImg.kf.setImage(with: url)
            self.postContent.text = res.content
        })
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollview.contentOffset)
        if scrollview.contentOffset.y <= 0{
            postImgTopConst.constant = 0;
        }
        else if(scrollview.contentOffset.y < 100)
        {
           postImgTopConst.constant = -scrollview.contentOffset.y * 1.5

        }
        if(scrollview.contentOffset.y > 95)
        {
            updateUI(showTool: true)
        }
        else
        {
            updateUI(showTool: false)
//            toolbar.animate(.background(color: .clear), .delay(1))
        }
    }
    
    func updateUI(showTool:Bool)
    {
        if !showToolbar{
            if showToolbar != showTool
            {
                showToolbar = true
                toolbar.animate(.background(color: .white), .duration(0.4))
                toolbar.shadowColor = UIColor.black
                backBtn?.image = backBtn?.image?.tint(with: .black)
                toolbar.titleLabel.text = post?.title
                postImg.animate(.fadeOut , .duration(0.4))
                UIApplication.shared.statusBarStyle = .default
                
            }
        }
        if showToolbar{
            if showToolbar != showTool
            {
                showToolbar = false
                toolbar.animate(.background(color: .clear), .duration(0.4))
                backBtn?.image = backBtn?.image?.tint(with: .white)
                toolbar.titleLabel.text = ""
                toolbar.shadowColor = UIColor.clear
                postImg.animate(.fadeIn , .duration(0.4))
                UIApplication.shared.statusBarStyle = .lightContent
            }
        }

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
