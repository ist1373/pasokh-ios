//
//  AboutUsViewController.swift
//  SnapAsk
//
//  Created by iman on 5/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var toolbar: Toolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initToolbar()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initToolbar()  {

        
    
        toolbar.depthPreset = .depth2
        toolbar.titleLabel.font = UIFont(name: "IRANSansMobile", size: 14)
        toolbar.titleLabel.text = "درباره پاسخ";
        toolbar.titleLabel.textColor = UIColor.black
        toolbar.layer.zPosition = 10;
        
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).tint(with: .white)
        
        let backBtn = IconButton(image: backImg?.tint(with: .black), tintColor: .black)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        
        toolbar.rightViews = [backBtn]
    }
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
