//
//  PoliciesViewController.swift
//  SnapAsk
//
//  Created by iman on 5/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit
class PoliciesViewController: UIViewController {

    @IBOutlet weak var policies_tv: UITextView!
    @IBOutlet weak var toolbar: Toolbar!
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        policies_tv.font = UIFont(name: "IRANSansMobile", size: 14)
        
        let p1 = "بوکسام در تابستان 95 کلید خورد و پس از رو به رو شدن با چالش های مختلف نظیر فراهم کردن دیتابیس ابتدایی کتاب ها و عقد قرارداد با فروشگاه های کتاب سرانجام به طور رسمی کار خودرا در مهر ماه 1396 آغاز کرد. بوکسام اولین سامانه اشتراک کتاب های دست دوم و نو در ایران است که می‌کوشد با فراهم کردن محل عطفی برای فروشنده و خریدار تجربه فوق‌العاده ای را برای هر د"
        let p2 = "نه اشتراک کتاب های دست"
        let p3 = "تابیس ابتدایی کتاب ها و عقد قرارداد با فروشگاه های کتاب سرانجام به "
        let stringArray = [p1, p2, p3]

        policies_tv.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "IRANSansMobile", size: 14))
        policies_tv.textAlignment = .right
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initToolbar()  {
        
        
        
        toolbar.depthPreset = .depth2
        toolbar.titleLabel.font = UIFont(name: "IRANSansMobile", size: 14)
        toolbar.titleLabel.text = "قوانین و مقررات"
        toolbar.titleLabel.textColor = UIColor.black
        toolbar.layer.zPosition = 10;
        
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).tint(with: .white)
        
        let backBtn = IconButton(image: backImg?.tint(with: .black), tintColor: .black)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        
        toolbar.rightViews = [backBtn]
    }
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



class NSAttributedStringHelper {
    static func createBulletedList(fromStringArray strings: [String], font: UIFont? = nil) -> NSAttributedString {
        
        let fullAttributedString = NSMutableAttributedString()
        let attributesDictionary: [NSAttributedStringKey: Any]
        
        if font != nil {
            attributesDictionary = [NSAttributedStringKey.font: font!]
        } else {
            attributesDictionary = [NSAttributedStringKey: Any]()
        }
        
        for index in 0..<strings.count {
            let bulletPoint: String = "\u{2022}"
            var formattedString: String = "\(bulletPoint) \(strings[index])"
            
            if index < strings.count - 1 {
                formattedString = "\(formattedString)\n"
            }
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedString, attributes: attributesDictionary)
            let paragraphStyle = NSAttributedStringHelper.createParagraphAttribute()
            attributedString.addAttributes([NSAttributedStringKey.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
            fullAttributedString.append(attributedString)
        }
        
        return fullAttributedString
    }
    
    private static func createParagraphAttribute() -> NSParagraphStyle {
        let paragraphStyle: NSMutableParagraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 15, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 15
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.headIndent = 11
        return paragraphStyle
    }
}
