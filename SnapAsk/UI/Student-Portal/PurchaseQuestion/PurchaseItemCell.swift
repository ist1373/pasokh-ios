//
//  PurchaseItemCell.swift
//  SnapAsk
//
//  Created by iman on 6/17/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Material



class PurchaseItemCell:UICollectionViewCell
{
    @IBOutlet weak var card: Card!
    @IBOutlet weak var purchaceBtn: RaisedButton!
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var titleLblb: UILabel!
    @IBOutlet weak var itemImg: UIImageView!
    
    
}
