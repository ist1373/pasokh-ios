//
//  PurchaseViewController.swift
//  SnapAsk
//
//  Created by iman on 6/17/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit
import NVActivityIndicatorView
import Kingfisher
import ToastSwiftFramework


class PurchaseViewController: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var spinner: NVActivityIndicatorView!

    @IBOutlet weak var toolbar: Toolbar!
    
    @IBOutlet weak var headerCard: Card!
    
    @IBOutlet weak var amountLbl: UILabel!
    
    @IBOutlet weak var purchaseCollectionView: UICollectionView!
    
    var purchases:[Purchase] = []
    
    var isCallback:Bool = false
    var callbackSuccess:Bool?
    var callbackMessage = ""
    var authority = ""
    var std:Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        
//      NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: .UIApplicationDidBecomeActive, object: nil)
        loadUser(runCallBack: true);
        headerCard.depthPreset = .depth2
        purchaseCollectionView.register(UINib(nibName: "PurchaseItemCell", bundle: nil), forCellWithReuseIdentifier: "PurchaseItemCell")
        purchaseCollectionView.delegate = self
        purchaseCollectionView.dataSource = self
        purchaseCollectionView.backgroundColor = self.view.backgroundColor
        spinner.isHidden = false
        spinner.startAnimating()
        
        
        if let ob = PurchaseService.getInstance().getAvailablePurchases()
        {
            
            ob.subscribe(onNext:{res in
                self.purchases = res
                self.purchaseCollectionView.reloadData()
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
                
            })
        }

    }
    


    func verifyPurchace()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        
       
        if let pId = try? AppDelegate.getStorage().object(ofType: Int64.self, forKey: "last-purchase-id")
        {
            TransactionServic.getInstance().zarinContentVerify(userId: (std?.id!)! , pId: pId, authority: authority).subscribe(onNext:{res in
                    if res.success! == 1
                    {
                        self.loadUser(runCallBack: false)
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                    }
                    else{
                        self.showToast(message: res.message!, bgColor: .red)
                    }
                })
        }
        
    }
    
    
    
    func loadUser(runCallBack:Bool)
    {
        spinner.isHidden = false
        spinner.startAnimating()
        StudentService.getInstance().getAuthenticatedStudetn(self).subscribe(onNext:{res in
            self.std = res
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.amountLbl.text = "موجودی :" + " \(res.credit!) ".replacedEnglishDigitsWithPersian + GlobalProperties.CreditScale
            
            if self.isCallback && runCallBack
            {
                self.showToast(message: self.callbackMessage)
                self.view.layoutSubviews()
                if self.callbackSuccess!
                {
                    self.verifyPurchace()
                }
                
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return purchases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = purchaseCollectionView.dequeueReusableCell(withReuseIdentifier: "PurchaseItemCell",for: indexPath) as! PurchaseItemCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.purchaceBtn.cornerRadiusPreset = .cornerRadius3
        cell.purchaceBtn.titleEdgeInsets = UIEdgeInsetsMake(5,0,0,0)
        
        var p = purchases[indexPath.row];
        cell.titleLblb.text = p.title
        cell.costLbl.text = "\(String(Int(p.price!)).replacedEnglishDigitsWithPersian) تومان"
        cell.costLbl.padding = UIEdgeInsetsMake(5,0,0,0)
        let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((p.image!.id)!)))!
        print(url)
        cell.itemImg.kf.setImage(with: url)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if UIDevice().userInterfaceIdiom == .phone
//        {
            let size = (self.view.frame.width / 2) - 10
            return CGSize(width: size, height: size + 30)
//        }
        
//        if UIDevice().userInterfaceIdiom == .pad
//        {
//            return CGSize(width: 150, height: 225)
//        }
//        return CGSize(width: 180, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let per = purchases[indexPath.row]
        try? AppDelegate.getStorage().setObject(per.id!, forKey: "last-purchase-id", expiry: .never)
        TransactionServic.getInstance().paymentRequest(pId: per.id!, callbackUrl: "snapAsk://\(per.id!)").subscribe(onNext:{res in
            if res.success! == 1
            {
                let url = URL(string: "https://www.zarinpal.com/pg/StartPay/\(res.message!)")
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url!)
                }
            }

        },onError:{error in
            print(error)
        })
    }
    
    func initToolbar()  {
       // UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor

        toolbar.depthPreset = .depth2
        toolbar.backgroundColor = UIColor.white
        toolbar.titleLabel.font = UIFont(name: "Mj_Flow Bold", size: 15)
        toolbar.titleLabel.text = "افزودن اعتبار";
        toolbar.titleLabel.textColor =  GlobalProperties.PrimaryColor
        toolbar.layer.zPosition = 10;
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: .black)
        
        let backBtn = IconButton(image: backImg?.tint(with: .black), tintColor: .black)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        toolbar.rightViews = [backBtn]

    }
    

    
    @objc func back()
    {
        
        self.dismiss(animated: true, completion: nil)
    }
    



    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        UIApplication.shared.statusBarStyle = .lightContent
//    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            if self.isCallback
//            {
//                print("fffffffffffff")
//                self.showToast(message: self.callbackMessage)
//                self.view.layoutSubviews()
//            }
//        }
//    }
    
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return .lightContent
//    }
}
