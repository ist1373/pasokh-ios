//
//  TabBarViewController.swift
//  SnapAsk
//
//  Created by iman on 5/27/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material



class ST_TabBarViewController: UITabBarController {
    let button = FABButton(image: UIImage(named:"rise-hand"))
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        button.image = UIImage(named:"rise-hand")?.tint(with: Color.white)
        button.depthPreset = .depth4
        
        button.backgroundColor = GlobalProperties.PrimaryDarkColor
        
        self.view.insertSubview(button, aboveSubview: self.tabBar)
        
        button.addTarget(self, action: #selector(ST_TabBarViewController.onAddQuestion), for: .touchUpInside)
        
        self.selectedIndex = 3;
        

    }
    
    
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        tabFrame.size.height = 56
        tabFrame.origin.y = self.view.frame.size.height - 56
        self.tabBar.frame = tabFrame
        self.tabBar.backgroundColor = .white
        self.tabBar.tintColor = GlobalProperties.PrimaryColor
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // safe place to set the frame of button manually
        button.frame = CGRect.init(x: self.tabBar.center.x - 30, y: self.view.bounds.height - 74, width: 60, height: 60)
        var i = 0
        for item in self.tabBar.items!
        {
            if(i>1)
            {
                item.titlePositionAdjustment = UIOffset(horizontal: 20, vertical: 0)
            }
            else
            {
                item.titlePositionAdjustment = UIOffset(horizontal: -20, vertical: 0)
            }
            i += 1
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func onAddQuestion()  {

        StudentService.getInstance().getAuthenticatedStudetn(self).subscribe(onNext:{res in
            self.checkCredit(student: res)
            
        })
    }
    
    func checkCredit(student:Student)
    {
        DynamicService.getInstance().getDynamicContent(keyContent: "QUESTION_CREDIT").subscribe(onNext:{res in
            if let qCredit = Int(res.data!)
            {
                if student.credit! >= qCredit
                {
                    let storyboard: UIStoryboard = UIStoryboard(name: "AddQuestion", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "AddQuestionViewController") as! AddQuestionViewController
                    
                    self.present(vc, animated: true, completion: nil) //(vc, sender: self)
                }
                else
                {
                    self.showToast(message: "اعتبار شما برای پرسیدن سوال کافی نیست", bgColor: .red)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                        let storyboard: UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
                        
                        self.present(vc, animated: true, completion: nil) //(vc, sender: self)
                    }
                    
                }
            }
            
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
