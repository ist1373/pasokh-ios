//
//  ST_RightViewController.swift
//  SnapAsk
//
//  Created by iman on 5/28/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import FontAwesomeKit


class ST_RightViewController: UIViewController {

    var slideMenuController:SlideMenuController?
    
    @IBOutlet weak var back_btn: UIButton!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var logInOut: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var headerPart: UIView!
    
    @IBOutlet weak var exitItem: MenuItem!
    @IBOutlet weak var aboutusItem: MenuItem!
    @IBOutlet weak var rulesItem: MenuItem!
    @IBOutlet weak var contactusItem: MenuItem!
    @IBOutlet weak var shareItem: MenuItem!

    @IBOutlet weak var purchaseItem: MenuItem!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        back_btn.layer.zPosition = 15
        headerPart.backgroundColor = GlobalProperties.PrimaryColor
        initMenuItems()
    }

    @IBAction func onBackTouch(_ sender: Any) {
        self.slideMenuController()?.closeRight()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    
    
    
    func initMenuItems() {

        shareItem.icon.image = UIImage(named:"share")?.tint(with: shareItem.title.textColor)
        shareItem.title.text = "معرفی به دوستان"
        
        
        contactusItem.icon.image = UIImage(named:"email")?.tint(with: contactusItem.title.textColor)
        contactusItem.title.text = "ارتباط با ما"
        
        rulesItem.icon.image = UIImage(named:"rules")?.tint(with: rulesItem.title.textColor)
        rulesItem.title.text = "قوانین و مقررات"
        let rulesItemGesture = UITapGestureRecognizer(target: self, action: #selector(ST_RightViewController.policiesTouch))
        rulesItem.addGestureRecognizer(rulesItemGesture)
        
        purchaseItem.icon.image = FAKIonIcons.iosCartIcon(withSize: 40).image(with: CGSize(width:40,height:40)).tint(with: exitItem.title.textColor)
        purchaseItem.title.text = "افزودن اعتبار"
        let purchaseItemGesture = UITapGestureRecognizer(target: self, action: #selector(ST_RightViewController.purchaseTouch))
        purchaseItem.addGestureRecognizer(purchaseItemGesture)
        
        
        aboutusItem.icon.image = UIImage(named:"info")?.tint(with: aboutusItem.title.textColor)
        aboutusItem.title.text = "درباره پاسخ"
        let aboutusGusture = UITapGestureRecognizer(target:self,action: #selector(ST_RightViewController.aboutUsTouch))
        aboutusItem.addGestureRecognizer(aboutusGusture)
        
        exitItem.icon.image = FAKIonIcons.androidExitIcon(withSize: 50).image(with: CGSize(width:50,height:50)).tint(with: exitItem.title.textColor)
        exitItem.title.text = "خروج"
        exitItem.isHidden = false;
        let exitRecognizer = UITapGestureRecognizer(target: self, action: #selector(onExitTouch))
        exitItem.addGestureRecognizer(exitRecognizer)
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            if let fn = user.firstName
            {
               username.text = fn
            }
            if let ln = user.lastName
            {
                username.text = username.text! + " " + ln
            }
            if username.text == "کاربر مهمان"
            {
                username.text = "شماره موبایل"
            }
            print(user.mobileNumber)
            logInOut.text = user.mobileNumber!.replacedEnglishDigitsWithPersian
         
        }
    }
    
    @objc func aboutUsTouch()
    {
        let st = UIStoryboard()
        let storyboard: UIStoryboard = UIStoryboard(name: "RightDrawer", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func policiesTouch()
    {

        let storyboard: UIStoryboard = UIStoryboard(name: "RightDrawer", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PoliciesViewController") as! PoliciesViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func purchaseTouch()
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.show(vc, sender: self)
    }
    
    @objc func onExitTouch()
    {
        try? AppDelegate.getStorage().removeAll()
        let st = UIStoryboard(name: "SignInUp", bundle: nil)
        let vc  = st.instantiateViewController(withIdentifier: "LoginViewController")
        vc.modalPresentationStyle = .currentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
