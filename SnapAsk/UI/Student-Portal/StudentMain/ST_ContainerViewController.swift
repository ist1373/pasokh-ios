//
//  ST_ContainerViewController.swift
//  SnapAsk
//
//  Created by iman on 5/28/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Material
import FontAwesomeKit
import MaterialShowcase
import NVActivityIndicatorView

class ST_ContainerViewController: SlideMenuController , SlideMenuControllerDelegate , MaterialShowcaseDelegate{

    

    @IBOutlet weak var toolbar: Toolbar!
    
    var menuBtn:IconButton? = nil;
    var menuImgClose:UIImage? = nil;
    var menuImgOpen:UIImage? = nil;
    var tutorialStep = 0;
    var questionBtn:FABButton?
    
    
    override func awakeFromNib() {
        
//        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
//        let statusBarColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
//        statusBarView.backgroundColor = statusBarColor
//        statusBarView.layer.zPosition = 10
//        view.addSubview(statusBarView)
        
        
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.rightViewWidth  = self.view.frame.width * 0.7;
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.simultaneousGestureRecognizers = true
        SlideMenuOptions.panGesturesEnabled = true
        SlideMenuOptions.tapGesturesEnabled = true
        

        
        toolbar.depthPreset = .depth2
        toolbar.titleLabel.font = UIFont(name: "Mj_Flow Bold", size: 15)
        toolbar.titleLabel.text = "پاسخ"
        toolbar.titleLabel.textColor = GlobalProperties.PrimaryDarkColor
        toolbar.layer.zPosition = 10;
        
        
//        let addBookImg = UIImage(named:"ic_add_book");
//        let addBookBtn = IconButton(image: ResizeImage(image: addBookImg!, targetSize: CGSize(width: 25, height: 25)), tintColor: .white)
//        addBookBtn.pulseColor = .white
        
        menuImgClose = FAKIonIcons.androidMenuIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        menuImgOpen = FAKIonIcons.androidArrowForwardIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        
        menuBtn = IconButton(image: menuImgClose?.tint(with: GlobalProperties.PrimaryDarkColor), tintColor: GlobalProperties.PrimaryDarkColor)
        menuBtn?.pulseColor = .white
        menuBtn?.addTarget(self, action: #selector(handleMenuBtn(button:)), for: .touchUpInside)
        
        
       // toolbar.leftViews = [addBookBtn]
        toolbar.rightViews = [menuBtn!]
        
        self.delegate = self;
        
        
        
        
        
        
        if let controller:ST_TabBarViewController = self.storyboard?.instantiateViewController(withIdentifier: "ST_TabBarViewController") as! ST_TabBarViewController {
            self.mainViewController = controller
            self.questionBtn = controller.button
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ST_RightViewController") {
            self.rightViewController = controller
            let rvc = controller as! ST_RightViewController
            rvc.slideMenuController = self.slideMenuController()
        }
        
        if let t = try? AppDelegate.getStorage().object(ofType: Bool.self, forKey: "st-tutorial-end")
        {
            if t {

            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                    self.showTutorialCaseMenue()
                }

            }
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                self.showTutorialCaseMenue()
            }
        }
        
        super.awakeFromNib()
    }
    
    
    
    func showTutorialCaseMenue() {
        
        
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: menuBtn!) // always required to set targetView
        showcase.primaryText = "منوی کناری"
        showcase.secondaryText = "برای ورود، ثبت نام و یا مشاهده قابلیت های برنامه، منوی کناری را باز کنید"
        
        
        
        // Background
        showcase.backgroundPromptColor = GlobalProperties.PrimaryDarkColor
        showcase.backgroundPromptColorAlpha = 0.95
        showcase.backgroundViewType = .circle
        
        // Target
        showcase.targetTintColor = GlobalProperties.PrimaryDarkColor
        showcase.targetHolderRadius = 44
        showcase.targetHolderColor = UIColor.clear
        // Text
        showcase.primaryTextColor = UIColor.white
        showcase.secondaryTextColor = UIColor.white
        showcase.primaryTextSize = 20
        showcase.secondaryTextSize = 15
        showcase.primaryTextFont = GlobalProperties.getIranSansFont(size: 15)
        showcase.secondaryTextFont = GlobalProperties.getIranSansFont(size: 13)
        //Alignment
        showcase.primaryTextAlignment = .right
        showcase.secondaryTextAlignment = .right
        // Animation
        showcase.aniComeInDuration = 0.5 // unit: second
        showcase.aniGoOutDuration = 0.5 // unit: second
        showcase.aniRippleScale = 1.5
        showcase.aniRippleColor = UIColor.white
        showcase.aniRippleAlpha = 0.2
        showcase.delegate = self
        
        
        showcase.show(completion: {
            try? AppDelegate.getStorage().setObject(true, forKey: "st-tutorial-end" , expiry: .never)
        })
    }
    
    func showTutorialCaseAddQuestion() {
        
  
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: questionBtn!) // always required to set targetView
        showcase.primaryText = "پرسش سوال"
        showcase.secondaryText = "برای پرسش سوال خود از این قسمت استفاده کنید"
        showcase.delegate = self
        
        
        // Background
        showcase.backgroundPromptColor = GlobalProperties.PrimaryDarkColor
        showcase.backgroundPromptColorAlpha = 0.95
        showcase.backgroundViewType = .circle
        
        // Target
        showcase.targetTintColor = GlobalProperties.PrimaryDarkColor
        showcase.targetHolderRadius = 44
        showcase.targetHolderColor = UIColor.clear
        // Text
        showcase.primaryTextColor = UIColor.white
        showcase.secondaryTextColor = UIColor.white
        showcase.primaryTextSize = 20
        showcase.secondaryTextSize = 15
        showcase.primaryTextFont = GlobalProperties.getIranSansFont(size: 15)
        showcase.secondaryTextFont = GlobalProperties.getIranSansFont(size: 13)
        //Alignment
        showcase.primaryTextAlignment = .center
        showcase.secondaryTextAlignment = .center
        // Animation
        showcase.aniComeInDuration = 0.5 // unit: second
        showcase.aniGoOutDuration = 0.5 // unit: second
        showcase.aniRippleScale = 1.5
        showcase.aniRippleColor = UIColor.white
        showcase.aniRippleAlpha = 0.2
        showcase.delegate = self
        
        showcase.show(completion: {
            // You can save showcase state here
            // Later you can check and do not show it again
            
        })
    }
    
    
    func showCaseDidDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
        
        if tutorialStep == 0
        {
           print("showcase runnnnnnnnn")
           showTutorialCaseAddQuestion()
        }
        tutorialStep += 1
        try? AppDelegate.getStorage().setObject(true, forKey: "tutorial-end", expiry: .never)
        
    }
    
    
    
//    func onChangeItem(index: Int) {
////        var statusBarView:UIView?
//        if(index == 3)
//        {
//            toolbar.isHidden = true
//            UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
//
//        }
//        else
//        {
////            if let v = statusBarView
////            {
////                v.removeFromSuperview()
////            }
//            UIApplication.shared.statusBarView?.backgroundColor = .white
//            toolbar.isHidden = false
//        }
//    }
    
    @objc
    func handleMenuBtn(button: UIButton) {
        print("cliccccccc")
        if (self.slideMenuController()?.isRightOpen())!
        {
            self.slideMenuController()?.closeRight()
        }
        else
        {
            self.slideMenuController()?.openRight()
        }
        
    }
    
    func rightWillClose() {
        menuBtn?.image = menuImgClose
    }
    func rightWillOpen() {
        menuBtn?.image = menuImgOpen
    }
    

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
  
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        UIApplication.shared.statusBarStyle = .default
//    }
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return .lightContent
//    }

}
