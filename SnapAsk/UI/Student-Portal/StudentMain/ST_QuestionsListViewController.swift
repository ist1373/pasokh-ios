//
//  ST_QuestionsListViewController.swift
//  SnapAsk
//
//  Created by iman on 6/10/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Material

class ST_QuestionsListViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var messageBtn: UIButton!
    var page = 0 ;
    var size = 20 ;
    var loadingData = false;
    var questions:[Question] = []

    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.register(UINib(nibName: "QuestionCell", bundle: nil), forCellReuseIdentifier: "QuestionCell")
        tableview.delegate = self
        tableview.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getQuestions()
    }
    func getQuestions() {
        spinner.isHidden = false
        spinner.startAnimating()
        if let ob = QuestionService.getInstance().getUserQuestions(page: page)
        {
            ob.subscribe(onNext:{res in
                if self.page == 0
                {
                    self.questions = res
                }
                else
                {
                    for  q in res
                    {
                        self.questions.append(q)
                    }
                }
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
                self.tableview.reloadData()
                if res.count > 0
                {
                    self.loadingData = false
                }
                if self.questions.count == 0{
                    self.messageBtn.isHidden = false
                }
            })
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "QuestionCell",for: indexPath) as! QuestionCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth1
        cell.selectionStyle = .none
        let q = questions[indexPath.row];
        cell.title.text = q.description;
        
        if (q.questionState == QuestionState.Pending)
        {
            cell.state.textColor = GlobalProperties.blueColor
            cell.state.text = "در حال بررسی"
            cell.ratingView.isHidden = true
            
        }
        else if q.questionState == QuestionState.Finished
        {
            cell.ratingView.isHidden = false
            cell.state.text = "پاسخ دهنده: "
            
            if let fn = q.responder?.firstName
            {
                cell.state.text = cell.state.text! + fn + " "
            }
            if let ln = q.responder?.lastName
            {
                cell.state.text = cell.state.text! + ln
            }
            cell.state.textColor = GlobalProperties.greenColor
            cell.ratingView.rating = Double(q.rate!)
        }
        else
        {
            cell.ratingView.isHidden = true
            cell.state.text = "پاسخ دهنده: "
            
            if let fn = q.responder?.firstName
            {
                cell.state.text = cell.state.text! + fn + " "
            }
            if let ln = q.responder?.lastName
            {
                cell.state.text = cell.state.text! + ln
            }
            cell.state.textColor = GlobalProperties.greenColor
        }
        
        if let img = q.image
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((img.id)!)))!
            cell.questionImage.kf.setImage(with: url)
        }
        
        cell.date.text = PersianTool.getDateStringFromTimeStamp(date: q.creationDate!)
        print("q.creationDate")
        
        var date = Date(timeIntervalSince1970: q.creationDate! / 1000)

        cell.date.text = date.relativeTime.replacedEnglishDigitsWithPersian
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = questions.count - 1
    
        if !loadingData && indexPath.row == lastElement {
            print("page:\(page) ")
            page += 1
            spinner.isHidden = false
            spinner.startAnimating()
            loadingData = true
            getQuestions()
            
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if questions[indexPath.row].questionState == QuestionState.Accepted || questions[indexPath.row].questionState == QuestionState.Finished
        {
            let chatStoryboard: UIStoryboard = UIStoryboard(name: "ChatMessage", bundle: nil)
            let vc: ChatNavController = chatStoryboard.instantiateViewController(withIdentifier: "ChatNavController") as! ChatNavController
            try? AppDelegate.getStorage().setObject(questions[indexPath.row].chatRoom, forKey: "chatroom", expiry: .date(Date().addingTimeInterval(10)))
            try? AppDelegate.getStorage().setObject(questions[indexPath.row].id!, forKey: "qId", expiry: .date(Date().addingTimeInterval(10)))
            self.present(vc, animated: true, completion: nil)
        }
        else if questions[indexPath.row].questionState == QuestionState.Pending
        {
            self.showToast(message: "لطفا تا یافتن پاسخ دهنده برای سوال خود منتظر بمانید", bgColor: GlobalProperties.PrimaryColor)
        }
        else if questions[indexPath.row].questionState == QuestionState.WaitingForTutor
        {
            self.showToast(message: "لطفا منتظر بمانید تا پاسخ دهنده مستقیم درخواست شما را بپذیرد", bgColor: GlobalProperties.PrimaryColor)
        }
        else if questions[indexPath.row].questionState == QuestionState.RejectedByTutor
        {
            self.showToast(message: "پاسخ دهنده مستقیم از پاسخ دادن به این سوال انصراف داده است", bgColor: GlobalProperties.PrimaryColor)
        }
        else if questions[indexPath.row].questionState == QuestionState.Rejected
        {
            self.showToast(message: "این سوال توسط مدیران برنامه رد شده است", bgColor: GlobalProperties.PrimaryColor)
        }

        
    }

    
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return UIStatusBarStyle.default
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
