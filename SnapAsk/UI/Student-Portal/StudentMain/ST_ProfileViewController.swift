//
//  ST_ProfileViewController.swift
//  SnapAsk
//
//  Created by iman on 6/18/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit
import NVActivityIndicatorView
import Kingfisher

class ST_ProfileViewController: UIViewController {

    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    @IBOutlet weak var fullname: UILabel!
    
    @IBOutlet weak var grade: UILabel!
    
    @IBOutlet weak var edit_btn: IconButton!
    
    @IBOutlet weak var cardHeader: PulseView!
    

    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var email: AttrItem!
    
    @IBOutlet weak var filed: AttrItem!
    
    @IBOutlet weak var school: AttrItem!
    
    @IBOutlet weak var avg: AttrItem!
    
    @IBOutlet weak var city: AttrItem!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getMyUser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.setRounded()
        
//        fullname.attrIcon.image = FAKIonIcons.iosPersonOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
//        fullname.attrTitle.text = "نام و نام خانوادگی"
        
        edit_btn.image = FAKIonIcons.editIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).tint(with: UIColor.white)
        
        email.attrIcon.image = FAKIonIcons.iosEmailOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        email.attrTitle.text = "ایمیل"

        
        filed.attrIcon.image = FAKIonIcons.iosBookOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        filed.attrTitle.text = "رشته"
        
        school.attrIcon.image = FAKIonIcons.iosCalculatorOutlineIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: GlobalProperties.PrimaryDarkColor)
        school.attrTitle.text = "مدرسه"
        
        city.attrIcon.image = FAKIonIcons.iosLocationOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        city.attrTitle.text = "شهر"
        
        avg.attrIcon.image = FAKIonIcons.iosPaperOutlineIcon(withSize: 25).image(with: CGSize(width: 25, height: 25)).tint(with: GlobalProperties.PrimaryDarkColor)
        avg.attrTitle.text = "معدل"
        
        
        // Do any additional setup after loading the view.
    }
    
    func getMyUser()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        StudentService.getInstance().getAuthenticatedStudetn(self).subscribe(onNext:{res in
            self.fullname.text = ""
            if let temp = res.firstName
            {
                self.fullname.text = temp
            }
            if let temp = res.lastName
            {
                self.fullname.text = self.fullname.text! + " " + temp
            }
            
            if let gr = res.grade
            {
                self.grade.text = gr.description
            }
            
            if let email = res.email
            {
                self.email.attrLabel.text = email;
            }
            if let fieldd = res.field
            {
                self.filed.attrLabel.text = fieldd.description
            }
            if let schooll = res.school
            {
                self.school.attrLabel.text = schooll
            }
            if let avgg = res.average
            {
                self.avg.attrLabel.text = "\(avgg)"
            }
            if let cityy = res.city
            {
                self.city.attrLabel.text = cityy.title
            }
            if let pi = res.profileImage
            {
                let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((pi.id)!)))!
                self.profileImage.kf.setImage(with: url)
            }
            
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onEdit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "EditProfile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ST_EditProfileViewController") as! ST_EditProfileViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ksldjflskdjflskjdfsd")
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
