//
//  ST_BlogCategoryViewController.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ST_BlogCategoryViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var spinner: NVActivityIndicatorView!
    var categories : [Category] = []

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        tableview.delegate = self
        tableview.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCategory()
    }
    
    func getCategory() {
        spinner.isHidden = false
        spinner.startAnimating()
        CategoryService.getInstance().getCategory().subscribe(onNext:{ res in
            self.categories = res
            self.tableview.reloadData()
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            
        })
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "CategoryCell",for: indexPath) as! CategoryCell
        cell.backgroundColor = UIColor.clear
        cell.card.depthPreset = .depth2
        cell.card.cornerRadiusPreset = .cornerRadius2
 
        cell.selectionStyle = .none
        let cat = categories[indexPath.row];
        cell.title.text = cat.title;
        cell.titleCard.cornerRadiusPreset = .cornerRadius2
        cell.titleCard.alpha = 0.8
        let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((cat.coverImage?.id)!)))!
        cell.catImg.kf.setImage(with: url)
        cell.catImg.layer.cornerRadiusPreset = .cornerRadius2
        cell.catImg.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "BlogUI", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "PostListViewController") as! PostListViewController
        vc.catId = categories[indexPath.row].id!
        vc.catTitle = categories[indexPath.row].title!
        present(vc, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
