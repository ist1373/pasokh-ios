//
//  AddQuestionViewController.swift
//  SnapAsk
//
//  Created by iman on 6/7/18.
//  Copyright © 2018 iman. All rights reserved.
//



import UIKit
import SwiftyOnboard
import FontAwesomeKit
import ToastSwiftFramework
    
class AddQuestionViewController: UIViewController, SwiftyOnboardDataSource,SwiftyOnboardDelegate,OverlayPageDelegate{
    
    
   var question = Question()
    var questionTopicPage :QuestionTopicPage!
    var questionImagePage: QuestionImagePage!
    var questionTutorPage: QuestionTutorPage!
//    var bookOwnerPage:BookOwnerPage!
    
    
    
    var swiftyOnboard :CustomSwiftyOnboard? = nil
    var currentPage = 0;
    override func viewDidLoad() {
        super.viewDidLoad()
        swiftyOnboard = CustomSwiftyOnboard(frame: view.frame)
        view.addSubview(swiftyOnboard!)
        swiftyOnboard?.dataSource = self
        swiftyOnboard?.delegate = self
        swiftyOnboard?.fadePages = false
        
        self.hideKeyboardWhenTappedAround()
        
        //UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: CustomSwiftyOnboard) -> Int {
        return 3
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: CustomSwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        var view:SwiftyOnboardPage?
        
        switch index {
        case 0:
            view = QuestionTopicPage.instanceFromNib() as? QuestionTopicPage
            questionTopicPage = view as! QuestionTopicPage
            questionTopicPage!.initUI()
            
        case 1:
            view = QuestionImagePage.instanceFromNib() as? QuestionImagePage
            questionImagePage = view as! QuestionImagePage
            questionImagePage!.initUI()
            
        case 2:
            view = QuestionTutorPage.instanceFromNib() as? QuestionTutorPage
            questionTutorPage = view as! QuestionTutorPage
            questionTutorPage!.initUI()
        default:
            view = QuestionTopicPage.instanceFromNib() as? QuestionTopicPage
            questionTopicPage = view as! QuestionTopicPage
            questionTopicPage!.initUI()
        }
        return view
    }
    
    
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: CustomSwiftyOnboard) -> SwiftyOnboardOverlay?{
        let view = AddQuestionOverlay.instanceFromNib() as? AddQuestionOverlay
        
        let icon = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
        view?.nextBtn.image = icon?.tint(with: UIColor.black)
       
        
//        let icon1 = FAKIonIcons.closeRoundIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
//        view?.cancelBtn.image = icon1?.tint(with: UIColor.white)

        
        let icon2 = FAKIonIcons.chevronLeftIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
        view?.backBtn.image = icon2?.tint(with: UIColor.black)
        
        
        view?.pageCont.currentPage = 0;
        view?.delegate = self
        return view
    }
    
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool) {
        print("touch nextBtn")
        if didSelectNextPage
        {
            if currentPage == 0
            {
                if questionTopicPage.fillQuestion(question: question)
                {
                    currentPage = currentPage+1
                    swiftyOnboard?.goToPage(index: currentPage, animated: true)
                }
            }
            else if currentPage == 1
            {
                if questionImagePage.fillQuestion(question: question)
                {
                    currentPage = currentPage+1
                    swiftyOnboard?.goToPage(index: currentPage, animated: true)
                }

            }
            else if currentPage == 2
            {
                if questionTutorPage.fillQuestion(question: question)
                {
                    saveQuestion()
                }
            }
            

        }
        else
        {
            if currentPage > 0
            {
                currentPage = currentPage-1
                swiftyOnboard?.goToPage(index: currentPage, animated: true)
            }
            else if currentPage == 0
            {
                self.dismiss(animated: true, completion: nil)
            }

        }
        
    }
    
    func saveQuestion()
    {
        print(question.image)
        QuestionService.getInstance().addQuestion(question: question).subscribe(onNext:{res in
            print(res)
            if (res.success)! == 1
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.showToast(message: res.message!, bgColor: .red)
            }
        })
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: CustomSwiftyOnboard, atIndex index: Int) -> UIColor?{
        return UIColor(rgb:0x4D2E7F)
    }
    
    func swiftyOnboard(_ swiftyOnboard: CustomSwiftyOnboard, tapped index: Int) {
        self.view.endEditing(true)
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: CustomSwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        print(position)
        let overlayPage = overlay as! AddQuestionOverlay
        let currentPage = round(position)
        self.currentPage = Int(currentPage);
        overlayPage.pageCont.currentPage = self.currentPage
        
        if currentPage == 2
        {
            let icon1 = FAKIonIcons.checkmarkRoundIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
            (overlay as! AddQuestionOverlay).nextBtn.image = icon1?.tint(with: UIColor.black)
            (overlay as! AddQuestionOverlay).nextBtn.title = "ارسال"
        }
        else
        {
            (overlay as! AddQuestionOverlay).nextBtn.title = "بعدی"
            let icon1 = FAKIonIcons.chevronLeftIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
            (overlay as! AddQuestionOverlay).backBtn.image = icon1?.tint(with: UIColor.black)
            let icon = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
            (overlay as! AddQuestionOverlay).nextBtn.image = icon?.tint(with: UIColor.black)
        }
        
        switch currentPage {
        case 0:
            (overlay as! AddQuestionOverlay).toolbar.titleLabel.text = "انتخاب موضوع و مقطع"
            (overlay as! AddQuestionOverlay).backBtn.title = "بازگشت"
        case 1:
            (overlay as! AddQuestionOverlay).toolbar.titleLabel.text = "انتخاب تصویر سوال"
            (overlay as! AddQuestionOverlay).backBtn.title = "قبلی"
            
        case 2:
            (overlay as! AddQuestionOverlay).toolbar.titleLabel.text = "انتخاب پاسخ دهنده"
            (overlay as! AddQuestionOverlay).backBtn.title = "قبلی"
        default:
            print("")
        }
    }
    
    
    func swiftyOnboard(_ swiftyOnboard: CustomSwiftyOnboard, currentPage index: Int) {
        if index == 1
        {

            if questionTopicPage.fillQuestion(question: self.question)
            {
                print(question.grade)
            }
            else
            {
                swiftyOnboard.goToPage(index: 0, animated: true)
            }
        }
        if index == 2
        {
            if questionImagePage.fillQuestion(question: self.question)
            {
                print(question.image?.id)
            }
            else
            {
                swiftyOnboard.goToPage(index: 1, animated: true)
            }
            
        }
    }
    
    
   
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        UIApplication.shared.statusBarStyle = .lightContent
//        UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
//    }
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return .lightContent
//    }

    
    
}


