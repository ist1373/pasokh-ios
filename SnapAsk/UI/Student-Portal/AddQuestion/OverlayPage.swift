//
//  OverlayPage.swift
//  Booksum
//
//  Created by iman on 10/22/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit
import SwiftyOnboard
import Material


public protocol OverlayPageDelegate: class {
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool)
}

public extension OverlayPageDelegate {
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool) {}
}



public class OverlayPage: SwiftyOnboardOverlay {
    
    open weak var delegate: OverlayPageDelegate?
    
    @IBOutlet weak var pageController: UIPageControl!
    
    @IBAction func onNextPageSelect(_ sender: Any) {
        delegate?.overlayPage(self, didSelectNextPage: true)
        print("<#T##items: Any...##Any#>")
    }
    
    @IBOutlet weak var nextPage: IconButton!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "OverlayPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
