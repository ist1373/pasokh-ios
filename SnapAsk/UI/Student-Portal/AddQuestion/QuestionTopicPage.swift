//
//  BookDetailsPage.swift
//  Booksum
//
//  Created by iman on 11/12/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework


class QuestionTopicPage: SwiftyOnboardPage, UIScrollViewDelegate {

    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var seventh_btn: RaisedButton!
    
    @IBOutlet weak var eighth_btn: RaisedButton!
    
    
    @IBOutlet weak var nineth_btn: RaisedButton!
    
    
    @IBOutlet weak var tenth_btn: RaisedButton!
    
    
    @IBOutlet weak var eleventh_btn: RaisedButton!
    
    
    @IBOutlet weak var twelvth_btn: RaisedButton!
    
    
    
    var togglesBtn : [Int:RaisedButton] = [:]
    
    
    @IBOutlet weak var math_radio: DLRadioButton!
    @IBOutlet weak var englich_radio: DLRadioButton!
    
    
    
    var questionTopic:QuestionTopic?
    var questionGrade:Grade?
    var gradeSelected:Int?


    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "QuestionTopicPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    

    
    func initUI()
    {
        scrollView.delegate = self
        
        togglesBtn[7] = seventh_btn
        togglesBtn[8] = eighth_btn
        togglesBtn[9] = nineth_btn
        togglesBtn[10] = tenth_btn
        togglesBtn[11] = eleventh_btn
        togglesBtn[12] = twelvth_btn
        
        
        seventh_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        eighth_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        nineth_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        tenth_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        eleventh_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        twelvth_btn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        
    
    }
    
    @IBAction func onSelectTopic(_ sender: DLRadioButton) {
        if let str =  sender.selected()?.titleLabel?.text
        {
            if str == "ریاضی"
            {
                questionTopic = QuestionTopic.Math
                print("1")
            }
            else if str == "فیزیک"
            {
                questionTopic = QuestionTopic.Physics
                print("2")
            }
            else if str == "شیمی"
            {
                questionTopic = QuestionTopic.Chemistry
                print("3")
            }
            else if str == "زیست"
            {
                questionTopic = QuestionTopic.Biology
                print("4")
            }
            else if str == "زبان"
            {
                questionTopic = QuestionTopic.English
                print("5")
            }
            else if str == "ادبیات"
            {
                questionTopic = QuestionTopic.Literature
                print("6")
            }
            else if str == "عربی"
            {
                questionTopic = QuestionTopic.Arabic
                print("7")
            }
            else if str == "دینی"
            {
                questionTopic = QuestionTopic.Religious
                print("8")
            }
        }
    }
    

    
    func fillQuestion(question:Question) ->  Bool
    {
        if let qt = questionTopic
        {
            if let qg = questionGrade
            {
                question.grade = qg
                question.questionTopic = qt
                return true
            }
            else
            {
                showToast(message: "لطفا مقطع تحصیلی را انتخاب کنید")
            }
        }
        else
        {
           showToast(message: "لطفا موضوع سوال را انتخاب کنید")
        }
        
        return false
    }
    
    func showToast(message:String)
    {
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = .red
        style.cornerRadius = 15
        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
        
        self.parentViewController?.view.makeToast(message,duration: 3.0, position: .center, style: style)
    }
    
    
    @IBAction func onTouchSeventh(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 7
            {
                gradeSelected = 7
                questionGrade = Grade.Seventh
            }
        }
        else
        {
            gradeSelected = 7
            questionGrade = Grade.Seventh
        }
        updateGrades()
    }
    
    @IBAction func onTouchEight(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 8
            {
                gradeSelected = 8
                questionGrade = Grade.Eighth
            }
        }
        else
        {
            gradeSelected = 8
            questionGrade = Grade.Eighth
        }
        updateGrades()
    }
    
    @IBAction func onTouchNine(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 9
            {
                gradeSelected = 9
                questionGrade = Grade.Ninth
            }
        }
        else
        {
            gradeSelected = 9
            questionGrade = Grade.Ninth
        }
        updateGrades()
    }
    
    @IBAction func onTouchTen(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 10
            {
                gradeSelected = 10
                questionGrade = Grade.Tenth
            }
        }
        else
        {
            gradeSelected = 10
            questionGrade = Grade.Tenth
        }
        updateGrades()
    }
    
    @IBAction func onTouchEleven(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 11
            {
                gradeSelected = 11
                questionGrade = Grade.Eleventh
            }
        }
        else
        {
            gradeSelected = 11
            questionGrade = Grade.Eleventh
        }
        updateGrades()
    }
    
    @IBAction func onTouchTwelve(_ sender: Any) {
        if let gs = gradeSelected
        {
            if gs != 12
            {
                gradeSelected = 12
                questionGrade = Grade.Twelfth
            }
        }
        else
        {
            gradeSelected = 12
            questionGrade = Grade.Twelfth
        }
        updateGrades()
    }
    
    
    
    func updateGrades()
    {
        
        for i in 7...12
        {
            if i == gradeSelected!
            {
                togglesBtn[i]?.animate(.background(color: GlobalProperties.PrimaryColor))
                togglesBtn[i]?.titleColor = .white
            }
            else
            {
                togglesBtn[i]?.animate(.background(color: .white))
                togglesBtn[i]?.titleColor = GlobalProperties.PrimaryColor
            }
            
        }
    }
    
    
}






