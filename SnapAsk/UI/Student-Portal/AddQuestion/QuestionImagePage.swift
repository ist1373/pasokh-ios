//
//  QuestionImagePage.swift
//  SnapAsk
//
//  Created by iman on 6/9/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import CameraViewController
import NVActivityIndicatorView
import ToastSwiftFramework

class QuestionImagePage: SwiftyOnboardPage, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UploadCompleteDelegate {

    
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    @IBOutlet weak var questionDesc_tv: TextView!
    @IBOutlet weak var questionImage: UIImageView!
    
    var uploadedFile:UploadedFile?
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "QuestionImagePage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    
    
    func initUI()
    {
        FileService.getInstance().delegate = self
        
        questionDesc_tv.layer.cornerRadius = 5
        questionDesc_tv.layer.borderColor = GlobalProperties.lightColor.cgColor
        questionDesc_tv.layer.borderWidth = 1
        questionDesc_tv.font = UIFont(name:"IRANSansMobile" , size:14)!
        questionDesc_tv.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(QuestionImagePage.onSelectImage))
        questionImage.addGestureRecognizer(imageGesture)
      
    }
    
    
    @objc func onSelectImage()
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        
        
        let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
        let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true){ [weak self] image, asset in

            if let img = image{
                self?.spinner.isHidden = false
                self?.spinner.startAnimating()

                self?.questionImage.image = img
                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
            }
            self?.parentViewController?.dismiss(animated: true, completion: nil)
        }
        
//        let imagePickerViewController = CameraViewController.imagePickerViewController(croppingParameters: croppingEnabled) { [weak self] image, asset in
//
//            if let img = image{
//                self?.spinner.isHidden = false
//                self?.spinner.startAnimating()
//
//                self?.questionImage.image = img
//                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
//            }
//            self?.parentViewController?.dismiss(animated: true, completion: nil)
//        }
        
        self.parentViewController?.present(imagePickerViewController, animated: true, completion: nil)
    }
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        print(imageId)
        print(success)
        if success
        {
            uploadedFile = UploadedFile()
            uploadedFile?.fileType = "Public"
            uploadedFile?.id = imageId
        }
        spinner.isHidden = true
        spinner.stopAnimating()
    }
    
    func fillQuestion(question:Question) ->  Bool
    {
        print(uploadedFile?.id)
        if let uf = uploadedFile
        {
            if questionDesc_tv.text.characters.count >= 10
            {
                question.image = uf
                question.description = questionDesc_tv.text
                return true
            }
            else
            {
                showToast(message: "لطفا توضیح سوال خود را تکمیل کنید(حداقل ۱۰ حرف)")
            }
        }
        else
        {
            showToast(message: "لطفا تصویر سوال خود را انتخاب کنید")
        }
        
        return false
    }
    
    func showToast(message:String)
    {
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = .red
        style.cornerRadius = 15
        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
        
        self.parentViewController?.view.makeToast(message,duration: 3.0, position: .center, style: style)
    }
    
}
