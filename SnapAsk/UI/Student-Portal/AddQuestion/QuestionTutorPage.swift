//
//  QuestionTutorPage.swift
//  SnapAsk
//
//  Created by iman on 6/9/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyOnboard
import Validator
import EasyTipView
import Material
import Motion
import DLRadioButton
import ToastSwiftFramework


class QuestionTutorPage: SwiftyOnboardPage ,UITableViewDelegate,UITableViewDataSource{

    
    var answerType:AnswerType?
    var responder:User?
    
    var selcted:Int?
    
    var tutors:[Tutor] = []

    
    @IBOutlet weak var favorateTutor_tbl: UITableView!
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "QuestionTutorPage", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func initUI()
    {
        favorateTutor_tbl.allowsSelection = false
        favorateTutor_tbl.isScrollEnabled = false
        favorateTutor_tbl.alpha = 0.5
        
        self.favorateTutor_tbl.register(UINib(nibName: "FavoriteTutorCell", bundle: nil), forCellReuseIdentifier: "FavoriteTutorCell")

        favorateTutor_tbl.delegate = self
        favorateTutor_tbl.dataSource = self
        
        loadFavoriteTutors()
        
    }
    
    func loadFavoriteTutors()  {
        
        if let ob = FavoriteTutorService.getInstance().getFavoriteTutors()
        {
            ob.subscribe(onNext:{res in
                print(res)
                self.tutors = res
                self.favorateTutor_tbl.reloadData()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tutors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = favorateTutor_tbl.dequeueReusableCell(withIdentifier: "FavoriteTutorCell",for: indexPath) as! FavoriteTutorCell
        cell.title_lbl.text = "تستتت"
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.card.depthPreset = .depth1
        cell.avatar.setRounded()
        cell.title_lbl.text =  (tutors[indexPath.row].firstName ?? "") + " " + (tutors[indexPath.row].lastName ?? "")
//        cell.selectedBackgroundView?.backgroundColor = GlobalProperties.PrimaryColor
        if indexPath.row == selcted
        {
            cell.card.backgroundColor = GlobalProperties.PrimaryColor
        }
        else
        {
            cell.card.backgroundColor = .white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCell:FavoriteTutorCell = favorateTutor_tbl.cellForRow(at: indexPath)! as! FavoriteTutorCell
        selectedCell.card.backgroundColor = GlobalProperties.PrimaryColor
        responder = tutors[indexPath.row]
        selcted = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let selectedCell:FavoriteTutorCell = favorateTutor_tbl.cellForRow(at: indexPath) as? FavoriteTutorCell
        {
          selectedCell.card.backgroundColor = .white
        }
    }

    
    
    @IBAction func onSelectType(_ sender: DLRadioButton) {
        if let str =  sender.selected()?.titleLabel?.text {
            if str == "عمومی"
            {
                answerType = AnswerType.Public
                favorateTutor_tbl.allowsSelection = false
                favorateTutor_tbl.isScrollEnabled = false
                favorateTutor_tbl.alpha = 0.5
            }
            else {
                answerType = AnswerType.Favorite
                favorateTutor_tbl.allowsSelection = true
                favorateTutor_tbl.isScrollEnabled = true
                favorateTutor_tbl.alpha = 1
            }
        }
    }
    
    
    func fillQuestion(question:Question) -> Bool
    {
        
        if let at = answerType
        {
            if answerType == AnswerType.Public
            {
                question.responder = nil
                return true
            }
            else {
                if let res = responder
                {
                    question.responder = res
                    return true
                }
                else
                {
                    showToast(message: "لطفا یکی از پاسخ دهندگان محبوب خود را انتخاب کنید")
                    return false
                }
            }
            
        }
        else
        {
            showToast(message: "لطفا نوع پاسخ دهنده خود را انتخاب کنید")
            return false
        }
    }
    
    func showToast(message:String)
    {
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = .red
        style.cornerRadius = 15
        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
        self.parentViewController?.view.makeToast(message,duration: 3.0, position: .center, style: style)
    }
}

enum AnswerType:String {
    case Public
    case Favorite
}




