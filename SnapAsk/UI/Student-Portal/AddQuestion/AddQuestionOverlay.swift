//
//  AddBookOverlay.swift
//  Booksum
//
//  Created by iman on 11/12/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit
import Material
import SwiftyOnboard




class AddQuestionOverlay: SwiftyOnboardOverlay {
    open weak var delegate: OverlayPageDelegate?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    //@IBOutlet weak var stepperIndicator: StepIndicatorView!
    
   
    @IBOutlet weak var pageCont: UIPageControl!
    
    @IBOutlet weak var toolbar: Toolbar!
    
    @IBOutlet weak var nextBtn: RaisedButtonRTL!
    @IBOutlet weak var backBtn: RaisedButtonLTR!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AddQuestionOverlay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func onNextTouch(_ sender: Any) {
        print("touch nextBtn")
        self.delegate?.overlayPage(self, didSelectNextPage: true)
    }
    
    
    @IBAction func onCancelTouch(_ sender: Any) {
        self.parentViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onBackTouch(_ sender: Any) {
        self.delegate?.overlayPage(self, didSelectNextPage: false)
    }
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        initToolbar()
    }
    
    func initToolbar()  {
        nextBtn.shadowColor = .clear
        backBtn.shadowColor = .clear
        toolbar.depthPreset = .depth2
        toolbar.backgroundColor = UIColor.white
        toolbar.titleLabel.font = UIFont(name: "Mj_Flow Bold", size: 15)
        toolbar.titleLabel.text = "افزودن سوال";
        toolbar.titleLabel.textColor =  GlobalProperties.PrimaryColor
        toolbar.layer.zPosition = 10;
    }
    

}
