//
//  ST_EditProfileViewController.swift
//  SnapAsk
//
//  Created by iman on 6/25/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit
import DropDown
import NVActivityIndicatorView
import CameraViewController
import Kingfisher


class ST_EditProfileViewController: UIViewController,LocationTableViewDelegate ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate,UploadCompleteDelegate {

    

    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    @IBOutlet weak var toolbar: Toolbar!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var school: UITextField!
    @IBOutlet weak var grade: UITextField!
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var avg: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var passrep: UITextField!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var editImage_btn: RaisedButton!
    @IBOutlet weak var save_btn: RaisedButton!
    
    let dropDownField = DropDown()
    let dropDownGrade = DropDown()
    
    var student:Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        getMyUser()
        
        
        
        
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMyUser()
    {
        spinner.isHidden = false
        spinner.startAnimating()
        StudentService.getInstance().getAuthenticatedStudetn(self).subscribe(onNext:{res in
            self.student = res
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.initUI()
            
        })
    }
    
    func initUI(){
        editImage_btn.cornerRadiusPreset = .cornerRadius2
        save_btn.cornerRadiusPreset = .cornerRadius2
        image.setRounded()
        
        
        city.setAsDropDown()
        city.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ST_EditProfileViewController.onShowProvinces)))
        
      
        
        
        field.setAsDropDown()
        field.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ST_EditProfileViewController.onShowFields)))
        
        dropDownField.anchorView = field
        dropDownField.dataSource = ["ریاضی فیزیک", "علوم تجربی", "علوم انسانی" , "فنی حرفه ای"]
        dropDownField.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .right
            cell.optionLabel.font = GlobalProperties.getIranSansFont(size: 13)
        }
        
        dropDownField.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.field.text = item
            switch item {
            case "فنی حرفه ای":
                self.student!.field = Field.Technical
                break
            case "ریاضی فیزیک":
                self.student!.field = Field.Math
                break
            case "علوم تجربی":
                self.student!.field = Field.NaturalScience
                break
            case "علوم انسانی":
                self.student!.field = Field.Humanities
                break
            default:
                break
    }
        }

        
        grade.setAsDropDown()
        grade.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ST_EditProfileViewController.onShowGrade)))
        dropDownGrade.anchorView = grade
        dropDownGrade.dataSource = ["هفتم", "هشتم", "نهم","دهم","یازدهم","دوازدهم"]
        
        dropDownGrade.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .right
            cell.optionLabel.font = GlobalProperties.getIranSansFont(size: 13)
        }
        
        dropDownGrade.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.grade.text = item
            switch item {
            case "هفتم":
                print("seventh")
                self.student!.grade = Grade.Seventh
                break
            case "هشتم":
                self.student!.grade = Grade.Eighth
                break
            case "نهم":
                self.student!.grade = Grade.Ninth
                break
            case "دهم":
                self.student!.grade = Grade.Tenth
                break
            case "یازدهم":
                self.student!.grade = Grade.Eleventh
                break
            case "دوازدهم":
                self.student!.grade = Grade.Twelfth
                break
            default:
                break
            }
        }
        
        fillUserData()
    }
    
    func fillUserData()
    {
        print(student?.average)
        print("student?.average")
        if let fname = student?.firstName
        {
            firstName.text = fname
        }
        if let lname = student?.lastName
        {
            lastName.text = lname
        }
        if let cit = student?.city
        {
            city.text = cit.title
        }
        if let av = student?.average
        {
            print("avg.text")
            avg.text = "\(av)"
        }
        if let schoo = student?.school
        {
            school.text = schoo
        }
        if let g = student?.grade
        {
            grade.text = g.description
            dropDownGrade.selectRow(at: g.hashValue)
        }
        if let f = student?.field
        {
            field.text = f.description
            dropDownField.selectRow(at: f.hashValue)
        }
        
        if let e = student?.email
        {
            email.text = e
        }
        if let pi = student?.profileImage
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((pi.id)!)))!
            image.kf.setImage(with: url)
        }
    }
    

    
    @objc func onShowProvinces()
    {
        let storyboard: UIStoryboard = UIStoryboard(name: "Location", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil) //(vc, sender: self)
        vc.locationTableView.locationDelegate = self
    }
    
    @objc func onShowFields()
    {
        dropDownField.show()
    }
    
    @objc func onShowGrade()
    {
        dropDownGrade.show()
    }
    
    
    
    func initToolbar()  {
        //UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
        
        toolbar.depthPreset = .depth2
        toolbar.backgroundColor = UIColor.white
        toolbar.titleLabel.font = GlobalProperties.getIranSansFont(size: 14)
        toolbar.titleLabel.text = "ویرایش اطلاعات";
        toolbar.titleLabel.textColor =  GlobalProperties.PrimaryColor
        toolbar.layer.zPosition = 10;
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: .black)
        
        let backBtn = IconButton(image: backImg?.tint(with: .black), tintColor: .black)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        toolbar.rightViews = [backBtn]
        
    }
    
    func onSelectCity(city: City) {
        print(city)
        self.city.text = city.title
        student?.city = city
    }
    
    
    
    @objc func back()
    {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        UIApplication.shared.statusBarStyle = .lightContent
//        UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
//    }

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func onEditImage(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        FileService.getInstance().delegate = self
        
        
        let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
        let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true) { [weak self] image, asset in
            
            if let img = image{
                self?.spinner.isHidden = false
                self?.spinner.startAnimating()
                
                self?.image.image = img
                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
            }
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(imagePickerViewController, animated: true, completion: nil)
    }
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        spinner.isHidden = true
        spinner.stopAnimating()
        let uf = UploadedFile()
        uf.id = imageId
        student?.profileImage = uf;
    }
    
    
    
    @IBAction func onSave(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        if chechValidation()
        {
            if let temp = firstName.text
            {
                student?.firstName = temp
            }
            if let temp = lastName.text
            {
                student?.lastName = temp
            }
            if let temp = email.text
            {
                student?.email = temp
            }
            if let temp = avg.text
            {
                student?.average = Float(temp)
            }
            if let temp = school.text
            {
                student?.school = temp
            }
            
            StudentService.getInstance().editStudent(student: student!).subscribe(onNext:{res in
                if res.success! == 1
                {
                    self.parent?.showToast(message: "تغییرات با موفقیت انجام شد")
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    self.showToast(message: res.message!)
                }
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
            })
        }
        


    }
    
    func chechValidation() -> Bool
    {
        if let temp = pass.text
        {
            if temp == passrep.text
            {
               student?.password = temp
               return true
            }
            else
            {
                print("not equal")
                showToast(message: "رمز عبور با تکرار آن برابر نیست")
                return false
            }
        }
        return true
    }
    
    
    
    
}
