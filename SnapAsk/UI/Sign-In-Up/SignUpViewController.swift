//
//  SignUpViewController.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import Validator
import EasyTipView
import NVActivityIndicatorView
import ToastSwiftFramework

class SignUpViewController: UIViewController {

    @IBOutlet weak var studentRole_view: UIView!
    @IBOutlet weak var tutorRole_view: UIView!
    
    @IBOutlet weak var email_tf: TextField!
    @IBOutlet weak var pass_tf: TextField!
    @IBOutlet weak var passrep_tf: TextField!
    @IBOutlet weak var phone_tf: TextField!
    @IBOutlet weak var inviteToken_tf: TextField!
    
    
    @IBOutlet weak var signIn_lbl: UILabel!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var register: RaisedButton!
    
    
    var registerType:RegisterType?;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let studentGesture = UITapGestureRecognizer(target: self, action: #selector(onTouchStudent))
        let tutorGesture = UITapGestureRecognizer(target: self, action: #selector(onTouchTutor))
        let signInGesture = UITapGestureRecognizer(target: self, action: #selector(goToSignIn))
        
        studentRole_view.addGestureRecognizer(studentGesture)
        tutorRole_view.addGestureRecognizer(tutorGesture)
        signIn_lbl.addGestureRecognizer(signInGesture)
        
        setTextFieldDesign(textField: email_tf)
        setTextFieldDesign(textField: pass_tf)
        setTextFieldDesign(textField: passrep_tf)
        setTextFieldDesign(textField: phone_tf)
        setTextFieldDesign(textField: inviteToken_tf)
        
        
        email_tf.placeholder = "ایمیل"
        pass_tf.placeholder = "رمز عبور"
        passrep_tf.placeholder = "تکرار رمز عبور"
        phone_tf.placeholder = "شماره تلفن"
        inviteToken_tf.placeholder = "کد دعوت دوستان"

        register.layer.cornerRadius = 25
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onRegister(_ sender: Any) {
        if(NetworkReachability.isConnectedToNetwork())
        {
            if(checkValidation())
            {
                indicator.isHidden = false
                indicator.startAnimating()
                if(registerType! == .Student)
                {
                    registerStudent()
                }
                else
                {
                    registerTutor()
                }
            }
        }
        else
        {
            var style = ToastStyle()
            style.messageColor = .white
            style.backgroundColor = .red
            style.cornerRadius = 15
            style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
            self.view.makeToast("اتصال به اینترنت بررسی شود.",duration: 3.0, position: .bottom, style: style)
        }
    }
    
    
    func registerStudent() {
        let studetnDto = StudentDto()
        studetnDto.email = email_tf.text!
        studetnDto.password = pass_tf.text!
        studetnDto.mobileNumber = phone_tf.text!
        if inviteToken_tf.text!.characters.count > 0
        {
           studetnDto.invitedToken = inviteToken_tf.text!
        }
        
        //TODO: add device info
        
        StudentService.getInstance().registerStudent(studentDto: studetnDto).subscribe(
            onNext:{ response in
                print("responseeeeeeeeeeeee")
                print(response)
                if response.success! == 1 {
                    try? AppDelegate.getStorage().setObject("Student", forKey: "userType", expiry: .never)
                    self.goToVerification()
                }
                else
                {
                    self.showToast(message: response.message!)
                }
                self.indicator.isHidden = true
                self.indicator.stopAnimating()
        },  onError:{error in
                self.showToast(message: "خطا در ارتباط با سرور")
            })
    }
    
    func registerTutor()
    {
        let tutorDto = TutorDto()
        tutorDto.email = email_tf.text!
        tutorDto.password = pass_tf.text!
        tutorDto.mobileNumber = phone_tf.text!
        //TODO: add device info
        
        TutorService.getInstance().registerTutor(tutorDto: tutorDto).subscribe(
            onNext:{ response in
                print("responseeeeeeeeeeeee")
                print(response)
                if response.success! == 1{
                    try? AppDelegate.getStorage().setObject("Tutor", forKey: "userType", expiry: .never)
                    self.goToVerification()
                }
                else
                {
                    self.showToast(message: response.message!)
                }
                self.indicator.isHidden = true
                self.indicator.stopAnimating()
        },  onError:{error in
            self.showToast(message: "خطا در ارتباط با سرور")
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        })
    }
    
    func goToVerification()
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
        vc.phone = phone_tf.text!
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func checkValidation() -> Bool
    {
        if let rt = registerType{
        }
        else
        {
            showToast(message: "لطفا نوع حساب کاربری خود را مشخص کنید")
            return false
        }
        if (pass_tf.text?.characters.count)! < 6
        {
            showToast(message: "لازم است رمز عبور حداقل ۶ حرف باشد")
            return false
        }
        if (pass_tf.text != passrep_tf.text)
        {
            showToast(message: "رمز عبور با تکرار آن برابر نیست")
            return false
        }
        return true
    }
    
    
//    
//    func showToast(message:String)
//    {
//        var style = ToastStyle()
//        style.messageColor = .white
//        style.backgroundColor = .red
//        style.cornerRadius = 15
//        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
//        self.view.makeToast(message,duration: 3.0, position: .bottom, style: style)
//    }
    
    
    
    
    
    @objc func onTouchStudent()
    {
        studentRole_view.animate(.scale(1.1),.fade(1),.delay(0.1))
        tutorRole_view.animate(.scale(0.9),.fade(0.7),.delay(0.1))
        registerType = RegisterType.Student
        inviteToken_tf.isHidden = false
    }
    
    @objc func onTouchTutor()
    {
        studentRole_view.animate(.scale(0.9),.fade(0.7),.delay(0.1))
        tutorRole_view.animate(.scale(1.1),.fade(1),.delay(0.1))
        registerType = RegisterType.Tutor
        inviteToken_tf.isHidden = true
    }
    
    @objc func goToSignIn()
    {
        self.dismiss(animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func setTextFieldDesign(textField:TextField)
    {
        textField.textColor = Color.white
        textField.textAlignment = .center
        textField.dividerNormalColor = Color.white
        textField.dividerActiveColor = Color.white
        textField.font = UIFont(name:"IRANSansMobile" , size:18)!
        textField.placeholderNormalColor = Color.white
        textField.placeholderActiveColor = Color.white
        textField.placeholderActiveScale = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

}

enum RegisterType{
    case Student
    case Tutor
}










