//
//  ForgotPassViewController.swift
//  SnapAsk
//
//  Created by iman on 5/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit


class ForgotPassViewController: UIViewController {

    
    @IBOutlet weak var empho_tf: TextField!
    @IBOutlet weak var forgot_btn: RaisedButton!
    @IBOutlet weak var back_btn: FlatButton!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        forgot_btn.layer.cornerRadius = 25
        
        var backImg = FAKIonIcons.chevronRightIcon(withSize: 17).image(with: CGSize(width: 17, height: 17)).tint(with: .white)
        
        back_btn.image = backImg
        
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onForgot(_ sender: Any) {
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
