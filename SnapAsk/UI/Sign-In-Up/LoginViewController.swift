//
//  LoginViewController.swift
//  SnapAsk
//
//  Created by iman on 5/19/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import Validator
import EasyTipView
import NVActivityIndicatorView
import ToastSwiftFramework

protocol UserLogedInDelegate: class {
    func onUserLogedIn(user: User)
}


class LoginViewController: UIViewController {

    weak var delegate: UserLogedInDelegate? = nil
    
    @IBOutlet weak var phone_tf: TextField!
    @IBOutlet weak var password_tf: TextField!
    
    @IBOutlet weak var signIn_btn: RaisedButton!
    @IBOutlet weak var signUp_lbl: UILabel!
    @IBOutlet weak var forgotPass_lbl: UILabel!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        phone_tf.placeholder = "شماره موبایل / ایمیل"
        password_tf.placeholder = "رمز عبور"

        setTextFieldDesign(textField: password_tf)
        setTextFieldDesign(textField: phone_tf)
        
        signIn_btn.layer.cornerRadius = 25
        
        let signUpGesture = UITapGestureRecognizer(target: self, action: #selector(onTouchSignUp))
        signUp_lbl.addGestureRecognizer(signUpGesture)
        
   
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTextFieldDesign(textField:TextField)
    {
        textField.textColor = Color.white
        textField.textAlignment = .center
        textField.dividerNormalColor = Color.white
        textField.dividerActiveColor = Color.white
        textField.font = UIFont(name:"IRANSansMobile" , size:18)!
        textField.placeholderNormalColor = Color.white
        textField.placeholderActiveColor = Color.white
        textField.placeholderActiveScale = 0
    }
    
    @objc func onTouchSignUp() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func onTouchForgot(_ sender: Any) {
        print("imannnnnnn")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassViewController")
        self.present(vc!, animated: true, completion: nil)
    }
    
    
    @IBAction func onLogin(_ sender: Any) {

        if NetworkReachability.isConnectedToNetwork()
        {
            indicator.isHidden = false
            indicator.startAnimating()
            login(empho: phone_tf.text!, pass: password_tf.text!)
        }
        else
        {
            var style = ToastStyle()
            style.messageColor = .white
            style.backgroundColor = .red
            style.cornerRadius = 15
            style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
            self.view.makeToast("اتصال به اینترنت بررسی شود.",duration: 3.0, position: .bottom, style: style)
        }
    }
    
    func login(empho:String,pass:String)
    {
        
        UserService.getInstance().userLogin(empho: phone_tf.text!, password: password_tf.text!).subscribe(onNext:{
            response  in
            
            if response.statusCode! >= 200 && response.statusCode! < 300
            {
                try! AppDelegate.getStorage().setObject(response.token!, forKey: "token",expiry: .never)
                self.getUser(response: response)
            }
            else if response.statusCode == 401
            {
                var style = ToastStyle()
                style.messageColor = .white
                style.backgroundColor = .red
                style.cornerRadius = 15
                style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
                self.view.makeToast("پست الکترونیک یا رمزعبور نادرست است",duration: 3.0, position: .bottom, style: style)
            }
            else {
                var style = ToastStyle()
                style.messageColor = .white
                style.backgroundColor = .red
                style.cornerRadius = 15
                style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
                self.view.makeToast("خطا در ارتباط با سرور",duration: 3.0, position: .bottom, style: style)
            }
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            
        },onError:{
            error in
            var style = ToastStyle()
            style.messageColor = .white
            style.backgroundColor = .red
            style.cornerRadius = 15
            style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
            self.view.makeToast("خطا در ارتباط با سرور",duration: 3.0, position: .bottom, style: style)
            print(error)
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        })
    }
    
    func openStudentStoryBoard() {
        let sb = UIStoryboard(name: "StudentMain", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ST_ContainerViewController")
        self.present(vc, animated: true, completion: nil)
        
    }
    func openTutorStoryBoard(){
        let sb = UIStoryboard(name: "TutorMain", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "TT_ContainerViewController")
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func getUser(response:JwtAuthenticationResponse)  {
        
        UserService.getInstance().getAuthenticatedUser(self).subscribe(onNext:{
            user in
            try? AppDelegate.getStorage().setObject(user, forKey: "user", expiry: .never)
            self.delegate?.onUserLogedIn(user: user)
            
            if let auths = response.authorities
            {
                for auth in auths
                {
                    if auth.authority == "ROLE_STUDENT"
                    {
                        try? AppDelegate.getStorage().setObject("Student", forKey: "userType",expiry: .never)
                        self.openStudentStoryBoard()
                    }
                    else if auth.authority == "ROLE_TUTOR"
                    {
                        try? AppDelegate.getStorage().setObject("Tutor", forKey: "userType",expiry: .never)
                        self.openTutorStoryBoard()
                    }
                    
                }
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

}
