//
//  TT_RegDetailsViewController.swift
//  SnapAsk
//
//  Created by iman on 5/28/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import CameraViewController
import NVActivityIndicatorView
import ToastSwiftFramework
import DLRadioButton


class TT_RegDetailsViewController: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate,UploadCompleteDelegate {

    var tutor:Tutor?
    
    
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    @IBOutlet weak var name_tf: TextField!
    
    @IBOutlet weak var family_tf: TextField!
    
    @IBOutlet weak var biography_tv: UITextView!
    
    @IBOutlet weak var pickProfile_btn: RaisedButton!
    
    @IBOutlet weak var profile_img: UIImageView!
    
    @IBOutlet weak var next_btn: RaisedButton!
    
    
    @IBOutlet weak var bioBtn: RaisedButton!
    
    @IBOutlet weak var phsBtn: RaisedButton!
    
    @IBOutlet weak var chmBtn: RaisedButton!
    
    @IBOutlet weak var mthBtn: RaisedButton!
    
    @IBOutlet weak var engBtn: RaisedButton!
    
    @IBOutlet weak var rlgBtn: RaisedButton!
    
    @IBOutlet weak var arbBtn: RaisedButton!
    
    @IBOutlet weak var ltcBtn: RaisedButton!
    
    var togglesBtn : [RaisedButton:Bool] = [:]

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setTextFieldDesign(textField: name_tf)
        setTextFieldDesign(textField: family_tf)
        
        pickProfile_btn.layer.cornerRadius = 25
        next_btn.layer.cornerRadius = 25
        
        name_tf.placeholder = "نام"
        family_tf.placeholder = "نام خانوادگی"
        biography_tv.layer.cornerRadius = 10
        biography_tv.font = UIFont(name:"IRANSansMobile" , size:14)!
        biography_tv.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        profile_img.setRounded()
        
        FileService.getInstance().delegate = self
        
        
        bioBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        phsBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        chmBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        mthBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        engBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        rlgBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        arbBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        ltcBtn.setAsToggleButton(color: GlobalProperties.PrimaryColor)
        // Do any additional setup after loading the view.
        togglesBtn[bioBtn] = false
        togglesBtn[phsBtn] = false
        togglesBtn[chmBtn] = false
        togglesBtn[mthBtn] = false
        togglesBtn[engBtn] = false
        togglesBtn[rlgBtn] = false
        togglesBtn[arbBtn] = false
        togglesBtn[ltcBtn] = false
        
        
        TutorService.getInstance().getAuthenticatedTutor().subscribe(onNext:{t in
            self.tutor = t
            self.tutor?.topics = [];
        })


   
       
       
           
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setTextFieldDesign(textField:TextField)
    {
        textField.textColor = Color.white
        textField.textAlignment = .right
        textField.dividerNormalColor = Color.white
        textField.dividerActiveColor = Color.white
        textField.font = UIFont(name:"IRANSansMobile" , size:18)!
        textField.placeholderNormalColor = Color.white
        textField.placeholderActiveColor = Color.white
        textField.placeholderActiveScale = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func onSelectImage(_ sender: Any) {
 
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        
        
        let croppingEnabled = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize(width: 50, height: 50))
        let imagePickerViewController = CameraViewController(croppingParameters: croppingEnabled, allowsLibraryAccess: true) { [weak self] image, asset in

            if let img = image{
                self?.spinner.isHidden = false
                self?.spinner.startAnimating()
                
                self?.profile_img.image = img
                FileService.getInstance().uploadImage(img, accessType: AccessType.Public)
            }
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(imagePickerViewController, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func onNext(_ sender: Any) {
       if checkValidation()
       {
        tutor?.firstName = name_tf.text
        tutor?.lastName = family_tf.text
        tutor?.biography = biography_tv.text
        
        TutorService.getInstance().editTutor(tutor: tutor!).subscribe(onNext:{res in
                if res.success! == 1
                {
                    let sb = UIStoryboard(name: "TutorMain", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "TT_ContainerViewController")
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    
                }
            })
        

        }
    }
    
    func checkValidation() -> Bool
    {
        if name_tf.text?.characters.count == 0
        {
            showToast(message: "لطفا نام خود را وارد کنید")
            return false
        }
        else if family_tf.text?.characters.count == 0
        {
            showToast(message: "لطفا نام خانوادگی خود را وارد کنید")
            return false
        }
        else if family_tf.text?.characters.count == 0
        {
            showToast(message: "لطفا نام خانوادگی خود را وارد کنید")
            return false
        }
        else if biography_tv.text?.characters.count == 0
        {
            showToast(message: "لطفا بیوگرافی خود را وارد کنید")
            return false
        }
        else if biography_tv.text?.characters.count == 0
        {
            showToast(message: "لطفا بیوگرافی خود را وارد کنید")
            return false
        }
        else if tutor?.gender == nil{
            showToast(message: "لطفا  جنسیت خود را انتخاب کنید")
            return false
        }
        return true
    }
    
    
//    func showToast(message:String)
//    {
//        var style = ToastStyle()
//        style.messageColor = .white
//        style.backgroundColor = .red
//        style.cornerRadius = 15
//        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
//        self.view.makeToast(message,duration: 3.0, position: .bottom, style: style)
//    }
    
    
    func onUploadComplete(_ success: Bool, imageId: Int) {
        var uf = UploadedFile()
        uf.id = imageId
        tutor?.profileImage = uf
        
        spinner.isHidden = true
        spinner.stopAnimating()
        
    }
    
    
    @IBAction func onBioSelect(_ sender: Any) {
        
        if togglesBtn[bioBtn]!
        {
            togglesBtn[bioBtn] = false
            if let index = tutor?.topics.index(of: "Biology") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[bioBtn] = true
            tutor?.topics.append("Biology")
        }
        updateTopics()
    }
    
    @IBAction func onPhsSelect(_ sender: Any) {
        if togglesBtn[phsBtn]!
        {
            togglesBtn[phsBtn] = false
            if let index = tutor?.topics.index(of: "Physics") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[phsBtn] = true
            tutor?.topics.append("Physics")
            
        }
        updateTopics()
    }
    
    @IBAction func onChmSelect(_ sender: Any) {
        if togglesBtn[chmBtn]!
        {
            togglesBtn[chmBtn] = false
            if let index = tutor?.topics.index(of: "Chemistry") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[chmBtn] = true
            tutor?.topics.append("Chemistry")
            
        }
        updateTopics()
    }
    
    @IBAction func onMthSelect(_ sender: Any) {
        if togglesBtn[mthBtn]!
        {
            togglesBtn[mthBtn] = false
            if let index = tutor?.topics.index(of: "Math") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[mthBtn] = true
            tutor?.topics.append("Math")
            
        }
        updateTopics()
    }
    
    @IBAction func onEngSelect(_ sender: Any) {
        if togglesBtn[engBtn]!
        {
            togglesBtn[engBtn] = false
            if let index = tutor?.topics.index(of: "English") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[engBtn] = true
            tutor?.topics.append("English")
        }
        updateTopics()
    }
    
    @IBAction func onRlgSelect(_ sender: Any) {
        if togglesBtn[rlgBtn]!
        {
            togglesBtn[rlgBtn] = false
            if let index = tutor?.topics.index(of: "Religious") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[rlgBtn] = true
            tutor?.topics.append("Religious")
            
        }
        updateTopics()
    }
    
    @IBAction func onArbSelect(_ sender: Any) {
        if togglesBtn[arbBtn]!
        {
            togglesBtn[arbBtn] = false
            if let index = tutor?.topics.index(of: "Arabic") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[arbBtn] = true
            tutor?.topics.append("Arabic")
            
        }
        updateTopics()
    }
    
    @IBAction func onLtcSelect(_ sender: Any) {
        if togglesBtn[ltcBtn]!
        {
            togglesBtn[ltcBtn] = false
            if let index = tutor?.topics.index(of: "Literature") {
                tutor?.topics.remove(at: index)
            }
        }
        else
        {
            togglesBtn[ltcBtn] = true
            tutor?.topics.append("Literature")
            
        }
        updateTopics()
    }
    
    
    @IBAction func onSelectTopic(_ sender: DLRadioButton) {
        if let str =  sender.selected()?.titleLabel?.text
        {
            if str == "آقا"
            {
                tutor?.gender = Gender.Male
            }
            else if str == "خانم"
            {
                tutor?.gender = Gender.Female
            }
        }
    }
    
    
    func updateTopics()
    {
        for (k,v) in togglesBtn
        {
            if v
            {
                k.animate(.background(color: GlobalProperties.PrimaryColor))
                k.titleColor = .white
            }
            else
            {
                k.animate(.background(color: .white))
                k.titleColor = GlobalProperties.PrimaryColor
            }
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
