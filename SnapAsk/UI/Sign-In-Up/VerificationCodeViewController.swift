//
//  VerificationCodeViewController.swift
//  SnapAsk
//
//  Created by iman on 5/27/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import NVActivityIndicatorView
import EasyTipView
import NVActivityIndicatorView
import ToastSwiftFramework

class VerificationCodeViewController: UIViewController {

    @IBOutlet weak var resendTimeout_lbl: UILabel!
    
    @IBOutlet weak var phone_tf: TextField!
    
    @IBOutlet weak var verifiCode_tf: TextField!
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    
    
    @IBOutlet weak var resend_btn: RaisedButton!
    
    var phone = ""
    var timerCount = 90;
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(VerificationCodeViewController.Counting), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view.
        phone_tf.text = phone
        setTextFieldDesign(textField: phone_tf)
        setTextFieldDesign(textField: verifiCode_tf)
        
        resend_btn.layer.cornerRadius = 25
        
        resend_btn.isHidden = true
        resendTimeout_lbl.isHidden = false
    }
    
    @objc func Counting() {
        if timerCount > 0 {
            resendTimeout_lbl.text = " تلاش مجدد در \(timerCount) ثانیه دیگر"
            timerCount -= 1
        } else {
            resend_btn.isHidden = false
            resendTimeout_lbl.isHidden = true
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onChangeVerifiCode(_ sender: Any) {
        if verifiCode_tf.text?.characters.count == 6
        {
            UserService.getInstance().confirmVerificationCode(verifCode: verifiCode_tf.text!).subscribe(onNext:{res in
                if res.success! == 1
                {
                    try! AppDelegate.getStorage().setObject(res.message!, forKey: "token" , expiry: .never)
                    self.getUser();
                }
                else
                {
                    self.verifiCode_tf.text = ""
                    self.showToast(message: res.message!, bgColor: .red)
                }
            })
            
        }
        else
        {
            print(verifiCode_tf.text?.characters.count)
        }
    }
    
    @IBAction func onResendVerifi(_ sender: Any) {
        UserService.getInstance().resendVerificationCode(mobile: phone).subscribe(onNext:{res in
            if res.success! == 1{
                self.resend_btn.isHidden = true
                self.resendTimeout_lbl.isHidden = false
                self.timerCount = 90
                self.verifiCode_tf.text = ""
            }
            else
            {
                self.showToast(message: res.message!)
            }

        })
    }
    
    
//    func showToast(message:String)
//    {
//        var style = ToastStyle()
//        style.messageColor = .white
//        style.backgroundColor = .red
//        style.cornerRadius = 15
//        style.messageFont = UIFont(name:"IRANSansMobile" , size:11)!
//        self.view.makeToast(message,duration: 3.0, position: .bottom, style: style)
//    }
    
    func setTextFieldDesign(textField:TextField)
    {
        textField.textColor = Color.white
        textField.textAlignment = .center
        textField.dividerNormalColor = Color.white
        textField.dividerActiveColor = Color.white
        textField.font = UIFont(name:"IRANSansMobile" , size:18)!
        textField.placeholderNormalColor = Color.white
        textField.placeholderActiveColor = Color.white
        textField.placeholderActiveScale = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    func getUser()  {
        UserService.getInstance().getAuthenticatedUser(self).subscribe(onNext:{
            user in
            try? AppDelegate.getStorage().setObject(user, forKey: "user", expiry: .never)
            
            if let userType = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "userType")
            {
                if userType == "Student"
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ST_GradeSelViewController") as! ST_GradeSelViewController
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TT_RegDetailsViewController")
                    self.present(vc!, animated: true, completion: nil)
                }
            }
            
            
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
