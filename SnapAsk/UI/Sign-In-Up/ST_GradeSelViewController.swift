//
//  ST_GradeSelViewController.swift
//  SnapAsk
//
//  Created by iman on 5/28/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

class ST_GradeSelViewController: UIViewController {

    var student:Student?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StudentService.getInstance().getAuthenticatedStudetn(self).subscribe(onNext:{std in
            self.student = std
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onTouchGrades(_ sender: Any) {
       // student?.grade = Grade.
        let btn  = sender as! FlatButton
        switch (btn.titleLabel?.text)! {
        case "مقطع هفتم":
            self.student?.grade = Grade.Seventh
        case "مقطع هشتم":
            self.student?.grade = Grade.Eighth
        case "مقطع نهم":
            self.student?.grade = Grade.Ninth
        case "مقطع دهم":
            self.student?.grade = Grade.Tenth
        case "مقطع یازدهم":
            self.student?.grade = Grade.Eleventh
        case "مقطع دوازدهم":
            self.student?.grade = Grade.Twelfth
        default:
            return
        }
        saveStudent();
    }
    
    func saveStudent()
    {
        print(student?.grade)
        StudentService.getInstance().editStudent(student: self.student!).subscribe(onNext:{ res in
            print(res)
            if res.success! == 1{
                let sb = UIStoryboard(name: "StudentMain", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ST_ContainerViewController") as! ST_ContainerViewController
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
