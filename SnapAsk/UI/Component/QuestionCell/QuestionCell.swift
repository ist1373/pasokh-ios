//
//  QuestionCell.swift
//  SnapAsk
//
//  Created by iman on 6/10/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

class QuestionCell: UITableViewCell {

    @IBOutlet weak var card: Card!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    
    
    
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
