//
//  LocationTableView.swift
//  Booksum
//
//  Created by iman on 11/16/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit

protocol LocationTableViewDelegate:class{
    func onSelectCity(city:City)
}

protocol CurrentProvinceDelegate:class {
    func onChangeProvince(pro:Province)
}

class LocationTableView: UIView , UITableViewDelegate , UITableViewDataSource {

    weak var locationDelegate:LocationTableViewDelegate? = nil
    weak var currentProvinceDelegate:CurrentProvinceDelegate? = nil

    @IBOutlet weak var tabelView: UITableView!
    
    private var provinces : [Province] = []
    
    private var cities : [City] = []
    
    private var locationType:LocationType = LocationType.Province
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    func loadViewFromNib() {
        let view = Bundle.main.loadNibNamed("LocationTableView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view);
        self.tabelView.delegate = self;
        self.tabelView.dataSource = self;
        self.tabelView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch locationType {
        case .Province , .CurrentProvince:
            return provinces.count
        case .City:
            return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabelView.dequeueReusableCell(withIdentifier: "LocationCell",for: indexPath) as! LocationCell
        switch locationType {
        case .Province , .CurrentProvince:
            cell.locationTitle.text = provinces[indexPath.row].title
        case .City:
            cell.locationTitle.text = cities[indexPath.row].title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch locationType {
        case .Province:
            loadCities(provinceId: Int64(provinces[indexPath.row].id!))
        case .City:
            locationDelegate?.onSelectCity(city: cities[indexPath.row])
            self.parentViewController?.dismiss(animated: true, completion: nil)
        case .CurrentProvince:
            try? AppDelegate.getStorage().setObject(provinces[indexPath.row], forKey: "currentProvince", expiry: .never)
            currentProvinceDelegate?.onChangeProvince(pro: provinces[indexPath.row]);
            self.parentViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
//    func loadCurrentProvinces()
//    {
//        locationType = .CurrentProvince
//        LocationService.getInstance().getCurrentProvinces().subscribe(onNext:{provs in
//            self.provinces = provs
//            self.tabelView.reloadData()
//        })
//    }
    
    
    func loadProvinces(){
        locationType = LocationType.Province
        LocationService.getInstance().getProvinces().subscribe(onNext:{provs in
            self.provinces = provs
            self.tabelView.reloadData()
        })
    }
    func loadCities(provinceId:Int64)
    {
        locationType = LocationType.City
        LocationService.getInstance().getCities(provinceId: provinceId).subscribe(onNext:{cities in
            self.cities = cities;
            self.tabelView.reloadData();
        })
    }

}

enum LocationType{
    case City
    case Province
    case CurrentProvince
}





