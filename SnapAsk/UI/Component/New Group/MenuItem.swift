//
//  MenuItem.swift
//  SnapAsk
//
//  Created by iman on 5/30/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit

class MenuItem: UIView {

    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    func loadViewFromNib() {
        let view = Bundle.main.loadNibNamed("MenuItem", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view);
        
    }

    
}
