//
//  AttrItem.swift
//  SnapAsk
//
//  Created by iman on 6/18/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import UIKit
import Material


class AttrItem : UIView   {
    
    
    @IBOutlet weak var card: PulseView!
    @IBOutlet weak var attrIcon: UIImageView!
    @IBOutlet weak var attrLabel: UILabel!
    @IBOutlet weak var attrTitle: UILabel!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    func loadViewFromNib() {
        let view = Bundle.main.loadNibNamed("AttrItem", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        card.depthPreset = .depth1
        card.cornerRadiusPreset = .cornerRadius3
        view.backgroundColor = .clear
      
        
        
//        let cardGesture = UITapGestureRecognizer(target: self, action: #selector(AttrItem.onEdit))
//        card.addGestureRecognizer(cardGesture)
        
        
        self.addSubview(view);
        
    }
    

    
}
