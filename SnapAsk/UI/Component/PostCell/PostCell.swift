//
//  PostCell.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Material

class PostCell: UITableViewCell {
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDesc: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var card: Card!
    
}
