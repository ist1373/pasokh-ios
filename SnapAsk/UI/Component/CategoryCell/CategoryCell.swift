//
//  CategoryCell.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var card: Card!
    @IBOutlet weak var catImg: UIImageView!
    
    @IBOutlet weak var titleCard: Card!
    
    @IBOutlet weak var title: UILabel!
}

