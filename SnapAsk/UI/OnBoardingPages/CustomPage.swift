//
//  CustomPage.swift
//  Booksum
//
//  Created by iman on 10/21/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit
import SwiftyOnboard


class CustomPage: SwiftyOnboardPage {
    
  
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleOnBoard: UILabel!
    @IBOutlet weak var descriptionOnBoard: UILabel!
    
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
