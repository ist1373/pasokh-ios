//
//  IntroductionViewController.swift
//  SnapAsk
//
//  Created by iman on 6/17/18.
//  Copyright © 2018 iman. All rights reserved.
//
import UIKit
import SwiftyOnboard
import Alamofire
import ObjectMapper
import Material
import FontAwesomeKit

class IntroductionViewController: UIViewControllerIndicator,SwiftyOnboardDataSource,SwiftyOnboardDelegate,OverlayOnBoardPageDelegate {
    
    var swiftyOnboard :CustomSwiftyOnboard? = nil
    var currentProvinces:[Province] = []
    var loadCurrentProvinces = true;
    var currentTableView :UITableView?;
    var currentPage = 0;
    
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: CustomSwiftyOnboard) -> Int {
        return 3
    }
    
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool) {
        if currentPage == 2
        {
            let sb = UIStoryboard(name: "SignInUp", bundle: nil)
            let signInVC = sb.instantiateViewController(withIdentifier: "LoginViewController")
            self.present(signInVC, animated: true, completion: nil)
        }
        else
        {
            var next = currentPage+1
            swiftyOnboard?.goToPage(index: next, animated: true)
        }
        
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: CustomSwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = CustomPage.instanceFromNib() as? CustomPage
        
        switch index {
        case 0:
            view?.image.image = UIImage(named:"student_big_active")
            view?.titleOnBoard.text = "عنوان ۱"
            view?.descriptionOnBoard.text = "توضیحات عنوان ۱"
        case 1:
            
            view?.image.image = UIImage(named:"teacher_big_active")
            view?.titleOnBoard.text =  "عنوان ۲"
            view?.descriptionOnBoard.text = "توضیحات عنوان ۲"
        case 2:
            
            view?.image.image = UIImage(named:"student_big_active")
            view?.titleOnBoard.text = NSLocalizedString("OnBoardTitlePage3", comment: "")
            view?.descriptionOnBoard.text = NSLocalizedString("OnBoardDescPage3", comment: "")
        default:
            view?.image.image = UIImage(named:"book_shelf")
        }
        return view
   
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if let pro = try? AppDelegate.getStorage().object(ofType: Province.self, forKey: "currentProvince")
        {
            print("go to home")
            loadNavigationDrawer()
        }
        else
        {
            swiftyOnboard = CustomSwiftyOnboard(frame: view.frame)
            view.addSubview(swiftyOnboard!)
            swiftyOnboard?.dataSource = self
            swiftyOnboard?.delegate = self
        }
    }
    
    
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: CustomSwiftyOnboard) -> SwiftyOnboardOverlay?{
        print("overlaaaay")
        let view = OverlayOnBoardPage.instanceFromNib() as? OverlayOnBoardPage
        let icon = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15))
        view?.nextPage.image = icon?.tint(with: UIColor.white)
        view?.pageController.currentPage = 0;
        view?.pageController.numberOfPages = 3
        view?.delegate = self
        return view
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: CustomSwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let overlayPage = overlay as! OverlayOnBoardPage
        let currentPage = round(position)
        self.currentPage = Int(currentPage);
        overlayPage.pageController.currentPage = Int(currentPage)
        if currentPage == 3
        {
            overlayPage.nextPage.isHidden = true;
        }
        else
        {
            overlayPage.nextPage.isHidden = false;
        }
        
        //            overlay.skipButton.setTitle("", for: .normal)
        //            overlay.continueButton.setTitle("ادامه", for: .normal)
        //            overlay.continueButton.titleLabel?.font = UIFont(name:"iranSans" , size:15)
        //            overlay.continueButton.titleLabel?.textColor = UIColor.white
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: CustomSwiftyOnboard, atIndex index: Int) -> UIColor?{
        return UIColor(rgb:0x4D2E7F)
//        return GlobalProperties.PrimaryDarkColor
    }
    
    
    
    

    
    func loadNavigationDrawer() {
        let storyboard: UIStoryboard = UIStoryboard(name: "BookMarketStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavigationDrawerController") as! NavigationDrawerController
        self.present(vc, animated: false, completion: nil)
        
    }
    

    
}
