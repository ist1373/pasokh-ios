//
//  OverlayPage.swift
//  Booksum
//
//  Created by iman on 10/22/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import UIKit
import SwiftyOnboard
import Material


public protocol OverlayOnBoardPageDelegate: class {
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool)
}

public extension OverlayOnBoardPageDelegate {
    func overlayPage(_ overlayPage: SwiftyOnboardOverlay, didSelectNextPage:Bool) {}
}



public class OverlayOnBoardPage: SwiftyOnboardOverlay {
    
    open weak var delegate: OverlayOnBoardPageDelegate?
    
    @IBOutlet weak var pageController: UIPageControl!
    
    @IBAction func onNextPageSelect(_ sender: Any) {
        delegate?.overlayPage(self, didSelectNextPage: true)
        print("<#T##items: Any...##Any#>")
    }
    
    @IBOutlet weak var nextPage: IconButton!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "OverlayOnBoardPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
