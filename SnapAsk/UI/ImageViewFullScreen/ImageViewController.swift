//
//  ImageViewController.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import Material
import FontAwesomeKit
import Kingfisher


class ImageViewController: UIViewController ,UIScrollViewDelegate{

    var imgId :Int?
    
    @IBOutlet weak var toolbar: Toolbar!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var image: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initToolbar()
        
        if let id = imgId
        {
            let url = URL(string: GlobalProperties.getLocalThumbnailImageURL(Int64((id))))!
            image.kf.setImage(with: url)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return image
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func initToolbar()  {
        UIApplication.shared.statusBarView?.backgroundColor = GlobalProperties.PrimaryColor
        
        toolbar.depthPreset = .depth2
        toolbar.backgroundColor = GlobalProperties.PrimaryColor
        toolbar.titleLabel.font = GlobalProperties.getIranSansFont(size: 14)
        toolbar.titleLabel.text = "تصویر سوال";
        toolbar.titleLabel.textColor = UIColor.white
        toolbar.layer.zPosition = 10;
        
        let backImg = FAKIonIcons.chevronRightIcon(withSize: 15).image(with: CGSize(width: 15, height: 15)).tint(with: .white)
        
        let backBtn = IconButton(image: backImg?.tint(with: .white), tintColor: .white)
        backBtn.pulseColor = .white
        backBtn.addTarget(self, action: #selector(AboutUsViewController.back), for: .touchUpInside)
        toolbar.rightViews = [backBtn]
        
    }
    
    
    
    @objc func back()
    {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    


}
