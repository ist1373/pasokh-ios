//
//  City.swift
//  Booksum
//
//  Created by iman on 10/23/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import ObjectMapper

class City: Mappable,Codable {
    
    var id:Int64?;
    
    var title:String?;
    

    var province:Province?;
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        title         <- map["title"]
        province      <- map["province"]
    }

}
