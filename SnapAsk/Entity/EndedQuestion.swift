//
//  EndedQuestion.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class EndedQuestion:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var credit:Int?
    
    var bid:Float?
    
    var responder:User?;
    
    var creationDate:Double?
    
    var description:String?
    
    var question:Question?
    
    var endedQuestionState: EndedQuestionState?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        responder      <- map["responder"]
        creationDate    <- map["creationDate"]
        description    <- map["description"]
        credit    <- map["credit"]
        bid    <- map["bid"]
        question    <- map["question"]
        endedQuestionState    <- map["endedQuestionState"]
    }
}

enum EndedQuestionState :String , Codable {
    case Completed
    case Archived

    var description:String{
        switch self {
        case .Completed:
            return "تسویه نشده"
        case .Archived:
            return "تسویه شده"
       
            
        }
        
    }
}

