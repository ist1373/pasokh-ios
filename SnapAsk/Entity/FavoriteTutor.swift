//
//  FavoriteTutor.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class FavoriteTutor:Mappable {
    
    var id:Int64?;
    
    var questioner:User?;
    
    var creationDate:Double?
    
    var tutor:User?
    
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        questioner         <- map["questioner"]
        tutor      <- map["tutor"]
        creationDate    <- map["creationDate"]
       
    }
}

