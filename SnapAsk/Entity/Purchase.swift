//
//  Purchase.swift
//  SnapAsk
//
//  Created by iman on 6/19/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Purchase:Mappable {
    var id:Int64?;
    
    var title:String?;
    
    var description:String?
    
    var price:Double?
    
    var image:UploadedFile?
    
    var province:Province?;
    
    var isShow:Bool?
    
    var creationDate:Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        title         <- map["title"]
        description         <- map["description"]
        price         <- map["price"]
        image         <- map["image"]
        province         <- map["province"]
        isShow      <- map["isShow"]
        creationDate      <- map["creationDate"]
    }
}
