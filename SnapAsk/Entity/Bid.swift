//
//  Bid.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Bid:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var bidAmount:Float?;
    
    var creationDate:Double?
    
    var question:Question?
    
    var tutor:Tutor?
    
    var bidState:BidState?
    
    var acceptDate:Double?
    
    var failedDate:Double?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        tutor      <- map["tutor"]
        creationDate    <- map["creationDate"]
        bidAmount    <- map["bidAmount"]
        question    <- map["question"]
        tutor    <- map["tutor"]
        bidState    <- map["bidState"]
        acceptDate    <- map["acceptDate"]
        failedDate    <- map["failedDate"]
    }
}


enum BidState :String , Codable {
    case Pending
    case Accept
    case Failed
    
    var description:String {
        switch self {
        case .Pending:
            return "در حال بررسی"
        case .Accept:
            return "پذیرفته شده"
        case .Failed:
            return "رد شده"
        }
        
    }
}
