//
//  ComplaintReport.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation

import Foundation
import ObjectMapper

class ComplaintReport:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var tutor:User?;
    
    var creationDate:Double?
    
    var description:String?
    
    var question:Question?
    
    var reportTopic:ReportTopic?
    
    var user:User?
    
    var reportState:ReportState?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        tutor      <- map["tutor"]
        creationDate    <- map["creationDate"]
        description    <- map["description"]
        question    <- map["question"]
        user    <- map["user"]
        reportTopic    <- map["reportTopic"]
        reportState    <- map["reportState"]
    }
}

enum ReportTopic :String , Codable {
    case NotAnswered
    case WrongAnswered
    case LateAnswered
    case Other
    
    var description:String{
        switch self {
        case .NotAnswered:
            return "پاسخ داده نشده"
        case .WrongAnswered:
            return "پاسخ اشتباه است"
        case .LateAnswered:
            return "دیر پاسخ داده شده"
        case .Other:
            return "دیگر"
            
        }
        
    }
}

enum ReportState :String , Codable {
    case Seen
    case NotSeen

    
    var description:String{
        switch self {
        case .Seen:
            return "دیده شده"
        case .NotSeen:
            return "دیده نشده"

            
        }
        
    }
}
