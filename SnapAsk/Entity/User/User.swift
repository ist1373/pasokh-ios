//
//  User.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper


class User : Mappable , Codable  {
    required init?(map: Map) {

    }
    
    init() {
        
    }
    
    var id: Int?;
    
    var uid: String?;
    
    var username: String?;
    
    var email: String?;
    
    var gender:Gender?;
    
    
    var mobileNumber: String?;
    
    var userState:UserState?
    
    
    var password: String?;
    
    var firstName: String?;
    
    var lastName: String?;
    
    
//    var enabled: Bool?;
    
    
//    var verified: Bool?;
    
    
    var isPresent: Bool?;
    
    
    var playerId: String?;
    
    var profileImage:UploadedFile?
    
    
    var lastPasswordResetDate: String?;
    
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid    <- map["uid"]
        username    <- map["username"]
        email    <- map["email"]
        mobileNumber    <- map["mobileNumber"]
        password    <- map["password"]
        firstName    <- map["firstName"]
        lastName    <- map["lastName"]
       
        isPresent    <- map["isPresent"]
        playerId    <- map["playerId"]
        profileImage <- map["profileImage"]
        lastPasswordResetDate    <- map["lastPasswordResetDate"]
    }
}



enum UserState :String , Codable {
    case Pending
    case Verified
    case Qualified
    case Rejected
    case Banned
    case NeedToEdit
    case Deleted
    
    var description:String{
        switch self {
        case .Pending:
            return "در حال بررسی"
        case .Verified:
            return "تایید شده"
        case .Qualified:
            return "صحت سنجی شده"
        case .Rejected:
            return "رد شده"
        case .Banned:
            return "ممنوع شده"
        case .NeedToEdit:
            return "نیاز به تغییر"
        case .Deleted:
            return "حذف شده"
        }
        
    }
}

enum Gender :String , Codable {
    case Male
    case Female
    var description:String{
        switch self {
        
        case .Male:
            return "آقا"
        case .Female:
            return "خانم"
        }
        
    }
}
