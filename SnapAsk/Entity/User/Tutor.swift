//
//  Tutor.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Tutor : User  {
    
    
    var deviceId: String?;
    
    var systemOS: String?;
    
    var systemVersion: String?;
    
    var systemDevice: String?;
    
    var shabaCode: String?;
    
    var biography: String?;
    
    var postalCode: String?;
    
    var nationalCode: String?;
    
    var address:String?
    
    var city: City?;
    
    var nationalCardImage: UploadedFile?;
    
    var topics:[String] = [];
    
    

    
    override func mapping(map: Map) {
        
        super.mapping(map: map)
        deviceId    <- map["deviceId"]
        systemOS    <- map["systemOS"]
        systemVersion    <- map["systemVersion"]
        systemDevice    <- map["systemDevice"]
        shabaCode    <- map["shabaCode"]
        biography    <- map["biography"]
        nationalCode    <- map["nationalCode"]
        city    <- map["city"]
        address    <- map["address"]
        postalCode    <- map["postalCode"]
        topics    <- map["topics"]
        nationalCardImage    <- map["nationalCardImage"]
    }
}















