//
//  Tutor.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Student : User  {
    
    var deviceId: String?;
    
    var systemOS: String?;
    
    var systemVersion: String?;
    
    var systemDevice: String?;
    
    var inviteToken: String?;
    
    var city: City?;
    
    var grade: Grade?;
    
  
    var field:Field?;
    
    var school:String?;
    
    var average: Float?;
    
    var credit:Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        deviceId    <- map["deviceId"]
        systemOS    <- map["systemOS"]
        systemVersion    <- map["systemVersion"]
        systemDevice    <- map["systemDevice"]
        inviteToken    <- map["inviteToken"]
        city    <- map["city"]
        grade    <- map["grade"]
        field    <- map["field"]
        school    <- map["school"]
        average    <- map["average"]
        credit    <- map["credit"]
    }
}

enum Field : String , Codable{
    case Math
    case NaturalScience
    case Humanities
    case Technical
    var description: String {
        switch self {
        case .Math:
            return "ریاضی فیزیک"
        case .NaturalScience:
            return "علوم تجربی"
        case .Humanities:
            return "علوم انسانی"
        case .Technical:
            return "فنی حرفه ای"
        }
    }
}














