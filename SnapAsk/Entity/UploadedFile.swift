//
//  ChatMessage.swift
//  SnapAsk
//
//  Created by iman on 5/14/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper
class UploadedFile : Mappable , Codable {
    
    var id: Int?;
    
    var name: String?;
    
    var size: Double?;
    
    var originalPath: String?;
    
    var thumbnailPath: String?;
    
    var description: String?;
    
    var creationDate: Double?;
    
    var fileType: String?;
    
    var createdBy: User?;
    
    var isTransferred: Bool?;
    
    
    var accessType: AccessType?;
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
        size    <- map["size"]
        originalPath    <- map["originalPath"]
        thumbnailPath    <- map["thumbnailPath"]
        description    <- map["description"]
        creationDate    <- map["creationDate"]
        fileType    <- map["fileType"]
        createdBy    <- map["createdBy"]
        isTransferred    <- map["isTransferred"]
        accessType    <- map["accessType"]
    }
    
}

enum AccessType: String , Codable {
    case Public
    case Private
    case AccessOnPay
    case Forbidden
}



