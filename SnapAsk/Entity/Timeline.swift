//
//  Timeline.swift
//  SnapAsk
//
//  Created by iman on 6/19/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Timeline: Mappable {
    
    var id:Int64?;
    
    
    var questionTopic:QuestionTopic?
    
    var eventType:EventType?
    
    var star:Int?;
    
    var responder:User?
    
    var creationDate:Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id    <- map["id"]
        questionTopic         <- map["questionTopic"]
        eventType         <- map["eventType"]
        star         <- map["star"]
        responder         <- map["responder"]
        creationDate         <- map["creationDate"]
    }
}


enum EventType :String , Codable {
    case NewQuestion
    case NewAnswer
    case TakenStar
}

