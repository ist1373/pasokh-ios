//
//  ValidationError.swift
//  Booksum
//
//  Created by iman on 11/10/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation

struct ValidationError: Error {
    
    public let message: String
    
    public init(message m: String) {
        message = m
    }
}
