//
//  ChatMessage.swift
//  SnapAsk
//
//  Created by iman on 5/14/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper
class ChatMessage : Mappable  {

    var id:Int64?
    var uid:String?
    var authorUser:User?
    var recipientUser:User?
    var timeSent:Int64?
    var contents:String?
    var mediaFile:UploadedFile?
    var chatRoom:ChatRoom?
    var messageType:MessageType?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id    <- map["id"]
        uid    <- map["uid"]
        authorUser    <- map["authorUser"]
        recipientUser    <- map["recipientUser"]
        timeSent    <- map["timeSent"]
        contents    <- map["contents"]
        mediaFile    <- map["mediaFile"]
        chatRoom    <- map["chatRoom"]
        messageType    <- map["messageType"]
    }
}

enum MessageType :String , Codable {
    case SEND_TEXT
    case SEND_IMAGE
}
