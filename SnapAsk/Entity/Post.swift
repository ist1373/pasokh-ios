//
//  Post.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Post:Mappable {
    
    var id:Int64?;
    
    var creationDate:Double?
    
    var publicationDate:Double?


    var title:String?;
    
    var content:String?
    
    
    var coverImage:UploadedFile?
    
    var category:Category?;
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        creationDate         <- map["creationDate"]
        publicationDate      <- map["publicationDate"]
        title    <- map["title"]
        content    <- map["content"]
        coverImage    <- map["coverImage"]
        category    <- map["category"]
        
    }
}
