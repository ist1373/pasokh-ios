//
//  BasicResponse.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class BasicResponse : Mappable{

    var code: Int?;
    
    var message: String?;
    
    var success: Int?;
    
    var data:String?
    
    

    init() {
        
    }
    init(success:Int) {
        self.success = success;
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    

    
    func mapping(map: Map) {
        code    <- map["code"]
        message    <- map["message"]
        success    <- map["success"]
        data    <- map["data"]
    }
}
