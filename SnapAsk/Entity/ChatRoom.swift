//
//  ChatRoom.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//
import Foundation
import ObjectMapper

class ChatRoom: Mappable,Codable {
    
    var id:Int64?
    
    var uid:String?
    
    var questioner:User?
    
    var responder:User?
    
    var province:Province?;
    
    var creationDate:Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        questioner         <- map["questioner"]
        responder         <- map["responder"]
        province         <- map["province"]
        creationDate         <- map["creationDate"]
    }
    
}

