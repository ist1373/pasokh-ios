//
//  QuestionReport.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//


import Foundation
import ObjectMapper

class QuestionReport:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var tutor:User?;
    
    var creationDate:Double?
    
    var description:String?
    
    var question:Question?

    var reportType:ReportType?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        tutor      <- map["tutor"]
        creationDate    <- map["creationDate"]
        description    <- map["description"]
        question    <- map["question"]
        reportType    <- map["reportType"]
    }
}

enum ReportType :String , Codable {
    case ManyQuestion
    case NotQuestion
    case IrrevelentQuestion
    case Other

    var description:String{
        switch self {
            case .ManyQuestion:
                return "تعداد بیش از یک سوال پرسیده شده"
            case .NotQuestion:
                return "محتوا سوال نیست"
            case .IrrevelentQuestion:
                return "سوال با موضوع ارتباط ندارد"
            case .Other:
                return "دیگر"
            
        }
           
    }
}

