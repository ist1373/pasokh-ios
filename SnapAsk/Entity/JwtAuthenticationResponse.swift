//
//  JwtAuthenticationResponse.swift
//  Booksum
//
//  Created by iman on 11/10/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import ObjectMapper
class JwtAuthenticationResponse: Mappable , Decodable ,Encodable {

    var token:String?
    
    var statusCode:Int?
    
    var authorities:[Authority]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        token         <- map["token"]
        statusCode         <- map["statusCode"]
        authorities         <- map["authorities"]

    }
    
  
}
