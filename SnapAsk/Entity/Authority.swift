//
//  Authority.swift
//  SnapAsk
//
//  Created by iman on 6/29/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Authority: Mappable,Codable{
    var id:Int64?;
    
    var authority:String?;
    

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        authority      <- map["authority"]

    }
}
