//
//  InviteToken.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class InviteToken:Mappable {
    
    var id:Int64?;
    
    var token:String?;
    
    var creationDate:Double?
    
    var caller:User?
    
    var invited:User?;
    
    
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        token         <- map["token"]
        creationDate      <- map["creationDate"]
        caller    <- map["caller"]
        invited    <- map["invited"]
        
    }
}

