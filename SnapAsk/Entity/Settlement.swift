//
//  Settlement.swift
//  SnapAsk
//
//  Created by iman on 7/25/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation

import ObjectMapper

class Settlement:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var amount:Int64?
    
    var refId:String?
    
    var creationDate:Double?
    
    var creditNumber:String?
    
    var receiver:User?
    
    var settlementState:SettlementState?
    
    
    var settlementType:SettlementType?
    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid    <- map["uid"]
        creationDate         <- map["creationDate"]
        amount      <- map["amount"]
        refId    <- map["refId"]
        creditNumber    <- map["creditNumber"]
        receiver    <- map["receiver"]
        settlementState    <- map["settlementState"]
        settlementType    <- map["settlementType"]
    }
}

enum SettlementState :String , Codable {
    case Done
    case Cancel
    case Error
    case Pending
    
    var description: String {
        switch self {
        case .Done:
            return "انجام شده"
        case .Cancel:
            return "کنسل شده"
        case .Error:
            return "رخ دادن خطا"
        case .Pending:
            return "در حال انجام"
        }
    }
}
enum SettlementType :String , Codable {
    case Question
}

