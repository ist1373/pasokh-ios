//
//  ResponseError.swift
//  SnapAsk
//
//  Created by iman on 6/25/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation

struct ResponseError: Error {
    
    public var message: String
    public var code: Int?
    public init(message m: String) {
        message = m
    }
    public init(message m: String , code c:Int) {
        message = m
        code = c
    }
    
}
