//
//  Question.swift
//  SnapAsk
//
//  Created by iman on 6/11/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Question:Mappable {
    
    var id:Int64?;
    
    var uid:String?;
    
    var questioner:User?;
    
    var responder:User?;
    
    var creationDate:Double?
    
    var endDate:Double?
    
    var rejectedDate:Double?
    
    var description:String?
    
    var image:UploadedFile?
    
    var rate:Int?
    
    var grade:Grade?
    
    var questionState:QuestionState?
    
    var questionTopic:QuestionTopic?
    
    var credit:Int?
    
    var chatRoom:ChatRoom?
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        uid         <- map["uid"]
        questioner      <- map["questioner"]
        responder    <- map["responder"]
        creationDate    <- map["creationDate"]
        endDate    <- map["endDate"]
        rejectedDate    <- map["rejectedDate"]
        description    <- map["description"]
        image    <- map["image"]
        rate    <- map["rate"]
        grade    <- map["grade"]
        questionState    <- map["questionState"]
        questionTopic    <- map["questionTopic"]
        credit    <- map["credit"]
        chatRoom    <- map["chatRoom"]
    }
}

enum Grade :String , Codable {
    case Seventh
    case Eighth
    case Ninth
    case Tenth
    case Eleventh
    case Twelfth
    var description:String{
        switch self {
            
        case .Seventh:
            return "هفتم"
        case .Eighth:
            return "هشتم"
        case .Ninth:
            return "نهم"
        case .Tenth:
            return "دهم"
        case .Eleventh:
            return "یازدهم"
        case .Twelfth:
            return "دوازدهم"
        }
    }
}

enum QuestionState :String , Codable {
    case Pending
    case Accepted
    case Finished
    case Rejected
    case RejectedByTutor
    case WaitingForTutor
    var description: String {
        switch self {
        case .Pending:
            return "منتظر پاسخ دهنده"
        case .Accepted:
            return "پذیرفته شده"
        case .Finished:
            return "پایان یافته"
        case .Rejected:
            return "رد شده"
        case .RejectedByTutor:
            return "رد شده توسط پاسخ دهنده مستقیم"
        case .WaitingForTutor:
            return "منتظر پاسخ دهنده مستقیم"
        }
    }
}


enum QuestionTopic :String , Codable {
    case Physics
    case Chemistry
    case Biology
    case Literature
    case Math
    case Arabic
    case Religious
    case English
    
    var description: String {
        switch self {
        case .Physics:
            return "فیزیک"
        case .Chemistry:
            return "شیمی"
        case .Biology:
            return "زیست"
        case .Literature:
            return "ادبیات"
        case .Math:
            return "ریاضی"
        case .Arabic:
            return "عربی"
        case .Religious:
            return "دینی"
        case .English:
            return "زبان"
        }
    }
}




