//
//  Province.swift
//  Booksum
//
//  Created by iman on 10/23/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import ObjectMapper

class Province: Mappable, Codable {
    var id:Int?;
    
    var title:String?;
    
    var cityList:[City]?;
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        title         <- map["title"]
        cityList      <- map["cityList"]
    }
}
