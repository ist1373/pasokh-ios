//
//  TutorDto.swift
//  SnapAsk
//
//  Created by iman on 5/23/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class TutorDto:Mappable {
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        username    <- map["username"]
        email    <- map["email"]
        mobileNumber    <- map["mobileNumber"]
        password    <- map["password"]
        playerId    <- map["playerId"]
        deviceId    <- map["deviceId"]
        systemOS    <- map["systemOS"]
        systemVersion    <- map["systemVersion"]
        systemDevice    <- map["systemDevice"]
        shabaCode    <- map["shabaCode"]
        nationalCode    <- map["nationalCode"]
        profileImage    <- map["profileImage"]
    }
    
    
    
    var id: Int?;
    var username: String?;
    var password: String?;
    var email: String?;
    var mobileNumber: String?;
    var playerId: String?;
    var deviceId: String?;
    var systemOS: String?;
    var systemVersion: String?;
    var systemDevice: String?;
    var shabaCode: String?;
    var nationalCode: String?;
    var profileImage: UploadedFile?;
    
}
