//
//  Category.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import ObjectMapper

class Category:Mappable {
    
    var id:Int?;
    
    var title:String?;
    
    var coverImage:UploadedFile?
    
    var childCount:Int?
    
    var categoryIndex:Int?;
    

    
    
    init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        title         <- map["title"]
        coverImage      <- map["coverImage"]
        childCount    <- map["childCount"]
        categoryIndex    <- map["categoryIndex"]
        
    }
}


