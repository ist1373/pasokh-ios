//
//  UserService.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper



class UserService {
    private static let userService = UserService()
    
    public static func getInstance() ->UserService{
        return userService;
    }
    
    
    func userLogin(empho:String,password:String) -> Observable<JwtAuthenticationResponse> {
        let parameters: Parameters = [
            "username": empho,
            "password": password
        ]
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1 + "user/auth",parameters:parameters,encoding: JSONEncoding.default)
           
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            
            .debug()
            .retry(5)
            .map { (response, value) in
                if response.statusCode >= 200 && response.statusCode < 300
                {
                    let res = Mapper<JwtAuthenticationResponse>().map(JSONObject: value)
                    res?.statusCode = response.statusCode
                    return res!
                }
                else
                {
                    print("response:::")
                    print(response)
                    return JwtAuthenticationResponse(JSON: ["statusCode":response.statusCode])!
                }
            }.catchError { error in
                print("error::::")
                print(error)
                return Observable.error(error)
            }

            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getAuthenticatedUser(_ viewController:UIViewController) -> Observable<User> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "user",headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
      
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<User>().map(JSONObject: value)
                    //try? AppDelegate.getStorage().setObject(res, forKey: "user" , expiry: .never)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func confirmVerificationCode(verifCode:String) -> Observable<BasicResponse> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "user/registrationConfirm?token=" + verifCode)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                
                
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func resendVerificationCode(mobile:String) -> Observable<BasicResponse> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "user/resendRegistrationToken?mobile=" + mobile)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    
    
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
    
    
}
