//
//  FileService.swift
//  Booksum
//
//  Created by iman on 11/16/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import Kingfisher
import NVActivityIndicatorView
import ObjectMapper

protocol UploadCompleteDelegate:class {
    func onUploadComplete(_ success:Bool,imageId:Int);
}

class FileService{
    
    weak var delegate :UploadCompleteDelegate?
    static let fileService = FileService()
    
    public static func getInstance() ->FileService{
        
        return fileService;
    }
    
    public func uploadImage(_ image:UIImage,accessType:AccessType)
    {
    
        let imgData = UIImageJPEGRepresentation(UIImage.resizeImage(image: image, newHeight: 800), 0.5)!
        let parameters = ["accessType": accessType.rawValue]
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:GlobalProperties.BaseURLV1 + "file/upload", headers:setAuthorizationHeader())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    if let json = response.result.value
                    {
                        
                        let res = Mapper<BasicResponse>().map(JSONObject: json)
                        if let mess = res?.message{
                            print("r:")
                            
                            self.delegate?.onUploadComplete((res?.success!)! == 1 ? true : false,imageId:Int(mess)!)
                        }
                        else
                        {
                            self.delegate?.onUploadComplete(false,imageId:0)
                        }
                    }
                    else
                    {
                        self.delegate?.onUploadComplete(false,imageId: 0)
                    }
                    
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
                self.delegate?.onUploadComplete(false, imageId: 0)
                
            }
        }
    }
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
}
