//
//  PostService.swift
//  SnapAsk
//
//  Created by iman on 7/29/18.
//  Copyright © 2018 iman. All rights reserved.
//


import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class PostService {
    
    private static let postService = PostService()
    
    public static func getInstance() -> PostService{
        return postService;
    }
    
    func getPostsByCategory(catId:Int) -> Observable<[Post]>{
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "post?catId=\(catId)" ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .retry(5)
            .map { (response, value) in
                
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<Post>().mapArray(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getPostById(id:Int) -> Observable<Post>{
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "post/\(id)" ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .retry(5)
            .map { (response, value) in
                
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<Post>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
