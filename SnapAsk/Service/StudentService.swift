//
//  StudentService.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class StudentService {
    private static let studentService = StudentService()
    
    public static func getInstance() ->StudentService{
        return studentService;
    }
    
    func registerStudent(studentDto:StudentDto) -> Observable<BasicResponse> {

        let jsonStudent = ConvertTools.convertToDictionary(text: studentDto.toJSONString()!)
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "user/student",parameters:jsonStudent,encoding: JSONEncoding.default)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print("errorrrrrrr")
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<BasicResponse>().map(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getAuthenticatedStudetn(_ viewController:UIViewController) -> Observable<Student> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "user",headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
           
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<Student>().map(JSONObject: value)
                    //try? AppDelegate.getStorage().setObject(res, forKey: "user" , expiry: .never)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    func editStudent(student:Student) -> Observable<BasicResponse> {
        
        let jsonStudent = ConvertTools.convertToDictionary(text: student.toJSONString()!)
        return RxAlamofire
            .requestJSON(.put,GlobalProperties.BaseURLV1  + "user/student/" + String(describing: student.id!) ,parameters:jsonStudent,encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<BasicResponse>().map(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
