//
//  SettlementService.swift
//  SnapAsk
//
//  Created by iman on 7/25/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class SettlementService {

    
    static let settlementService = SettlementService()
    
    public static func getInstance() ->SettlementService{
        
        return settlementService;
    }
    
    
    
    func getAvailableMoney() -> Observable<BasicResponse>? {
        
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "settlement/cash?userId=\((u.id)!)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    func addSettlement() -> Observable<BasicResponse> {
        

        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "settlement",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getSettlementsByTutor(page:Int) -> Observable<[Settlement]>? {
        
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "settlement/tutor/\(u.id!)?page=\(page)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Settlement>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
