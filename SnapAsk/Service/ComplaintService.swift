//
//  ComplaintService.swift
//  SnapAsk
//
//  Created by iman on 7/30/18.
//  Copyright © 2018 iman. All rights reserved.
//



import Foundation
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class ComplaintService {
    private static let complaintService = ComplaintService()
    
    public static func getInstance() ->ComplaintService{
        return complaintService;
    }
    
    func addComplaintReport(complaintReport:ComplaintReport) -> Observable<BasicResponse> {
        
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "complaintReport?quesId=\((complaintReport.question?.id)!)&desc=\(complaintReport.description!)&topic=\(complaintReport.reportTopic!)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}

