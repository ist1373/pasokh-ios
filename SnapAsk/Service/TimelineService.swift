//
//  TimelineService.swift
//  SnapAsk
//
//  Created by iman on 6/19/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper

class TimelineService {
    private static let timelineService = TimelineService()
    
    public static func getInstance() ->TimelineService{
        
        return timelineService;
    }
    
    
    func getTimeline(page:Int) -> Observable<[Timeline]>{
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "timeline?page=\(page)" ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<Timeline>().mapArray(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
