//
//  EndedQuestion.swift
//  SnapAsk
//
//  Created by iman on 7/23/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class EndedQuestionService {
    private static let endedQuestionService = EndedQuestionService()
    
    public static func getInstance() -> EndedQuestionService{
        return endedQuestionService;
    }
    
    
    func getEndedQuestionByResponder(page:Int) -> Observable<[EndedQuestion]>? {
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            print(u.id!)
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "endedQuestion/tutor/" + String(u.id!) + "?page=\(page)",headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<EndedQuestion>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
