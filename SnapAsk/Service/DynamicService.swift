//
//  DynamicService.swift
//  SnapAsk
//
//  Created by iman on 8/3/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class DynamicService {
    private static let dynamicContent = DynamicService()
    
    public static func getInstance() -> DynamicService{
        return dynamicContent;
    }

    
    func getDynamicContent(keyContent:String) -> Observable<BasicResponse>{
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "dynamicContent?keyContent=\(keyContent)" ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .retry(5)
            .map { (response, value) in
                
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
}
