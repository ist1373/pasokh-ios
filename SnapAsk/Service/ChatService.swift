//
//  ChatService.swift
//  SnapAsk
//
//  Created by iman on 7/2/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class ChatService{
    
    static let chatService = ChatService()
    
    public static func getInstance() ->ChatService{
        
        return chatService;
    }
    
    func getExistingChatMessages(roomUId:String,page:Int) -> Observable<[ChatMessage]>{
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "chat/room/\(roomUId)?page=\(page)" ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<ChatMessage>().mapArray(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func addChatMessage(roomUId:String,chatMessage:ChatMessage) -> Observable<BasicResponse>{
        let jsonMess = ConvertTools.convertToDictionary(text: chatMessage.toJSONString()!)
        print(jsonMess)
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "chat/room/\(roomUId)",parameters:jsonMess,encoding: JSONEncoding.default ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<BasicResponse>().map(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    

    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
    
    
    
}
