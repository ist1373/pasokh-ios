//
//  TransactionService.swift
//  SnapAsk
//
//  Created by iman on 6/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class TransactionServic{
    private static let transactionService = TransactionServic()
    
    public static func getInstance() ->TransactionServic{
        
        return transactionService;
    }
    
    func paymentRequest(pId:Int64,callbackUrl:String) -> Observable<BasicResponse> {
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1 + "transaction/paymentRequestZarin?pId=\(pId)&callbackUrl=\(callbackUrl)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            
            .debug()
            .retry(5)
            .map { (response, value) in
                if response.statusCode >= 200 && response.statusCode < 300
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    print(res)
                    return res!
                }
                else
                {
                    print("response:::")
                    print(response)
                    return BasicResponse(success:0)
                }
            }.catchError { error in
                print("error::::")
                print(error)
                return Observable.error(error)
            }
            
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func zarinContentVerify(userId:Int,pId:Int64 , authority:String) -> Observable<BasicResponse> {
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1 + "transaction/verifyPaymentZarin?userId=\(userId)&pId=\(pId)&authority=\(authority)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            
            .debug()
            .retry(5)
            .map { (response, value) in
                if response.statusCode >= 200 && response.statusCode < 300
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    print(res)
                    return res!
                }
                else
                {
                    return BasicResponse(success:0)
                }
            }.catchError { error in
                print("error::::")
                print(error)
                return Observable.error(error)
            }
            
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
