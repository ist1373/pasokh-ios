//
//  TutorService.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper

class TutorService {
    private static let tutorService = TutorService()
    
    public static func getInstance() ->TutorService{
        return tutorService;
    }
    
    func registerTutor(tutorDto:TutorDto) -> Observable<BasicResponse> {
        
        let jsonStudent = ConvertTools.convertToDictionary(text: tutorDto.toJSONString()!)
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "user/tutor",parameters:jsonStudent,encoding: JSONEncoding.default)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getAuthenticatedTutor() -> Observable<Tutor> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "user",headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<Tutor>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func editTutor(tutor:Tutor) -> Observable<BasicResponse> {
        
        let jsonStudent = ConvertTools.convertToDictionary(text: tutor.toJSONString()!)
        return RxAlamofire
            .requestJSON(.put,GlobalProperties.BaseURLV1  + "user/tutor/" + String(describing: tutor.id!) ,parameters:jsonStudent,encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                let res = Mapper<BasicResponse>().map(JSONObject: value)
                return res!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
}
