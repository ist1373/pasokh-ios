//
//  FavoriteTutorService.swift
//  SnapAsk
//
//  Created by iman on 6/13/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper

class FavoriteTutorService {
    private static let favoriteTutorService = FavoriteTutorService()
    
    public static func getInstance() ->FavoriteTutorService{
        return favoriteTutorService;
    }
    
    func addFavoriteTutor(stuId:Int,tutorId:Int, isFavorate:Bool) -> Observable<BasicResponse> {
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "favoriteTutor/student/\(stuId)?tutorId=\(tutorId)&isAdded=\(isFavorate)",encoding: JSONEncoding.default ,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    
    func getFavoriteTutors() -> Observable<[Tutor]>?{
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            let parameters: Parameters = [
                "stuId":user.id!
            ]
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "favoriteTutor/student/\(user.id!)" , parameters: parameters ,headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    
                    
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Tutor>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }


    }
    
    func existFavoriteTutor(stuId:Int,tutorId:Int) -> Observable<BasicResponse>?{
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            let parameters: Parameters = [
                "stuId":user.id!
            ]
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "favoriteTutor/student/\(stuId)?tutorId=\(tutorId)" , parameters: parameters ,headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    
                    
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<BasicResponse>().map(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
        
        
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
}
