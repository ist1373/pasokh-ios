//
//  QuestionReportService.swift
//  SnapAsk
//
//  Created by iman on 7/21/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class QuestionReportService {
    private static let questionReportService = QuestionReportService()
    
    public static func getInstance() ->QuestionReportService{
        return questionReportService;
    }
    
    func addQuestionReport(questionReport:QuestionReport) -> Observable<BasicResponse> {
        
        let jsonQuestionRep = ConvertTools.convertToDictionary(text: questionReport.toJSONString()!)
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "questionReport?quesId=\((questionReport.question?.id)!)&desc=\(questionReport.description!)&reportType=\(questionReport.reportType!)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
