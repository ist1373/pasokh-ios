//
//  PurchaseService.swift
//  SnapAsk
//
//  Created by iman on 6/19/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class PurchaseService {
    private static let purchaseService = PurchaseService()
    
    public static func getInstance() ->PurchaseService{
        
        return purchaseService;
    }
    
    func getAvailablePurchases() -> Observable<[Purchase]>?{
        
        if let user = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "purchase/available" ,headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Purchase>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
