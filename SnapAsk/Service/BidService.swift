//
//  BidService.swift
//  SnapAsk
//
//  Created by iman on 7/1/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class BidService{
    
    static let bidService = BidService()
    
    public static func getInstance() ->BidService{
        
        return bidService;
    }
    
    func addBid(questionId:Int64,credit:Float) -> Observable<BasicResponse> {

                return RxAlamofire
                .requestJSON(.post,GlobalProperties.BaseURLV1 + "bid?quesId=\(questionId)" + "&amount=\(credit)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .retry(5)
                .map { (response, value) in
                    if response.statusCode >= 200 && response.statusCode < 300
                    {
                        let res = Mapper<BasicResponse>().map(JSONObject: value)
                        print(res)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        print("response:::")
                        print(response)
                        return BasicResponse(success:0)
                    }
                }.catchError { error in
                    print("error::::")
                    print(error)
                    return Observable.error(error)
                }
                
                .observeOn(MainScheduler.instance)
                .asObservable()
    }
    
    func addDirectBid(questionId:Int64,accept:Bool) -> Observable<BasicResponse> {
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1 + "bid?quesId=\(questionId)" + "&accept=\(accept)",encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .retry(5)
            .map { (response, value) in
                if response.statusCode >= 200 && response.statusCode < 300
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    print(res)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    print(response)
                    return BasicResponse(success:0)
                }
            }.catchError { error in
                print("error::::")
                print(error)
                return Observable.error(error)
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
}
