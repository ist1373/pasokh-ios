
import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper

import Cache

class LocationService {
    
    private static let locationService = LocationService()
    static func getInstance() -> LocationService
    {
        return locationService
    }
    
    func getProvinces() -> Observable<[Province]> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "location/province")
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.never()
            }
            .retry(5)
            .map { (response, value) in
                let provinces = Mapper<Province>().mapArray(JSONObject: value)
                return provinces!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getCurrentProvinces() -> Observable<[Province]> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "location/province/current")
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.never()
            }
            .retry(5)
            .map { (response, value) in
                let provinces = Mapper<Province>().mapArray(JSONObject: value)
                return provinces!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    func getCities(provinceId:Int64) -> Observable<[City]> {
        return RxAlamofire
            .requestJSON(.get,GlobalProperties.BaseURLV1  + "location/province/\(provinceId)/city")
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                print(error)
                return Observable.never()
            }
            .retry(5)
            .map { (response, value) in
                let cities = Mapper<City>().mapArray(JSONObject: value)
                return cities!
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
}

