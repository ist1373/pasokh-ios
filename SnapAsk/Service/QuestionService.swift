//
//  QuestionService.swift
//  SnapAsk
//
//  Created by iman on 6/11/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import ObjectMapper


class QuestionService {
    private static let questionService = QuestionService()
    
    public static func getInstance() ->QuestionService{
        return questionService;
    }
    
    func addQuestion(question:Question) -> Observable<BasicResponse> {
        
        let jsonQuestion = ConvertTools.convertToDictionary(text: question.toJSONString()!)
        
        return RxAlamofire
            .requestJSON(.post,GlobalProperties.BaseURLV1  + "question" ,parameters:jsonQuestion,encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    
    func endQuestion(questionId:Int64,rate:Int,desc:String) -> Observable<BasicResponse> {
        
        return RxAlamofire
            .requestJSON(.put,GlobalProperties.BaseURLV1  + "question/\(questionId)/end?rate=\(rate)&desc=\(desc)" ,encoding: JSONEncoding.default,headers:setAuthorizationHeader())
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .debug()
            .catchError { error in
                return Observable.error(error)
            }
            .retry(5)
            .map { (response, value) in
                if 200..<300 ~= response.statusCode
                {
                    let res = Mapper<BasicResponse>().map(JSONObject: value)
                    return res!
                }
                else if response.statusCode == 401
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginPage()
                    throw ResponseError(message: "\(response.statusCode)")
                }
                else
                {
                    throw ResponseError(message: "\(response.statusCode)")
                }
                
            }
            .observeOn(MainScheduler.instance)
            .asObservable()
    }
    
    func getUserQuestions(page:Int) -> Observable<[Question]>? {
    

         if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
         {
            print("uuuuuuuuuuuuuu")
            print(u.id!)
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "question/user/" + String(u.id!) + "?page=\(page)",headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Question>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
         }
         else
         {
            return nil
         }
    }
    
    func getPublicQuestions(page:Int) -> Observable<[Question]>? {
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "question/tutor/" + String(u.id!) + "/public?page=\(page)",headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Question>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        print(value)
                        let bres = Mapper<BasicResponse>().map(JSONObject: value)
                        throw ResponseError(message: bres!.message! , code : response.statusCode)
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    func getQuestionById(id:Int) -> Observable<Question>? {
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "question/" + String(id) , headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Question>().map(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        print(value)
                        let bres = Mapper<BasicResponse>().map(JSONObject: value)
                        throw ResponseError(message: bres!.message! , code : response.statusCode)
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    func getDirectQuestions(page:Int) -> Observable<[Question]>? {
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "question/tutor/" + String(u.id!) + "/direct?page=\(page)",headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Question>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                    // return Observable.error( ResponseError(message: "401") )
                       throw ResponseError(message: "\(response.statusCode)")
                    }
                    
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    
    func getQuestionsByTutor(page:Int) -> Observable<[Question]>? {
        if let u = try? AppDelegate.getStorage().object(ofType: User.self, forKey: "user")
        {
            return RxAlamofire
                .requestJSON(.get,GlobalProperties.BaseURLV1  + "question/tutor/" + String(u.id!) + "?page=\(page)",headers:setAuthorizationHeader())
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .debug()
                .catchError { error in
                    return Observable.error(error)
                }
                .retry(5)
                .map { (response, value) in
                    if 200..<300 ~= response.statusCode
                    {
                        let res = Mapper<Question>().mapArray(JSONObject: value)
                        return res!
                    }
                    else if response.statusCode == 401
                    {
                                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                                appDelegate.showLoginPage()
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                    else
                    {
                        // return Observable.error( ResponseError(message: "401") )
                        throw ResponseError(message: "\(response.statusCode)")
                    }
                }
                .observeOn(MainScheduler.instance)
                .asObservable()
        }
        else
        {
            return nil
        }
    }
    
    
    
    
    private func setAuthorizationHeader() -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        if let token = try? AppDelegate.getStorage().object(ofType: String.self, forKey: "token")
        {
            headers = [
                "Authorization": "\(token)",
            ]
        }
        return headers
    }
    
}
