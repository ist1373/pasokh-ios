//
//  GlobalProperties.swift
//  SnapAsk
//
//  Created by iman on 5/22/18.
//  Copyright © 2018 iman. All rights reserved.
//

import Foundation
import UIKit

struct GlobalProperties  {
//    static let DarkPrimaryColor = UIColor(red: 55/255, green: 34/255, blue: 96/255, alpha: 1.0)
//    static let PrimaryColor = UIColor(red: 76/255, green: 54/255, blue: 125/255, alpha: 1.0)
    
    //cacheKeys:
    //token
    //user
    //userType
    
    
    
    
    //local
    static let BaseURLV1 = "http://192.168.26.1:8080/api/v1/"
    
    
    
  //    static let BaseURLV1 = "https://pasokh20.com:443/api/v1/"
    
//    static let BaseURLV2 = "http://api.booksum.ir/api/v2/"
//    static let BaseFileV1 = "http://file.booksum.ir/download_file.php?"
    
    
    
    static let WebSocketUrl = NSURL(string: "wss://pasokh20.com:443/ws/websocket")!
    
    static let PrimaryDarkColor = UIColor(rgb: 0x0f9d58)
    static let PrimaryColor = UIColor(rgb: 0x3CB030)
    
    static let PrimaryText = UIColor(rgb: 0x444444)
    static let SecondaryText = UIColor(rgb: 0x757575)
    static let lightColor = UIColor(rgb: 0x999999)
    static let greenColor = UIColor(rgb: 0x3CB030)
    static let blueColor = UIColor(rgb: 0x3C73F8)
    
    static let LocalBaseURLV1 = "http://192.168.26.1:8080/api/v1/"
    
    static let CreditScale = "ایپاسخ"
    static let CreditValue = 500
    

    
    
    static func getThumbnailImageURL(_ id:Int64) -> String
    {
        return "http://file.booksum.ir/download_file.php?fid=\(id)&isThumb=true"
    }
    static func getLocalThumbnailImageURL(_ id:Int64) -> String
    {
        return "http://192.168.26.1:8080/api/v1/file/\(id)?isThumb=true"
    }
    
    static func getImageURL(_ id:Int64) -> String
    {
        return "http://file.booksum.ir/download_file.php?fid=\(id)&isThumb=false"
    }
    
    static func getIranSansFont(size:CGFloat) -> UIFont
    {
        return UIFont(name:"IRANSansMobile" , size:size)!
    }
}
