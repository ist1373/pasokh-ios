//
//  ValidationRules.swift
//  Booksum
//
//  Created by iman on 11/10/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import Validator

class ValidationRules {

    
    static let emailValidation = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationError(message: "پست الکترونیک نادرست است."))
   
    
    static func stringRequiredRule(message:String) -> ValidationRuleRequired<String>
    {
         return ValidationRuleRequired<String>(error: ValidationError(message: message))
    }
   
    static func stringEqualityRule(with:String,message:String) -> ValidationRuleEquality<String>
    {
         return  ValidationRuleEquality<String>(dynamicTarget: { return with }, error:  ValidationError(message: message))
    }
    
    static func ComparisonRule(min:Int,max:Int,message:String) -> ValidationRuleComparison<Int>
    {
        return  ValidationRuleComparison<Int>(min: min, max: max, error: ValidationError(message: message))
    }
    
    static func LengthRule(min:Int,max:Int,message:String) -> ValidationRuleLength
    {
        return  ValidationRuleLength(min: min,max: max, error: ValidationError(message: message))
    }
    
    
}
