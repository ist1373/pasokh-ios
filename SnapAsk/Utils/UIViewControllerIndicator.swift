//
//  UIViewControllerIndicator.swift
//  Booksum
//
//  Created by iman on 11/25/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class UIViewControllerIndicator: UIViewController {
    var indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
}
