//
//  PersianTool.swift
//  Booksum
//
//  Created by iman on 10/30/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation


class PersianTool {
    static func convertToPersianDigits(_ string:String) -> String
    {
        let number = NSNumber(value: Int(string)!)
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let faNumber = format.string(from: number)
        
        return faNumber!
    }
    
    static func convertToEnglishDigits(_ string:String) -> String
    {
        if string.characters.count > 0
        {
            let number = NSNumber(value: Int(string)!)
            let format = NumberFormatter()
            format.locale = Locale(identifier: "en_US")
            let faNumber = format.string(from: number)
            
            return faNumber!
        }
        return ""
    }
    
    

    
    
    static func getDateStringFromTimeStamp(date:Double) -> String {
        let date = Date(timeIntervalSince1970: date/1000)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "fa_IR")
        dateFormatter.dateStyle = .medium
        
        return dateFormatter.string(from: date)
    }
}
