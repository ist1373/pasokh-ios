//
//  ConvertTools.swift
//  Booksum
//
//  Created by iman on 11/19/17.
//  Copyright © 2017 booksum. All rights reserved.
//

import Foundation
class ConvertTools {
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}
