//
//  ViewController.swift
//  SnapAsk
//
//  Created by iman on 5/12/18.
//  Copyright © 2018 iman. All rights reserved.
//

import UIKit
import StompClientLib

class ViewController: UIViewController ,StompClientLibDelegate{
    
     let socketClient = StompClientLib()
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        print("didReceiveMessageWithJSONBody")
        print(jsonBody)
        
        
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("didReceiveMessageWithJSONBody")
        
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("stompClientDidDisconnect")
        
    }
    
    func stompClientDidConnect(client: StompClientLib!) {
        print("stompClientDidConnect")
        socketClient.subscribe(destination: "/topic/public")
        
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("serverDidSendReceipt")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("serverDidSendError")
    }
    
    func serverDidSendPing() {
        print("serverDidSendPing")
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

    
        let url = NSURL(string: "ws://192.168.26.1:8080/ws/websocket")!
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self)
        socketClient.subscribe(destination: "/topic/public")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

    
    @IBAction func onRegisterUser(_ sender: Any) {
//        let cm = ChatMessage()
//        cm.type = MessageType.JOIN
//        cm.sender = "lksdjfglkdjglkfjhglglkfjghlk"
//        let header = [StompCommands.commandHeaderContentType:"application/json;charset=UTF-8"]
//        socketClient.sendMessage(message: cm.toJSONString()!, toDestination: "/app/chat.addUser", withHeaders: header, withReceipt: nil)
//        
    }
    
    
    
}

